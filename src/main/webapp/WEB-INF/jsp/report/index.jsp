<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta name="X-UA-Compatible" content="IE=EDGE,Chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>团队统计</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/layerui.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
    </jsp:include>
    <style>
        .c{
            font-size: 20px;
            width: 200px;
            float: left;
            padding: 10px;
        }
    </style>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body style="padding: 20px;">

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>团队统计</legend>
</fieldset>

<div style="width: 100%;overflow: hidden;padding-left: 30px;">
    <div style="width: 700px;">
        <div class="c">团队人数：${item.allCount}</div> <div class="c">代理人数：${item.agentCount}</div>
        <div class="c">普通客户：${item.gamerCount}</div>
        <div class="c">充值总和：${item.rechargeTotal}</div> <div class="c">今日充值：${item.rechargeToday}</div>
    </div>
</div>
<br>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
    <legend>客户充值排行榜</legend>
</fieldset>
<table class="layui-table">
    <colgroup>
        <col width="150">
        <col width="150">
        <col width="200">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th>游戏ID</th>
        <th>用户账号</th>
        <th>姓名</th>
        <th>充值金币</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${item.tops}" var="item">
            <tr>
                <td>${item.gameId}</td>
                <td>${item.account}</td>
                <td>${item.userName}</td>
                <td>${item.amount}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>



<%@ include file="../common/ajax_login_timeout.jsp" %>
</body>
</html>
