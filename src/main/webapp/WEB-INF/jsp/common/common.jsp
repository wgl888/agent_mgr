<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/tags.jsp"%>
<script type="text/javascript">
	//用户中心主域地址(开发环境-'http://localhost:8001')
    var userCenterPath = '${userCenterPath}';
	var basePath = '<%=request.getContextPath() %>';
</script>
<!-- jquery -->
<c:if test="${param.useJQuery == true}">
	<script type="text/javascript"src="<%=request.getContextPath()%>/myResources/manager/plat/jquery/jquery-1.11.2.min.js"></script>
</c:if>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/js/common.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/js/common_detail.js"></script>
<c:if test="${param.useFormCheck == true}">
	<link href="<%=request.getContextPath()%>/myResources/manager/plat/icheck/icheck.css"rel="stylesheet" type="text/css" />
	<script type="text/javascript"src="<%=request.getContextPath()%>/myResources/manager/plat/icheck/jquery.icheck.min.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/myResources/manager/plat/Validform/5.3.2/Validform.min.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/myResources/manager/plat/Validform/5.3.2/Validform.js"></script>
</c:if>
<!-- 是否加入AngulaJs -->
<c:if test="${param.angular == true}">
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/angularjs/angular.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/angularjs/angular-resource.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/angularjs/baseapp.js"></script>
</c:if>
<!-- 是否需要使用分页 -->
<c:if test="${param.paging == true}">
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/laypage/1.2/laypage.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/laypage/1.2/doone_page.js"></script>
</c:if>

<!-- 是否需要弹出框 -->
<c:if test="${param.popup == true}">
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/layer/1.9.3/layer.js"></script>
</c:if>

<!-- 是否需要弹出框 -->
<c:if test="${param.ueditor == true}">
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ueditor/ueditor.all.min.js"></script>
</c:if>

<c:if test="${param.bootstrap == true}">
	<%--<link href="<%=request.getContextPath()%>/myResources/manager/assets/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css" />--%>
	<link href="<%=request.getContextPath()%>/myResources/manager/assets/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css" />
	<!-- ace样式核心 -->
	<link href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace.css" media="screen" rel="stylesheet" type="text/css" />
	<!-- ace的IE样式兼容包 -->
	<link href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/ace-part2.css" media="screen" rel="stylesheet" type="text/css" />

	<!-- 字体图标样式表 -->
	<link href="<%=request.getContextPath()%>/myResources/manager/assets/css/font-awesome.css"
		  media="screen" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/myResources/manager/assets/css/ace-fonts.css"
		media="screen" rel="stylesheet" type="text/css" />

	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/bootstrap.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace.js" type="text/javascript"></script>
	<!-- 各种插件 -->
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/bootstrapTools.js?v=123"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace/elements.scroller.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace-extra.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace/ace.sidebar.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace/ace.sidebar-scroll-1.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/ace/elements.spinner.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.inputlimiter.1.3.1.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.maskedinput.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/fuelux.spinner.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery.autosize.js"></script>

</c:if>



<c:if test="${param.datePicker == true}">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/datepicker.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/bootstrap-timepicker.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/daterangepicker.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/bootstrap-datetimepicker.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/colorpicker.css" />
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/date-time/bootstrap-datepicker.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/date-time/bootstrap-timepicker.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/date-time/moment.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/date-time/daterangepicker.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/date-time/bootstrap-datetimepicker.js"></script>
</c:if>

<c:if test="${param.layerUi == true}">
	<link rel="stylesheet"
		  href="<%=request.getContextPath()%>/myResources/manager/plat/layui/css/layui.css"  media="all">
	<script type="text/javascript"
			src="<%=request.getContextPath()%>/myResources/manager/plat/layui/layui.js"></script>
</c:if>

<c:if test="${param.zTree == true}">
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/js/dyncHeight.js"></script>
</c:if>
<!-- 树形组件 -->
<c:if test="${param.zTree == true}">
	<%--<link type="text/css" href="<%=request.getContextPath()%>/myResources/manager/plat/ztree/css/zTreeStyle/zTreeStyle.css?v=20150332" rel="stylesheet" />--%>
	<%--<link type="text/css" href="<%=request.getContextPath()%>/myResources/manager/plat/ztree/css/demo.css" rel="stylesheet" />--%>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/zTreeStyle/zTreeStyle.css?t=es4eg" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ztree/js/jquery.ztree.excheck-3.5.js"></script>
</c:if>
<c:if test="${param.chosenMultiple == true}">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/chosen.css" />
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery-ui.custom.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/chosen.jquery.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/js/chosen-multiple.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/js/ajaxfileupload.js"></script>
</c:if>
<!-- 查询界面App -->
<c:if test="${param.multiSch == true}">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/angularjs/multsch/style.css" />
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/angularjs/multsch/salesNoApp.js"></script>
<%-- 	<script src="<%=request.getContextPath()%>/myResources/manager/plat/angularjs/multsch/salesPraApp.js"></script> --%>
</c:if>
<c:if test="${param.ajaxFile == true }">
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/qlc/js/jquery.form.js"></script>
</c:if>
<!-- 自定义样式 -->
<link href="<%=request.getContextPath()%>/myResources/manager/plat/css/style.css" media="screen" rel="stylesheet" type="text/css" />

<%------------------------------以下为新增---------------------------------%>
<!-- 2017云平台新树形 -->
<c:if test="${param.newzTree == true }">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/zTreeStyle/zTreeStyle.css?t=es4eg" />
</c:if>

<!-- 滚动条插件 -->
<c:if test="${param.ZUIplugin == true }">
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/zUI.js?t=es5i5"></script>
</c:if>

<!-- 表格插件 -->
<c:if test="${param.flexigrid == true }">
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/flexigrid_ext.js"></script>
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/table_tool.js"></script>
</c:if>

<!--2017云项目样式改版jui.css-->
<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css" />

<%--form--%>
<c:if test="${param.nform == true }">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/form.css" />
</c:if>

<%--cookie工具--%>
<c:if test="${param.cookieutil == true }">
	<script src="<%=request.getContextPath()%>/myResources/manager/plat/js/cookieutil.js" type="text/javascript"></script>
</c:if>

<%--公共样式切换--%>
<script src="<%=request.getContextPath()%>/myResources/manager/plat/js/bodyClass.js" type="text/javascript"></script>

<c:if test="${param.jqueryUI == true}">
	<!-- jQuery UI插件 包含美化下拉 日期选择-->
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/assets/js/jquery-ui.js?t=es26p"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jquery-ui.css?t=es5i5" />
</c:if>
<c:if test="${param.userForm == true }">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/form/css/index.css" />
</c:if>
<c:if test="${param.iframeHeight == true}">
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/js/iframeHeight.js"></script>
</c:if>
<c:if test="${param.plupload == true}">
	<script type="text/javascript"
			src="<%=request.getContextPath() %>/myResources/manager/plat/plupload-2.3.1/js/plupload.full.min.js"></script>
</c:if>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/js/ylt_popup.js"></script>
