<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../common/tags.jsp"%>
<script type="text/javascript">
	// document.domain = "www.fujiancj.cn";
</script>
<%--import jquery--%>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/plat/jquery/jquery-1.11.2.min.js"></script>
<%--import layui--%>
<link rel="stylesheet"
	  href="<%=request.getContextPath()%>/myResources/manager/plat/layui/css/layui.css"  media="all">
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/plat/layui/layui.all.js"></script>
<%--open detail popup--%>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/js/ylt_popup.js"></script>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/js/ylt_tool.js"></script>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/plat/js/common.js"></script>
<script type="text/javascript"
		src="<%=request.getContextPath()%>/myResources/manager/plat/js/common_detail.js"></script>

<!-- 树形组件 -->
<c:if test="${param.zTree == true}">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/zTreeStyle/zTreeStyle.css?t=es4eg" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ztree/js/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ztree/js/jquery.ztree.exedit-3.5.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/ztree/js/jquery.ztree.excheck-3.5.js"></script>
</c:if>

<c:if test="${param.viewer == true}">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/viewerjs/viewer.css"/>
	<script type="text/javascript" src="${applicationScope.staticFilepath }/myResources/manager/viewerjs/viewer.js"></script>
</c:if>