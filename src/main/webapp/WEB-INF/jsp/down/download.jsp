<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta name="X-UA-Compatible" content="IE=EDGE,Chrome=1">
    <title>APP下载</title>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <style type="text/css">
        html, body, div, span, object, iframe, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, input, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video, h1, h2, h3, h4, h5, h6 {
            margin: 0;
            padding: 0;
            border: 0;
            -webkit-tap-highlight-color: transparent;
            -webkit-touch-callout: none;
        }

        html, body {
            min-height: 100%;
            color: #666;
            background-size: 100% 100vh;
            background-repeat: no-repeat;
        }

        html {
            font-size: 20px;
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        @media only screen and (min-width: 360px) {
            html {
                font-size: 22.5px;
            }
        }

        @media only screen and (min-width: 375px) {
            html {
                font-size: 23.5px;
            }
        }

        @media only screen and (min-width: 400px) {
            html {
                font-size: 25px;
            }
        }

        @media only screen and (min-width: 414px) {
            html {
                font-size: 25.875px;
            }
        }

        .container{
            text-align: center;
        }

        canvas{
            width: 10rem;
            height: 10rem;
            margin-top: 3rem;
            border:10px solid #fff;
        }

        .btnPic{
            width: 5rem;
            height: 2.2rem;
            margin-top: 0.2rem;
        }

        h3.title{
            color: #fff;
            margin-top: 0.8rem;
            font-size: 1.1rem;
            font-weight: 400;
        }

        .content{
            color:#fff;
            margin-top: 0.6rem;
            font-size: 0.8rem;
            font-weight: 400;
            line-height: 1.4rem;
            margin-left: 1rem;
            margin-right: 1rem;
        }


    </style>
</head>
<body style="background-image: url('<%=request.getContextPath()%>/myResources/manager/image/bgc.jpg');">
    <%--<div class="test"></div>--%>
    <%--<div class="container">--%>
        <%--<div style="padding-top: 100px;"><div id="output" class="codeBarPic"></div></div>--%>
        <%--<div><img id="downloadButton" src="<%=request.getContextPath()%>/myResources/manager/image/btn_common.png" class="btnPic"></div>--%>
    <%--</div>--%>

    <div class="test"></div>
    <div class="container">
        <div id="output"></div>
        <div><img id="downloadButton" src="<%=request.getContextPath()%>/myResources/manager/image/btn_common.png" class="btnPic"></div>
        <h3 class="title"><span>苹果用户安装说明</span></h3>
        <div class="content" >
            &nbsp;&nbsp;&nbsp;&nbsp;打开手机设置——通用——文件描述与设备管理——企业级应用设置信任
        </div>
    </div>

    <%--<input type="button" id="downloadButton" value="下载APP">--%>
</body>
<!-- 建议直接引用下面的js链接，以便得到最及时的更新，我们将持续跟踪各种主流浏览器的变化，为您提供最好的服务-->
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/myResources/manager/plat/jquery/jquery.qrcode.min.js"></script>
<script type="text/javascript" src="//www.shareinstall.net/js/page/shareinstall-h5.min.js"></script>
<script type="text/javascript">

    var ele=document.getElementById("downloadButton")
    ele.addEventListener("touchstart", function(){
        ele.src = "<%=request.getContextPath()%>/myResources/manager/image/btn_press.png";
    });
    ele.addEventListener("touchend", function(){
        ele.src = "<%=request.getContextPath()%>/myResources/manager/image/btn_common.png";
    });


    $(function(){
        var domain = window.location.href;
        $('#output').qrcode(domain);
    })

    //错误处理：确保app始终能正常的安装
    var timer = setTimeout(
        function () {
            var button = document.getElementById("downloadButton");
            button.style.visibility = "visible";
            button.onclick = function () {
                var ua = navigator.userAgent;
                if (ua.indexOf("MicroMessenger/") > -1) {
                    //微信中显示遮罩提示在浏览器中打开或进入应用宝
                    var div = document.createElement("div");
                    div.innerHTML = "<div style='font-size:2rem;color:#fff;text-align:center;"
                        + "position:fixed;left:0;top:0;background:rgba(0,0,0,0.5);filter:alpha(opacity=50);"
                        + "width:100%;height:100%;z-index:10000;'>点击右上角在浏览器中打开</div>";
                    document.body.appendChild(div);
                } else {
                    if (ua.indexOf("Android") > -1) {
                        //直接下载apk
                        //window.location="apk地址";
                    } else if (ua.indexOf("iPhone") > -1 || ua.indexOf("iPad") > -1
                        || ua.indexOf("iPod") > -1) {
                        //直接进入appstore
                        //window.location="appstore地址";
                    }
                }
            }
        }, 5000);

    //shareinstall初始化，初始化时将与shareinstall服务器交互，应尽可能早的调用
    /*web页面向app传递的json数据(json string/js Object)，应用被拉起或是首次安装时，通过相应的
     android/ios api可以获取此数据*/
    var data = ShareInstall.parseUrlParams();//shareinstall.js中提供的工具函数，解析url中的所有查询参数
    new ShareInstall({
        appKey: 'A6BK7F6FA27RAA',
        onready: function () {
            //shareinstall已成功回调，清除定时器
            clearTimeout(timer);
            timer = null;

            var m = this, button = document.getElementById("downloadButton");
            button.style.visibility = "visible";

            /*用户点击某个按钮时(假定按钮id为downloadButton)，安装app*/
            button.onclick = function () {
                m.wakeupOrInstall();
            }
        }
    }, data);
</script>
</html>
