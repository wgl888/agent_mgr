<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>提现申请</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="plupload" value="true"/>
        <jsp:param name="ueditor" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="overflow: auto">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="<%=request.getContextPath()%>/agent/user/subList" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>申请提现</h3>
                <h5>代理商申请将自己的收益提现。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/agent/user/doApplyWithDraw"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" name="userNo" value="${user.userNo }" />
        <input type="hidden" name="gameId" value="${user.gameId }" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>总返现金币</label>
                </dt>
                <dd class="opt">
                    ${user.profitTotal}
                    <span class="Validform_checktip"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>已提现金币</label>
                </dt>
                <dd class="opt">
                    ${user.profitUsedTotal}
                    <span class="Validform_checktip"></span>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>可提现金币</label>
                </dt>
                <dd class="opt">
                    <strong style="color:red;font-size: 18px;">${user.profitTotal - user.profitUsedTotal}</strong>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>申请提现金币</label>
                </dt>
                <dd class="opt">
                    <input type="text" placeholder="" class="input-txt"
                           id="amount" name="amount" datatype="*1-18" maxlength="18"
                           placeholder="请输入提现金币" nullmsg="提现金币额度不能为空">
                    <span class="Validform_checktip">

                    </span>
                    <p class="notic">输入大于0的整数。</p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        var gameId = '';
                        if(data.body.gameId!=undefined&&data.body.gameId!=null&&data.body.gameId!=''){
                            gameId = data.body.gameId;
                        }
                        location.href = "<%=request.getContextPath() %>/agent/user/subList?gameId="+gameId;
                        layer.close(index);
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

</script>

<script>

    var successFile = [];
    var failFile = [];

    //实例化一个plupload上传对象
    var uploaders = new plupload.Uploader({
        browse_button: 'upLoader', //触发文件选择对话框的按钮，为那个元素id
        url: '${pageContext.request.contextPath}/upload/img',//服务器端的上传页面地址
        multi_selection:false,
        filters: {
            prevent_duplicates: false //不允许选取重复文件
        },
        flash_swf_url: '${pageContext.request.contextPath}/myResources/plupload-2.3.6/js/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url: '${pageContext.request.contextPath}/myResources/plupload-2.3.6/js/js/Moxie.xap' //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
    });
    //在实例对象上调用init()方法进行初始化
    uploaders.init();
    //绑定各种事件，并在事件监听函数中做你想做的事
    uploaders.bind('FilesAdded', function (uploader, files) {
        uploader.start();
    });

    //上传完成后触发
    uploaders.bind('FileUploaded', function (uploader, file, responseObject) {
        var response = responseObject.response;
        var obj = JSON.parse(response);
        $("#showImg").html("");
        $("#showImg").append("<img style='margin:0 0 10px 0' width='120px' src='" + obj.path + "'>");
        $("#showImg").append("<input type='hidden' name='bankIcon' value='" + obj.path + "'>");
    });

    uploaders.bind('UploadComplete', function (uploader, file) {

    });
</script>

</body>
</html>
