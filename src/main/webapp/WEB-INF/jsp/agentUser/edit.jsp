<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <title>编辑银行信息</title>
    <jsp:include page="../common/common.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
        <jsp:param name="useJQuery" value="true"/>
        <jsp:param name="bootstrap" value="true"/>
        <jsp:param name="paging" value="false"/>
        <jsp:param name="popup" value="true"/>
        <jsp:param name="zTree" value="false"/>
        <jsp:param name="ZUIplugin" value="false"/>
        <jsp:param name="cookieutil" value="false"/>
        <jsp:param name="userForm" value="true"/>
        <jsp:param name="useFormCheck" value="true"/>
        <jsp:param name="plupload" value="true"/>
        <jsp:param name="ueditor" value="true"/>
    </jsp:include>
    <%@ include file="../common/ajax_login_timeout.jsp" %>
</head>
<body style="overflow: auto">

<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <a class="back" href="javascript:history.go(-1);" title="返回列表"><i
                    class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>${item.id==null?'新增':'编辑' }代理用户</h3>
                <h5>新增或者修改一级和二级代理商用户并设置分成比例。</h5>
            </div>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/agent/user/${item==null?'doAdd':'doEdit' }"
          method="post" class="form form-horizontal" id="form">
        <input type="hidden" id="id" name="id" value="${item.id }" />
        <input type="hidden" id="gameId" name="gameId" value="123456" />
        <div class="ncap-form-default">
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>用户账号</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="${item.account }" placeholder="" class="input-txt" ${item.id!=null?'disabled':''}
                           id="account" name="account" datatype="*1-120" maxlength="120"
                           placeholder="用户账号" nullmsg="用户账号不能为空">
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        输入账号。
                    </p>
                </dd>
            </dl>
            <%--<dl class="row">--%>
                <%--<dt class="tit">--%>
                    <%--<label><em>*</em>用户姓名</label>--%>
                <%--</dt>--%>
                <%--<dd class="opt">--%>
                    <%--<input type="text" value="${item.userName }" placeholder="" class="input-txt"--%>
                           <%--id="userName" name="userName" datatype="*1-120" maxlength="120"--%>
                           <%--placeholder="用户姓名" nullmsg="用户姓名">--%>
                    <%--<span class="Validform_checktip"></span>--%>
                <%--</dd>--%>
            <%--</dl>--%>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>代理级别</label>
                </dt>
                <dd class="opt">
                    <label>
                        <input type="radio" class="ace"
                               name="agentLevel" ${item.agentLevel=='1'||item.agentLevel==null?'checked':''} value="1"/>
                        <span class="lbl"> 一级代理</span>
                    </label>
                    <label>
                        <input type="radio" class="ace" name="agentLevel" ${item.agentLevel=='2'?'checked':''} value="2"/>
                        <span class="lbl"> 二级代理</span>
                    </label>
                    <label>
                        <input type="radio" class="ace" name="agentLevel" ${item.agentLevel=='3'?'checked':''} value="3"/>
                        <span class="lbl"> 玩家</span>
                    </label>
                    <span class="Validform_checktip"></span>
                    <p class="notic">
                        一级代理需要配置玩家和二级代理分成比例，二级代理只需配置玩家分成比例即可。
                    </p>
                </dd>
            </dl>
            <dl class="row" id="level_3">
                <dt class="tit">
                    <label><em>*</em>玩家分成比例</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<fmt:formatNumber type="number" value="${item.profitRate}" pattern="#"/>" placeholder="" class="input-txt"
                           id="profitRate" name="profitRate"
                           placeholder="玩家分成比例">%
                    <span class="Validform_checktip">

                    </span>
                    <p class="notic">不能为负数，为0表示不计算分成。</p>
                </dd>
            </dl>
            <dl class="row" id="level_2">
                <dt class="tit">
                    <label><em>*</em>二级玩家分成比例</label>
                </dt>
                <dd class="opt">
                    <input type="text" value="<fmt:formatNumber type="number" value="${item.subProfitRate}" pattern="#"/>" placeholder="" class="input-txt"
                           id="subProfitRate" name="subProfitRate" maxlength="120"
                           placeholder="二级玩家分成比例">%
                    <span class="Validform_checktip">
                    </span>
                    <p class="notic">不能为负数，为0表示不计算分成。</p>
                </dd>
            </dl>
            <div class="bot">
                <button type="submit" class="ncap-btn-big ncap-btn-green" style="height: 36px;" id="submitBtn">确认提交
                </button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $('input[name="agentLevel"]').click(function(){
        chooseLevel($(this).val());
    });



    chooseLevel = function(val){
        if(val==undefined||val==null||val==''){
            return;
        }
        if(val==1*1){
            $('#level_3').show();
            $('#level_2').show();
        }else if(val==2*1){
            $('#level_3').show();
            $('#level_2').val("0");
            $('#level_2').hide();
        }else if(val==3*1){
            $('#level_3').val("0");
            $('#level_3').hide();
            $('#level_2').val("0");
            $('#level_2').hide();
        }
    }


    chooseLevel(${item.agentLevel});

    $(function () {
        //form表单校验提交
        $("#form").Validform({
            tiptype: 4,
            showAllError: true,
            postonce: false,
            ajaxPost: true,
            callback: function (data) {//提交后回调函数
                if (data.head.respCode == 0) {
                    layer.confirm('操作成功，返回列表？', {icon: 3, title: '提示'}, function (index) {
                        history.go(-1);
                        layer.close(index);
                    });
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    });

</script>

</body>
</html>
