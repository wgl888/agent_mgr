<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>玩家选择</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/layerui.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
    </jsp:include>
</head>
<body>
<table id="myTable"></table>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="application/javascript">

    var table = layui.table;
    var myTable = table.render({
        elem: '#myTable'
        , method: 'POST'
        , height:280
        , url: '${pageContext.request.contextPath}/agent/user/queryGamer?parentUserNo=${parentUserNo}' //数据接口
        , limit: 10
        , limits: [10, 30, 50, 100, 200, 500]
        , request: {
            pageName: 'queryPage' //页码的参数名称，默认：page
            , limitName: 'querySize' //每页数据量的参数名，默认：limit
        }
        , skin: 'row' //行边框风格
        , even: true //开启隔行背景
        , page: true //开启分页
        , cols: [[ //表头
            {type:'checkbox'}
            , {field: 'account', title: '用户账号',width:160, align: 'center'}
            , {field: 'userName', title: '姓名',width:140, align: 'center', sort: true}
            , {field: 'registerTimeStr', title: '注册时间', align: 'center', sort: true}
        ]]
    });

    resetSearch = function () {
        $('#userName').val("");
        $('#account').val("");
        $('#startTime').val("");
        $('#endTime').val("");
        queryPage();
    };

    queryPage = function () {
        //这里以搜索为例
        myTable.reload({
            page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }

    doSomething=function(dom){
        var checkStatus = table.checkStatus('myTable');
        var data = checkStatus.data;
        dom.doBind(JSON.stringify(data));
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }


    function getLocalTime(timestamp) {
        var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = date.getDate() + ' ';
        var h = date.getHours() + ':';
        var m = date.getMinutes() + ':';
        var s = date.getSeconds();
        return Y+M+D+h+m+s;
    }



</script>
<script type="text/html" id="registerTime">
    {{getLocalTime(d.registerTime)}}
</script>
</body>
</html>
