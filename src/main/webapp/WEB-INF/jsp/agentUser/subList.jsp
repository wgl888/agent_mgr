<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>代理用户下级明细</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/layerui.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body style="padding: 20px;width:1200px;overflow-x:auto;">
<div class="layui-form-item layui-form-pane">
    <input type="hidden" id="parentUserNo" value="${parentUser.gameId}">
    <div class="layui-inline">
        <label class="layui-form-label">上级账号：</label>
        <div class="layui-input-block">
            <input placeholder="输入上级账号" id="parentAccount" name="parentAccount" ${isMgr==false?'readonly':''}
                   class="layui-input" type="text" value="${parentUser.account}" />
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">上级姓名：</label>
        <div class="layui-input-block">
        <input placeholder="输入上级姓名" id="parentUserName" name="parentUserName" ${isMgr==false?'readonly':''}
               class="layui-input" type="text" value="${parentUser.userName}" />
        </div>
    </div>
    <div class="layui-inline w300">
        <label class="layui-form-label">时间(起)：</label>
        <div class="layui-input-block">
            <input type="text" readonly id="startTime" name="startTime" class="layui-input"
                   placeholder="代理时间起">
        </div>
    </div>
    <div class="layui-inline w300">
        <label class="layui-form-label">时间(止)：</label>
        <div class="layui-input-block">
            <input type="text" readonly id="endTime" name="endTime" class="layui-input"
                   placeholder="代理时间止">
        </div>
    </div>

</div>
<div class="layui-form-item layui-form-pane">
    <div class="layui-inline">
        <label class="layui-form-label">下级账号：</label>
        <div class="layui-input-block">
            <input placeholder="输入下级账号" id="account" name="account"
                  class="layui-input" type="text"/>
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">下级姓名：</label>
        <div class="layui-input-block">
            <input placeholder="输入下级姓名" id="userName" name="userName"
                    class="layui-input" type="text"/>
        </div>
    </div>
    <div class="layui-inline">
        <div class="layui-btn-group">
            <button onclick="queryPage();"
                    type="button" class="layui-btn">
                <i class="layui-icon">&#xe615;</i>查询
            </button>
            <button onclick="resetSearch();"
                    type="button" class="layui-btn layui-btn-danger">
                <i class="layui-icon">&#x1002;</i>重置
            </button>
        </div>
    </div>
</div>
<div class="layui-form-item layui-form-pane">
    <c:if test="${parentUser.agentLevel == 1}">
        <button onclick="javascript:chooseGamer();"
                type="button" class="layui-btn layui-btn-normal">
            <i class="layui-icon">&#xe66a;</i>绑定下级代理
        </button>
    </c:if>
    <strong>上级返现总金币：<font id="sj_jb">${nowUser==null?parentUser.profitTotal:nowUser.profitTotal}&nbsp;&nbsp;</font></strong>
    <strong>已提现金币：<font id="ytx_jb">${nowUser==null?parentUser.profitUsedTotal:nowUser.profitUsedTotal}&nbsp;&nbsp;</font></strong>
    <strong>可提现金币：<font id="ktx_jb">${nowUser==null?(parentUser.profitTotal-parentUser.profitUsedTotal):(nowUser.profitTotal-nowUser.profitUsedTotal)}&nbsp;&nbsp;</font></strong>
    <button onclick="javascript:toApplyWithDraw();"
            type="button" class="layui-btn layui-btn-normal">
        <i class="layui-icon">&#xe608;</i>申请提现
    </button>
</div>
<table id="myTable"></table>
<button onclick="javascript:toApplyWithDraw();"
        type="button" class="layui-btn layui-btn-normal">
    <i class="layui-icon">&#xe608;</i>申请提现
</button>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="application/javascript">

    var table = layui.table;
    var myTable = table.render({
        elem: '#myTable'
        , method: 'POST'
        , height:570
        , width:1100
        , url: '${pageContext.request.contextPath}/agent/user/subPage?parentAccount=' + $('#parentAccount').val() + "&parentUserName=" + $('#parentUserName').val()//数据接口
        , limit: 10
        , limits: [10, 30, 50, 100, 200, 500]
        , request: {
            pageName: 'queryPage' //页码的参数名称，默认：page
            , limitName: 'querySize' //每页数据量的参数名，默认：limit
        }
        , skin: 'row' //行边框风格
        , even: true //开启隔行背景
        , page: true //开启分页
        , cols: [[ //表头
            {field: '', title: '操作',width:220, align: 'center', sort: false, fixed: 'left',templet: '#roleRemark'}
            , {field: 'parentUserName', title: '所属上级',width:100, align: 'center', sort: false}
            , {field: 'gameId', title: '游戏ID',width:70, align: 'center', sort: false}
            , {field: 'account', title: '用户账号',width:120, align: 'center'}
            , {field: 'userName', title: '姓名',width:90, align: 'center', sort: true}
            , {field: 'profitTotal', title: '贡献',width:80, align: 'center', sort: true}
            , {field: 'totalOffer', title: '分成',width:80, align: 'center', sort: true}
            , {field: 'agentLevel', title: '类型',width:80, align: 'center', sort: true,templet:'#agentLevel'}
            , {field: 'agentTimeStr', title: '代理时间',align: 'center', sort: true}
        ]]
    });

    resetSearch = function () {
        $('#userName').val("");
        $('#account').val("");
        $('#startTime').val("");
        $('#endTime').val("");
        queryPage();
    };

    queryPage = function () {
        // 查询用户详情回填金币
        $.ajax({
            type : "post",
            traditional:true,
            url : '<%=request.getContextPath()%>/agent/user/userDetail',
            data : {"account":$('#parentAccount').val(),
                    "userName":$('#parentUserName').val()},
            success : function(data) {
                try {
                    if(data!=null){
                        $('#sj_jb').html(data.profitTotal);
                         // cxz$('#ytx_jb').html(data.profitUsedTotal);
                        $('#ktx_jb').html(data.profitTotal-bean.profitUsedTotal);
                    }
                } catch (error) {

                }
            }
        });


        //这里以搜索为例
        myTable.reload({
            url: '${pageContext.request.contextPath}/agent/user/subPage',
            where: { //设定异步数据接口的额外参数，任意设
                parentUserName: $('#parentUserName').val(),
                parentAccount: $('#parentAccount').val(),
                userName: $('#userName').val(),
                account: $('#account').val(),
                startTime: $('#startTime').val(),
                endTime: $('#endTime').val()
            }
            , page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }

</script>
<script type="text/html" id="roleRemark">
    {{# if(d.agentLevel!='3'){}}
    <button type="button" class="layui-btn layui-btn-sm"
            author="/plat/role/edit"
            onclick="cancleBind('{{d.id}}')"><i class="layui-icon">&#xe642;</i>解除绑定
    </button>
    {{#}}}
    {{# if(d.agentLevel=='2'){}}
    <button type="button" class="layui-btn layui-btn-sm layui-btn-danger"
            author="/plat/role/delete"
            onclick="followGamer('{{d.gameId}}');"><i class="layui-icon">&#xe613;</i>下级
    </button>
    {{#}}}

    <c:if test="${isMgr==true}">
        <button type="button" class="layui-btn layui-btn-sm"
                author="/plat/role/edit"
                onclick="edit('{{d.id}}')"><i class="layui-icon">&#xe642;</i>编辑
        </button>
    </c:if>

</script>

<script type="text/html" id="agentLevel">
    {{# if(d.agentLevel=='1'){}}
    <strong>一级代理</strong>
    {{# }else if(d.agentLevel=='2'){}}
    <strong>二级代理</strong>
    {{# }else if(d.agentLevel=='3'){}}
    <strong>玩家</strong>
    {{#}}}
    </script>

<script type="text/javascript">

    followGamer = function(no){
        popup_none('玩家列表',
            "${pageContext.request.contextPath}/agent/user/gamerList?gameId="+no, 700, 400, this);
    }

    toApplyWithDraw = function(){
        location.href = '<%=request.getContextPath()%>/agent/user/toApplyWithDraw?gameId='+$('#parentUserNo').val();
    }

    chooseGamer = function () {
        popup_button('选择玩家',
            '<%=request.getContextPath()%>/agent/user/toQueryGamer?parentUserNo='+$('#parentUserNo').val(), 600, 400, this);
    };

    doBind = function(data){
        var obj = JSON.parse(data); //由JSON字符串转换为JSON对象
        var ids = '';
        for (var i = 0; i < obj.length; i++) {
            ids += obj[i].userNo + ",";
        }
        // 设置父ID和类型为二级代理商
        $.ajax({
            type : "post",
            traditional:true,
            url : '<%=request.getContextPath()%>/agent/user/bindAagentBatch',
            data : {"subUserIds":ids,"superUserId":$('#parentUserNo').val()},
            success : function(data) {
                if (data.head.respCode == 0) {
                    reload();
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    }


    cancleBind = function(id){
        var index = layer.confirm('确定要解除绑定？', {icon: 3, title:'提示'}, function(index){
            $.ajax({
                type : "post",
                traditional:true,
                url : '<%=request.getContextPath()%>/agent/user/cancleBind',
                data : {"id":id},
                success : function(data) {
                    if (data.head.respCode == 0) {
                        reload();
                    } else {
                        layer_error(data.head.respMsg);
                    }
                    layer.close(index);
                }
            });
        });


    }

    layui.use('laydate', function () {
        var laydate = layui.laydate;

        var start = {
            elem: '#startTime',
            max: '2099-06-16 23:59:59',
            istoday: false,
            format: 'yyyy-MM-dd HH:mm:ss',
            type:'datetime',
            istime: true,
            done: function (value, date) {
                endTime.config.min = date; //开始日选好后，重置结束日的最小日期
                endTime.config.min.month = date.month - 1;//将月份减一（默认为下个月，减一可得到设置的原月份）
            }
        };
        var end = {
            elem: '#endTime',
            max: '2099-06-16 23:59:59',
            istoday: false,
            format: 'yyyy-MM-dd',
            format: 'yyyy-MM-dd HH:mm:ss',
            type:'datetime',
            istime: false,
            done: function (value, date) {
                startTime.config.max = date; //结束日选好后，重置开始日的最大日期
                startTime.config.max.month = date.month - 1;//将月份减一（默认为下个月，减一可得到设置的原月份）
            }
        };
        //    执行一个laydate实例
        var startTime = laydate.render(start);
        var endTime = laydate.render(end);
    });

    add = function () {
        location.href = "${pageContext.request.contextPath}/agent/user/toAdd";
    }
    edit = function (id) {
        location.href = '${pageContext.request.contextPath}/agent/user/toEdit?id=' + id;
    }

    layerCallBack = function () {
        reload();
    }

    reload = function () {
        queryPage();
    }
</script>
</body>
</html>
