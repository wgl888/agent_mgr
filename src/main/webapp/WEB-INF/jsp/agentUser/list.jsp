<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta name="X-UA-Compatible" content="IE=EDGE,Chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>代理用户管理</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/layerui.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body style="padding: 20px;">
<div class="layui-form-item layui-form-pane">
    <div class="layui-inline">
        <label class="layui-form-label">用户姓名：</label>
        <div class="layui-input-block">
        <input placeholder="输入用户姓名" id="userName" name="userName"
               class="layui-input" type="text"/>
        </div>
    </div>
    <div class="layui-inline">
        <label class="layui-form-label">用户账号：</label>
        <div class="layui-input-block">
            <input placeholder="输入用户账号" id="account" name="account"
                   class="layui-input" type="text"/>
        </div>
    </div>
    <div class="layui-inline w300">
        <label class="layui-form-label">时间(起)：</label>
        <div class="layui-input-block">
            <input type="text" readonly id="startTime" name="startTime" class="layui-input"
                   placeholder="代理时间起">
        </div>
    </div>
    <div class="layui-inline w300">
        <label class="layui-form-label">时间(止)：</label>
        <div class="layui-input-block">
            <input type="text" readonly id="endTime" name="endTime" class="layui-input"
                   placeholder="代理时间止">
        </div>
    </div>
    <div class="layui-inline">
        <div class="layui-btn-group">
            <button onclick="queryPage();"
                    type="button" class="layui-btn">
                <i class="layui-icon">&#xe615;</i>查询
            </button>
            <button onclick="resetSearch();"
                    type="button" class="layui-btn layui-btn-danger">
                <i class="layui-icon">&#x1002;</i>重置
            </button>
            <button onclick="javascript:add();"
                    type="button" class="layui-btn layui-btn-normal">
                <i class="layui-icon">&#xe608;</i>新增
            </button>
        </div>
    </div>
</div>

<table id="myTable"></table>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="application/javascript">
    var table = layui.table;
    var myTable = table.render({
        elem: '#myTable'
        , method: 'POST'
        , height:570
        , url: '${pageContext.request.contextPath}/agent/user/page' //数据接口
        , limit: 10
        , limits: [10, 30, 50, 100, 200, 500]
        , request: {
            pageName: 'queryPage' //页码的参数名称，默认：page
            , limitName: 'querySize' //每页数据量的参数名，默认：limit
        }
        , skin: 'row' //行边框风格
        , even: true //开启隔行背景
        , page: true //开启分页
        , cols: [[ //表头
            {field: '', title: '操作',width:210, align: 'center', sort: false, fixed: 'left',templet: '#roleRemark'},
            {field: 'gameId', title: '游戏ID',width:80, align: 'center', sort: false, fixed: 'left'}
            , {field: 'account', title: '用户账号',width:140, align: 'center'}
            , {field: 'userName', title: '姓名',width:120, align: 'center', sort: true}
            , {field: 'agentLevel', title: '类型',width:100, align: 'center', sort: true,templet:'#agentLevel'}
            , {field: 'agentTimeStr', title: '代理时间',width:180, align: 'center', sort: true}
            , {field: 'profitRate', title: '分成比例',width:100, align: 'center', sort: true}
            , {field: 'subProfitRate', title: '二级分成比例',width:110, align: 'center', sort: true}
            , {field: 'profitTotal', title: '代理分成总金币',width:140, align: 'center', sort: true}
            , {field: 'totalOffer', title: '贡献总金币',width:110, align: 'center', sort: true}
            , {field: 'profitUsedTotal', title: '已提现金币',width:110, align: 'center', sort: true}
            , {field: 'canTotal', title: '可提现金币',width:110, align: 'center', sort: true}
        ]]
    });

    resetSearch = function () {
        $('#userName').val("");
        $('#account').val("");
        $('#startTime').val("");
        $('#endTime').val("");
        queryPage();
    };

    queryPage = function () {
        //这里以搜索为例
        myTable.reload({
            where: { //设定异步数据接口的额外参数，任意设
                userName: $('#userName').val(),
                account: $('#account').val(),
                startTime: $('#startTime').val(),
                endTime: $('#endTime').val()
            }
            , page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }

</script>
<script type="text/html" id="roleRemark">
    <button type="button" class="layui-btn layui-btn-sm"
            author="/plat/role/edit"
            onclick="edit('{{d.id}}')"><i class="layui-icon">&#xe642;</i>编辑
    </button>
    <button type="button" class="layui-btn layui-btn-sm layui-btn-danger"
            author="/plat/role/delete"
            onclick="javascript:subList('{{d.gameId}}');"><i class="layui-icon">&#xe613;</i>下级明细
    </button>
</script>


<script type="text/html" id="agentLevel">
    {{# if(d.agentLevel=='1'){}}
    <strong>一级代理</strong>
    {{# }else if(d.agentLevel=='2'){}}
    <strong>二级代理</strong>
    {{# }else if(d.agentLevel=='3'){}}
    <strong>玩家</strong>
    {{#}}}
</script>


<script type="text/javascript">

    layui.use('laydate', function () {
        var laydate = layui.laydate;

        var start = {
            elem: '#startTime',
            max: '2099-06-16 23:59:59',
            istoday: false,
            format: 'yyyy-MM-dd HH:mm:ss',
            type:'datetime',
            istime: true,
            done: function (value, date) {
                endTime.config.min = date; //开始日选好后，重置结束日的最小日期
                endTime.config.min.month = date.month - 1;//将月份减一（默认为下个月，减一可得到设置的原月份）
            }
        };
        var end = {
            elem: '#endTime',
            max: '2099-06-16 23:59:59',
            istoday: false,
            format: 'yyyy-MM-dd',
            format: 'yyyy-MM-dd HH:mm:ss',
            type:'datetime',
            istime: false,
            done: function (value, date) {
                startTime.config.max = date; //结束日选好后，重置开始日的最大日期
                startTime.config.max.month = date.month - 1;//将月份减一（默认为下个月，减一可得到设置的原月份）
            }
        };
        //    执行一个laydate实例
        var startTime = laydate.render(start);
        var endTime = laydate.render(end);
    });

    subList = function(no){
        location.href = "${pageContext.request.contextPath}/agent/user/subList?gameId="+no;
    }

    add = function () {
        location.href = "${pageContext.request.contextPath}/agent/user/toAdd";
    }
    edit = function (id) {
        location.href = '${pageContext.request.contextPath}/agent/user/toEdit?id=' + id;
    }

    layerCallBack = function () {
        reload();
    }

    reload = function () {
        queryPage();
    }
</script>
</body>
</html>
