<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta name="X-UA-Compatible" content="IE=EDGE,Chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <LINK rel="Bookmark" href="/favicon.ico">
    <LINK rel="Shortcut Icon" href="/favicon.ico"/>
    <title>提现申请列表</title>
    <%--引入公共js、css--%>
    <jsp:include page="../common/layerui.jsp">
        <jsp:param value="${contextPath}" name="contextPath"/>
    </jsp:include>
    <!--2017云项目样式改版-->
    <link rel="stylesheet" href="<%=request.getContextPath()%>/myResources/manager/plat/assets/css/jui.css"/>
</head>
<body style="padding: 20px;">
<div class="layui-form-item layui-form-pane">
    <form id="queryForm" class="layui-form layui-form-pane" method="post" action="<%=request.getContextPath()%>/withDraw/export">
        <div class="layui-inline">
            <label class="layui-form-label">用户姓名：</label>
            <div class="layui-input-block">
            <input placeholder="输入用户姓名" id="userName" name="userName"
                   class="layui-input" type="text"/>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">用户账号：</label>
            <div class="layui-input-block">
                <input placeholder="输入用户账号" id="account" name="account"
                       class="layui-input" type="text"/>
            </div>
        </div>
        <div class="layui-inline w300">
            <label class="layui-form-label">时间(起)：</label>
            <div class="layui-input-block">
                <input type="text" readonly id="startTime" name="startTime" class="layui-input"
                       placeholder="代理时间起">
            </div>
        </div>
        <div class="layui-inline w300">
            <label class="layui-form-label">时间(止)：</label>
            <div class="layui-input-block">
                <input type="text" readonly id="endTime" name="endTime" class="layui-input"
                       placeholder="代理时间止">
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-btn-group">
                <button onclick="queryPage();"
                        type="button" class="layui-btn">
                    <i class="layui-icon">&#xe615;</i>查询
                </button>
                <button onclick="resetSearch();"
                        type="button" class="layui-btn layui-btn-danger">
                    <i class="layui-icon">&#x1002;</i>重置
                </button>
                <button onclick="exportExcel();"
                        type="button" class="layui-btn layui-btn-warm">
                    <i class="layui-icon">&#x1002;</i>导出
                </button>
            </div>
        </div>
    </form>
</div>
<button onclick="auditBatch();"
        type="button" class="layui-btn layui-btn-normal">
    <i class="layui-icon">&#xe608;</i>批量审批
</button>
<table id="myTable"></table>
<%@ include file="../common/ajax_login_timeout.jsp" %>
<script type="application/javascript">
    var table = layui.table;
    var myTable = table.render({
        elem: '#myTable'
        , method: 'POST'
        , height:570
        , url: '${pageContext.request.contextPath}/agent/user/withDrawPage' //数据接口
        , limit: 10
        , limits: [10, 30, 50, 100, 200, 500]
        , request: {
            pageName: 'queryPage' //页码的参数名称，默认：page
            , limitName: 'querySize' //每页数据量的参数名，默认：limit
        }
        , skin: 'row' //行边框风格
        , even: true //开启隔行背景
        , page: true //开启分页
        , cols: [[ //表头
            {type:'checkbox'},
            {field: 'gameId', title: '游戏ID',width:180, align: 'center', sort: false}
            , {field: 'account', title: '用户账号',width:140, align: 'center'}
            , {field: 'userName', title: '姓名',width:120, align: 'center', sort: true}
            , {field: 'agentLevel', title: '类型',width:120, align: 'center', sort: true,templet:'#agentLevel'}
            , {field: 'agentTimeStr', title: '代理时间',width:180, align: 'center', sort: true}
            , {field: 'profitTotal', title: '代理分成总金币',width:140, align: 'center', sort: true}
            , {field: 'profitUsedTotal', title: '已提现金币',width:120, align: 'center', sort: true}
            , {field: 'canTotal', title: '可提现金币',width:120, align: 'center', sort: true}
            , {field: 'amount', title: '本次申请金币',width:120, align: 'center', sort: true}
            , {field: 'createTimeStr', title: '申请时间',width:120, align: 'center', sort: true}
            , {field: 'auditStatus', title: '状态',width:120, align: 'center', sort: true,templet:'#auditStatus'}
        ]]
    });

    exportExcel = function(){
        $("#queryForm").submit();
    }

    resetSearch = function () {
        $('#userName').val("");
        $('#account').val("");
        $('#startTime').val("");
        $('#endTime').val("");
        queryPage();
    };

    queryPage = function () {
        //这里以搜索为例
        myTable.reload({
            where: { //设定异步数据接口的额外参数，任意设
                userName: $('#userName').val(),
                account: $('#account').val(),
                startTime: $('#startTime').val(),
                endTime: $('#endTime').val()
            }
            , page: {
                curr: 1 //重新从第 1 页开始
            }
        });
    }

</script>

<script type="text/html" id="auditStatus">
    {{# if(d.auditStatus=='DSH'){}}
    <strong style="color:orange;">待审核</strong>
    {{# }else if(d.auditStatus=='SHTG'){}}
    <strong style="color:green;">已返现</strong>
    {{# }else if(d.auditStatus=='SHBTG'){}}
    <strong style="color:red;">审核不通过</strong>
    {{#}}}
</script>


<script type="text/html" id="agentLevel">
    {{# if(d.agentLevel=='1'){}}
    <strong>一级代理</strong>
    {{# }else if(d.agentLevel=='2'){}}
    <strong>二级代理</strong>
    {{# }else if(d.agentLevel=='3'){}}
    <strong>玩家</strong>
    {{#}}}
</script>


<script type="text/javascript">

    layui.use('laydate', function () {
        var laydate = layui.laydate;

        var start = {
            elem: '#startTime',
            max: '2099-06-16 23:59:59',
            istoday: false,
            format: 'yyyy-MM-dd HH:mm:ss',
            type:'datetime',
            istime: true,
            done: function (value, date) {
                endTime.config.min = date; //开始日选好后，重置结束日的最小日期
                endTime.config.min.month = date.month - 1;//将月份减一（默认为下个月，减一可得到设置的原月份）
            }
        };
        var end = {
            elem: '#endTime',
            max: '2099-06-16 23:59:59',
            istoday: false,
            format: 'yyyy-MM-dd',
            format: 'yyyy-MM-dd HH:mm:ss',
            type:'datetime',
            istime: false,
            done: function (value, date) {
                startTime.config.max = date; //结束日选好后，重置开始日的最大日期
                startTime.config.max.month = date.month - 1;//将月份减一（默认为下个月，减一可得到设置的原月份）
            }
        };
        //    执行一个laydate实例
        var startTime = laydate.render(start);
        var endTime = laydate.render(end);
    });

    layerCallBack = function () {
        reload();
    }

    reload = function () {
        queryPage();
    }

    auditBatch = function(){
        var checkStatus = table.checkStatus('myTable');
        var data = JSON.stringify(checkStatus.data);
        if(data=="[]"){
            layer_error("请至少选择一条要审批的记录");
            return;
        }
        var index = layer.confirm('请选择您要执行的操作，点击右上角取消本次操作。', {
            btn: ['通过','不通过'] //按钮
        }, function(){
            doAuditBatch("SHTG",data);
            layer.close(index);
        }, function(){
            doAuditBatch("SHBTG",data);
            layer.close(index);
        });
    }

    doAuditBatch = function(auditStatus,data){
        $.ajax({
            type: "post",
            url: "<%=request.getContextPath()%>/withDraw/auditBatch",
            data: {"jsonValue":data,"auditStatus":auditStatus},
            dataType: "JSON",
            traditional:true,
            async:false,
            success: function(data){
                if (data.head.respCode == 0) {
                    layer.msg("操作成功", {icon: 1,time:2000});
                    reload();
                } else {
                    layer_error(data.head.respMsg);
                }
            }
        });
    }


</script>
</body>
</html>
