//2015/7/28/c/x/l
;(function ($, window, document, undefined) {
	var defaults = {
		url:'',
		width: 700,
		height: 152,
		title:'',
		cross:0,
		button:true,
		close:function(){},
		cancel:function(){}
	};

	function Plugin(element, options){
		this.w  = window;
		this.el = $(element);
		this.options = $.extend({}, defaults, options);
		//定义穿越
		if(this.options.cross==1){
			this.w = this.w.parent;
		}
		if(this.options.cross==2){
			this.w = this.w.parent.parent;
		}
		this.init();
	}

	Plugin.prototype = {
		init: function(){
			var myModal ='<div class="modal fade" id="myModal" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel">'+
				'<div class="modal-dialog" role="document" style="width:'+this.options.width+'px;">'+
				'<div class="modal-content" style="width:'+this.options.width+'px;"></div></div></div>';
			var header = '<div class="modal-header" style="width:'+(this.options.width-2)+'px;">'+
				'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
				'<h4 class="modal-title" id="myModalLabel">'+this.options.title+'</h4></div>';
			var body = '<div class="modal-body" style="height:'+this.options.height+'px;width:'+this.options.width+'px;">'+
				'<iframe id="modalBodyIframe" name="modalBodyIframe" src="'+this.options.url+'" frameborder="0" height="100%" width="100%"></iframe></div>';
			var footer = '<div class="modal-footer" style="width:'+(this.options.width-2)+'px;">'+
				'<button type="button" class="btn btn-primary">保存</button>'+
				'<button type="button" class="btn btn-default" data-dismiss="modal" >关闭</button></div>';

			var targetWindow = this.w;
			var options = this.options;
			var button = this.options.button;
			if(!button){
				footer = '';
			}
			var content = this.options.content;
			if(content !=undefined && content != ''){
				body = '<div class="modal-body" style="height:'+this.options.height+'px;width:'+this.options.width+'px;">'+content+'</div>';
			}
			//生成动态元素
			var myModalDiv = targetWindow.document.getElementById("myModalDiv");
			if(myModalDiv == undefined){
				myModalDiv = targetWindow.document.createElement("div");
				myModalDiv.id = "myModalDiv";
			}
			myModalDiv.innerHTML=myModal;
			targetWindow.document.body.appendChild(myModalDiv);
			$(myModalDiv).find(".modal-content").html(header+body+footer);
			targetWindow.document.close();
			//注册保存事件
			$(myModalDiv).find(".btn-primary").click(function(){
				$(this).attr("disabled","disabled");
				targetWindow.modalBodyIframe.submitForm();
			});
			//注册关闭事件回调
			targetWindow.$('#myModal').on('hidden.bs.modal', function (e) {
				options.cancel();
			});
			targetWindow.$('#myModal').modal('show');
		},
		close:function(){
			this.options.close();
			//eval(this.options.callback);
			this.w.$('#myModal').modal('hide');
		}
	};

	$.fn.iframeModal = function(params){
		var plugin = $(window).data("iframeModal");
		if(plugin==undefined && typeof params === 'string'){
			//这边iframe的id暂时写死了,固定为home
			try {
				window.home.centerList1.$.fn.iframeModal("close");
			} catch (e) {
			}
			try {
				window.home.$.fn.iframeModal("close");
			} catch (e) {
			}
			return;
		}
		if (plugin!=undefined && typeof params === 'string' && typeof plugin[params] === 'function') {
			plugin[params]();
		}else{
			$(window).data("iframeModal", new Plugin(this, params));
		}
	};

	function getIE(){
		alert(navigator.appVersion);
		if(navigator.appName == "Microsoft Internet Explorer") {
			if(navigator.appVersion.match(/7./i)=='7.'){

				return true;
			}else {
				return false;
			}
		}
	}

	$.extend({
		"modalconfirm": function (title,content,fun) {
			parent.$.messager.confirm(title, content, function() {
				fun();
			});
		},
		"setFrameHeight":function(){
			var winHeight = document.body.clientHeight;
			var winHeight = document.getElementById("moduleContent").offsetHeight;
			if(winHeight<=700){winHeight=700;};
			parent.document.getElementById('centerList1').height=winHeight+120;
			parent.parent.document.getElementById('home').height=winHeight+80;
		}
	});
	//注册页面关闭事件,并设置iframe高度 586
	/*$(window).unload(function(){
		if(parent.parent.document.getElementById('home')!=undefined){
			parent.parent.document.getElementById('home').height=700;
		}else if(parent.document.getElementById('home') != undefined){
			parent.document.getElementById('home').height=700;
		}
	});*/
})(jQuery, window, document);
var winHeight = 700;
var winWidth = 700;
//	var list = parent.art.dialog.list;
//	art.dialog.data("state", "submit");
//	art.dialog.close();
//  art.dialog.top.art.dialog.tips('');
//  artDialog.alert('');