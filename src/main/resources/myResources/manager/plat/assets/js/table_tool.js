//工具JS


// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
};

var TABLE_TOOL = {};  //表格工具对象
TABLE_TOOL.format = {};   //数据格式化对象
//格式化金额
TABLE_TOOL.format.money = function (num, ext) {
    ext = ext === undefined || ext === null ? 2 : ext;  //默认保留2位小数
    if (ext < 0) {
        return num;
    }
    num = Number(num);
    if (isNaN(num)) {
        num = 0;
    }
    return num.toFixed(ext);
};
//装载 表格
TABLE_TOOL.loadGird = function (id, options) {
    if (!options.config) {
        options.config = {  //用于覆盖默认配置
            curpage: 'queryPage',
            rp: 'querySize',  //每页查询大小
            sortname: "orderBy",  //
            sortorder: "orderType",
            list: "records",  //结果列表名称
            queryPage: "queryPage", //查询页码名称
        }
    }
    var _table = $("#" + id).flexigrid(options);
    return _table;
};


//临时缓存对象
TABLE_TOOL.CACHE = {
    index: 0,
    max: 0,
    optionsNumber: 1,//保存的按钮配置的模板数据的数量
    options: [],  //保存将要注册点击事件的按钮配置（基于操作按钮）
    tableData: [],  //保存表格数据
    conversion: {}, //保存将要注册点击事件的按钮配置（基于数据转换）
    conversionParams: {},  //保存将要注册点击事件的参数（基于数据转换）
    clear: false,  //是否需要清除
};

//表格弹窗对象
TABLE_TOOL.alert = {};

/**
 * 正则表达式模板处理
 * @param str  要替换的字符串
 * @param map 数据
 * @returns {String}
 */
TABLE_TOOL.regReplace = function (str, map) {  //正则表达式模板处理
    str = str.replace(/\$\{([^\}]*)\}/g, "{{= $1}}").replace(/\{\{(\/?)(\w+|.)(?:\(((?:[^\}]|\}(?!\}))*?)?\))?(?:\s+(.*?)?)?(\(((?:[^\}]|\}(?!\}))*?)\))?\s*\}\}/g,
        //replace的自定义方法中的参数个数表明正则所匹配的分组成员的个数，一般第一个参数是匹配的整个字符串，也就是说，上面的这条正则分组成员应该是6个
        function (all, slash, type, fnargs, target, parens, args) {
            if (map[target] == null || map[target] === undefined) {
                throw new Error("未找到-->" + target + " 可替换的值!!");
            }
            return map[target];
        });
    return str;
};
//原数据转换
TABLE_TOOL.dataConversion = function (data, dataMap) {
    if (data === null || data === undefined || !dataMap) {
        throw new Error("参数不合法!");
        return false;
    }
    var key = "";
    var flag = false;  //是否需要延迟注册事件
    for (var x in dataMap) {  //遍历每一条json需要进行数据转换的 json数据的 key
        key = x;
        switch (dataMap[x].constructor) {
            case Object:  //是一个对象做数据转换
                var obj = dataMap[x];
                if (obj.map) {  //map属性存在
                    if (obj.map.constructor === String) {  //字符串
                        flag = true;
                        TABLE_TOOL.CACHE.conversionParams[x] ? -1 : TABLE_TOOL.CACHE.conversionParams[x] = [];  //创建一个数组对象
                        data = TABLE_TOOL.regReplace(obj.bind, {src: data});
                        var opt = {};
                        opt.fn = obj.fn;
                        opt.event = obj.event;
                        opt.flag = obj.flag;
                        opt.attr = x;
                        TABLE_TOOL.CACHE.conversion[x] = opt;

                    } else if (obj.map.constructor === Object) {
                        flag = true;
                        TABLE_TOOL.CACHE.conversionParams[x] ? -1 : TABLE_TOOL.CACHE.conversionParams[x] = [];  //创建一个数组对象
                        TABLE_TOOL.CACHE.conversionParams[x][TABLE_TOOL.CACHE.conversionParams[x].length] = data;   //缓存参数
                        data = TABLE_TOOL.regReplace(obj.bind, {
                            text: obj.map[data],
                            style: "font-size:inherit",
                            flag: "data-" + x
                        });
                        var opt = {};
                        opt.fn = obj.fn;
                        opt.event = obj.event;
                        opt.flag = "a[data-" + x + "]";
                        opt.attr = x;
                        TABLE_TOOL.CACHE.conversion[x] = opt;

                    }
                } else {
                    data = obj[data];  //数据中原本的值做为key
                }
                break;
            case Array:

                break;
            case String: //是一个字符串 直接交换数据的值
                var map = {};
                map[x] = data;
                data = TABLE_TOOL.regReplace(dataMap[x], map);
                break;
        }
    }
    if (flag) {
        return data;
    }

    var registerName = "register_" + key + "_event";
    clearInterval(window[registerName]);
    window[registerName] = window.setInterval(function () {
        var flag = 0;
        var objs = {};
        for (var z in TABLE_TOOL.CACHE.conversion) {
            var option = TABLE_TOOL.CACHE.conversion[z];
            try {
                objs[option.attr] = $(option.flag);
            } catch (e) {
                return;
            }
            if (!objs || objs.length == 0) {
                return;
            }
            flag++;
        }

        for (var x in objs) {
            objs[x].unbind(TABLE_TOOL.CACHE.conversion[x].event);
            objs[x].each(function (index) {
                $(this).attr("data-fn", x);
                if (TABLE_TOOL.CACHE.conversion[x].fn.constructor === Array) {
                    $(this).hover(function (e) {
                        var fn = $(this).attr("data-fn");
                        var array = TABLE_TOOL.CACHE.conversionParams[fn];
                        if (array.length > 0) {

                            var param = array[index];
                        } else {
                            var param = null;
                        }
                        TABLE_TOOL.CACHE.conversion[fn].fn[0].call(this, param);
                        e.stopPropagation();//阻止事件冒泡
                    }, function (e) {
                        var fn = $(this).attr("data-fn");
                        var array = TABLE_TOOL.CACHE.conversionParams[fn];
                        if (array.length > 0) {
                            var param = array[index];
                        } else {
                            var param = null;
                        }
                        TABLE_TOOL.CACHE.conversion[fn].fn[1].call(this, param);
                        e.stopPropagation();//阻止事件冒泡

                    });
                } else {
                    $(this).bind(TABLE_TOOL.CACHE.conversion[x].event, function (e) {
                        var fn = $(this).attr("data-fn");
                        var array = TABLE_TOOL.CACHE.conversionParams[fn];
                        if (array.length > 0) {
                            var param = array[index];
                        } else {
                            var param = null;
                        }
                        TABLE_TOOL.CACHE.conversion[fn].fn.call(this, param);
                        e.stopPropagation();//阻止事件冒泡

                    });
                }
            });
        }
        if (flag > 0) {
            clearInterval(window[registerName]);
        }


    }, 500);
    return data;  //返回数据
};

//默认提供的按钮样式映射
TABLE_TOOL.clzMap = {add: "plus", edit: "edit", del: "trash"};

/**
 * 获取表格操作按钮
 * @param options  配置项
 * @param data 表格JSON数据
 * @returns {String} 按钮组合
 */
TABLE_TOOL.clearCache = function () {
    delete TABLE_TOOL.CACHE;
    TABLE_TOOL.CACHE = {
        index: 0,
        max: 0,
        optionsNumber: 1,//保存的按钮配置的模板数据的数量
        options: [],  //保存将要注册点击事件的按钮配置（基于操作按钮）
        tableData: [],  //保存表格数据
        conversion: {}, //保存将要注册点击事件的按钮配置（基于数据转换）
        conversionParams: {}, //保存将要注册点击事件的参数（基于数据转换）;
        clear: false,  //是否需要清除
    };
};
TABLE_TOOL.getButtons = function (options, data) {
    if (TABLE_TOOL.CACHE.clear) {
        TABLE_TOOL.clearCache();
    }

    clearInterval(window["registerActEvent"]);
    var result = "";

    var bt_type_map = {
        icon: '<a class="m-l glyphicon glyphicon-${clz}" title="${title}" href="${click}" data-button="${clz}"></a>',
        text: '<a class="m-l"  href="${click}" data-button="${text}" data-val="${text}">${text}</a>',
        /*  button: '<a href="${click}" class="btn btn-outline btn-${clz}">${text}</a>'*/

    };

    TABLE_TOOL.CACHE.tableData[TABLE_TOOL.CACHE.tableData.length] = data;

    /**
     *
     * @param opt 每一个按钮的配置
     * @param data  表格其中一列的数据
     * @returns {String} 按钮html代码
     */
    var fn_iconButton = function (opt, data) {  //处理icon类类型按钮
        var bt = "";

        /*  if (opt.type == 'text') {  //文字类型按钮
         bt = bt_type_map['text'];
         } else if (opt.type == 'icon') {  //图标类型按钮
         bt = bt_type_map['icon'];
         }*/
        if (bt_type_map[opt.type] != undefined && bt_type_map[opt.type] != null) {
            bt = bt_type_map[opt.type];
        } else {
            throw new Error("参数:按钮类型错误!");
        }

        var clickAction = "";
        if (typeof opt.click === 'function') {  //是函数，动态注册一个在TABLE_TOOL对象上
            clickAction = "javascript:;";
        } else if (typeof opt.click === 'string') {  //字符串
            var usrMap = {};
            for (var i = 0; opt.column && i < opt.column.length; i++) {  //生成一个替换模型数据
                usrMap[opt.column[i]] = data[opt.column[i]];
            }
            clickAction = TABLE_TOOL.regReplace(opt.click, usrMap);
        } else {
            throw new Error("params click-->请传入function 或 string 类型");
        }

        var map = {};
        TABLE_TOOL.clzMap[opt.name] ? map.clz = TABLE_TOOL.clzMap[opt.name] : map.text = opt.name;
        map.click = clickAction;
        opt.title ? map.title = opt.title : map.title = "";
        bt = TABLE_TOOL.regReplace(bt, map);
        return bt;
    };


    if (options.constructor === Object) {  //对象 统一处理
        for (var x = 0; x < options.list.length; x++) {
            var opt = options.list[x];
            opt.type = options.type;
            if (opt.click.constructor === Function && TABLE_TOOL.CACHE.optionsNumber > 0) {  //保留点击触发事件的按钮 配置
                TABLE_TOOL.CACHE.options[TABLE_TOOL.CACHE.options.length] = opt;
            }
            result += fn_iconButton(opt, data);
        }

    } else if (options.constructor === Array) {  //数组类型 逐个处理
        for (var x = 0; x < options.length; x++) {  //遍历要增加的配置项
            var opt = options[x];
            if (opt.click.constructor === Function && TABLE_TOOL.CACHE.optionsNumber > 0) {  //模板配置保存份数: TABLE_TOOL.CACHE.optionsNumber
                TABLE_TOOL.CACHE.options[TABLE_TOOL.CACHE.options.length] = opt;
            }
            result += fn_iconButton(opt, data);
        }

    } else {
        throw new Error("getButtons params-->只能是Object 或者 Array!!");
    }
    TABLE_TOOL.CACHE.optionsNumber--;
    return result;
};
TABLE_TOOL.registerActEvent = function () {  //延迟注册;
    postPermiss();
    TABLE_TOOL.CACHE.clear = true;  //下一次在调用getButtons方法是需要清空CACHE

    var flag = TABLE_TOOL.CACHE.options.length;
    var objs = [];
    console.log(TABLE_TOOL.CACHE.options);
    for (var z = 0; z < TABLE_TOOL.CACHE.options.length; z++) {
        var option = TABLE_TOOL.CACHE.options[z];
        var cl_z = TABLE_TOOL.clzMap[option.name] ? TABLE_TOOL.clzMap[option.name] : option.name;
        try {
            objs[z] = $("a[data-button='" + cl_z + "']");
        } catch (e) {
            throw e;
            return;
        }
        if (!objs || objs.length == 0) {
            return;
        }
        flag--;
    }

    $(objs).each(function (fn_index) {
        // console.log(this);
        $(this).each(function (index) {
            // console.log(this);
            $(this).unbind("click").click(function (e) {
                var columns = {};
                var attrName;

                if (TABLE_TOOL.CACHE.options[fn_index].column) {
                    for (var i = 0; TABLE_TOOL.CACHE.options[fn_index].column && i < TABLE_TOOL.CACHE.options[fn_index].column.length; i++) {
                        attrName = TABLE_TOOL.CACHE.options[fn_index].column[i];
                        columns[attrName] = TABLE_TOOL.CACHE.tableData[index][attrName];
                    }
                    TABLE_TOOL.CACHE.options[fn_index].click.call(this, columns);
                } else {
                    TABLE_TOOL.CACHE.options[fn_index].click.call(this, TABLE_TOOL.CACHE.tableData[index]);
                }
                e.stopPropagation();//阻止事件冒泡
                delete columns;
                delete attrName;
            });
        });
    });
};

/**
 * 根据表单对象构造JS对象
 * @param selector：选择器
 */
TABLE_TOOL.getFromObj = function (selector) {
    var obj = {};
    var $from = $(selector);
    var _input = $from.find("input");
    var _select = $from.find("select");
    _input.each(function () {
        var name = $(this).attr("name");
        if (name) {
            obj[name] = $(this).val();
        }
    });

    _select.each(function () {
        var name = $(this).attr("name");
        if (name) {
            obj[name] = $(this).val();
        }
    });

    return obj;
};


/*
 基于 sweetalert.js参数

 title	提示框标题	-
 text	提示内容	-
 type	提示类型，有：success（成功），error（错误），warning（警告），input（输入）。	-
 allowOutsideClick	false	如果设置为“true”，用户可以通过点击警告框以外的区域关闭警告框。
 showCancelButton	是否显示“取消”按钮。	-
 animation	提示框弹出时的动画效果，如slide-from-top（从顶部滑下）等	-
 html	是否支持html内容。	-
 closeOnConfirm	true	如果希望以后点击了确认按钮后模态窗口仍然保留就设置为"false"。
 该参数在其他SweetAlert触发确认按钮事件时十分有用。
 timer	设置自动关闭提示框时间（毫秒）。	-
 showConfirmButton	是否显示确定按钮。	-
 confirmButtonText	定义确定按钮文本内容。	-
 imageUrl	定义弹出框中的图片地址。*/

//弹出一个警告对话框
//opt={title:"",text:"",confirm:function(){},confirmParam:{},cancel:function(){},cancelParam:{},}
TABLE_TOOL.alert.waring = function (opt) {
    swal({
        title: opt.title,
        text: opt.text,
        type: "warning",
        allowOutsideClick: "true",
        animation: "slide-from-top",   //slide-from-top slide-from-bottom
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "删除",
        cancelButtonText: "让我再考虑一下…",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (isConfirm) {  //点击的是否为确认按钮
            if (opt.confirm.constructor === Function) {
                opt.confirm.call(this, opt.confirmParam);  //确认按钮事件回调
            } else {
                throw new Error("参数 -->>confirm 不是一个函数!");
            }
        } else {
            if (opt.confirm.constructor === Function) {
                opt.cancel.call(this, opt.cancelParam);  //取消按钮事件回调
            } else {
                throw new Error("参数 -->>cancel 不是一个函数!");
            }
        }
    });
};


TABLE_TOOL.alert.tipsSuccess = function (msg, url, times) {
    var p = /<script[^\>]*?>([^\x00]*?)<\/script>/ig;
    times = !times? 3 : times;
    msg = msg.replace(p, '');

    var option = {
        msg:msg,
        mode: "succ",
        tilte: "提示消息",
        cover: true,
    };
    if(url){
        option.locationtime=times;
        option.func=function(){
            location.href=ADMIN_SITE_URL+url;
        }
    }else {
        option.closetime=times;
    }
    console.log(option);
    TABLE_TOOL.alert.showDialog(option);

};
TABLE_TOOL.alert.tipsError = function (msg, url, times) {
    var p = /<script[^\>]*?>([^\x00]*?)<\/script>/ig;
    times = !times ? 3 : times;
    msg = msg.replace(p, '');
    var option = {
        msg:msg,
        mode: "alert",
        tilte: "出错提示",
        cover: false
    };
    if(url){
        option.locationtime=times;
        option.func=function(){
            location.href=ADMIN_SITE_URL+url;
        }
    }else {
        option.closetime=times;
    }
    console.log(option);
    TABLE_TOOL.alert.showDialog(option);

};
TABLE_TOOL.alert.showDialog = function (opt) {
    showDialog(opt);
};
//自定义提示对话框
TABLE_TOOL.alert.customTips = function (id, title, opt) {
    opt.html = opt.html ? opt.html : "";

    if (opt.showCancelButton) {
        opt.html += "<a href='javascript:;' id='ok'class='ncap-btn-big ncap-btn-green w100 mr20'>确定</a><a href='javascript:;' id='no'class='ncap-btn-big ncap-btn-orange w100'>取消</a>";
    } else {
        opt.html += "<a href='javascript:;' id='ok'class='ncap-btn-big ncap-btn-green w100 mr20'>确定</a>";
    }
    var dialog = TABLE_TOOL.alert.custom(id, title, opt.html);
    $(dialog.dom.content).css({"text-align": "center", "padding": "15px 25px"});
    $(dialog.dom.wrapper).addClass("dialog-wrapper");
    $(dialog.dom.body).css("padding", "15 22px 5px");
    //$(dialog.dom.wrapper).css("margin-left", $(dialog.dom.wrapper).width() / -2 + "px");
    if ($("#ok")) {
        $("#ok").click(function () {
            if (opt.fn.constructor === Function) {
                opt.fn.apply(this);
                dialog.close();
            } else {
                dialog.hide();
            }
        });
    }
    if ($("#no")) {
        $("#no").click(function () {
            dialog.close();
        });
    }
    return dialog;
};

/* 显示Ajax表单 */
TABLE_TOOL.alert.ajax = function (id, title, url, width, model, param) {
    if (!width)    width = 480;
    if (!model) model = 1;
    var d = DialogManager.create(id);
    d.setTitle(title);
    d.setContents('ajax', url, param);
    d.setWidth(width);
    d.show('center', model);
    return d;
};
//显示一个内容为自定义HTML内容的消息
TABLE_TOOL.alert.custom = function (id, title, _html, width, model) {
    if (!width)    width = 480;
    if (!model) model = 0;
    var d = DialogManager.create(id);
    d.setTitle(title);
    d.setContents(_html);
    d.setWidth(width);
    d.show('center', model);
    return d;
};

TABLE_TOOL.alert.dropConfirm = function (msg, url) {  //基于浏览器的 confirm 对话框 确认后跳转页面
    if (confirm(msg)) {
        window.location = url;
    }
};

TABLE_TOOL.alert.ajaxConfirm = function (msg, url) {//基于浏览器的 confirm 对话框 确认后加载对话框
    if (confirm(msg)) {
        ajaxget(url);
    }
};

TABLE_TOOL.AJAX = {};  //AJAX请求对象

TABLE_TOOL.AJAX.interceptor = function () {
    $(document).ajaxSend(function (event, xhr, settings) {  //AJAX 请求发送前执行函数
        /*   console.log(event);
         console.log(xhr);
         console.log(settings);*/
        //console.log("AJAX SEND!");
        settings.datatype = "json";
    }).ajaxStart(function (event) {  //AJAX 请求开始时执行函数
        //console.log(event);
        // console.log("AJAX START!")
    }).ajaxComplete(function (event, xhr, settings) {  //AJAX 请求完成时执行函数
        /* console.log(event);
         console.log(xhr);
         console.log(settings);
         console.log("AJAX COMPLETE!")*/
    }).ajaxSuccess(function (event, xhr, settings) {  //AJAX 请求成功时执行函数

        if (settings.datatype != 'json') {  //值处理json数据
            return;
        }
        //jQuery格式化json的方法 还有 text html text script text xml 以及 text 等方法
        var data = settings.converters['text json'](xhr.responseText);
        if (!data.ajaxInterceptor) {  //表示响应的数据将被统一拦截
            return;
        }

        var defMsg = data.isSuccess ? "操作成功!" : "操作失败!";
        defMsg = data.message.trim().length > 0 ? data.message : defMsg;
        switch (data.action) {
            case "TIPS":
                data.isSuccess ? TABLE_TOOL.alert.tipsSuccess(defMsg) : TABLE_TOOL.alert.tipsError(defMsg);
                break;
            case "URL":
                data.isSuccess ? TABLE_TOOL.alert.tipsSuccess(defMsg, data.goToUrl) : TABLE_TOOL.alert.tipsError(defMsg, data.goToUrl);
                break;

        }


    }).ajaxError(function (event, xhr, settings) {    //AJAX 请求错误时执行函数
        /* var data = JSON.parse(xhr.responseText);
         if(!data.jsonType){
         return ;
         }*/
        //TABLE_TOOL.alert.tipsError("请求出现错误!!");
        console.info("请求出现错误!!")
    }).ajaxStop(function (event, xhr, settings) {  //AJAX 请求结束时执行函数

        console.log("AJAX STOP!")
    });
};

function getPrice(price){
    return price/100;
}

function getDate(time,pattern){
    if(time) {
        var date = new Date(time);
        return date.Format(pattern);
    }else{
        return "---";
    }
}
