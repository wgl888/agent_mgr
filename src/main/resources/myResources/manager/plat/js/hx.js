$.fn.smartFloat = function() {
    var position = function(element) {
        var top = element.offset().top;
        var left = element.offset().left;
        $(window).scroll(function() {
            var scrolls = $(this).scrollTop();
            if (scrolls > top) {
                if (window.XMLHttpRequest) {
                    element.css({
                        position: "fixed",
                        top: 0
                    }); 
                } else {
                    element.css({
                        top: scrolls - top
                    }); 
                }
            }else {
                element.css({
                    position: 'absolute',
                    top: top
                }); 
            }
        });
    };

    return $(this).each(function() {
        position($(this));                       
    });
};

function cont(u){
    $('#c').nextAll().remove();

    $.ajax({
        url : u,
        success : function(rs){
            $('#c').after(rs);
        },
        error : function(rs){
            for(var i in rs) {
                if (rs.hasOwnProperty(i)) {
                    $('#right').append( i + rs[i] +'<br />');
                }
            }
        }
    });
}

var mask        = $('#mask'),
    fsc_ck      = $('.fsc_ck'),
    left_nav    = $('#left_nav'),
    ln_a        = left_nav.find('a');

//打开口径说明方法
function tc(id,menuId)
{ return;
    var tempitem       = $(id),
    scrollHeight       = $(window.parent).scrollTop(),       // 获取当前窗口距离页面顶部高度
    windowHeight       = $(window.parent).height(),            // 获取当前窗口高度
    windowWidth        = $(document).width(),             // 获取当前窗口宽度
    popupHeight        = tempitem.height(),             // 获取弹出层高度
    popupWidth        = tempitem.width();              // 获取弹出层宽度
    posiTop            = ((windowHeight - popupHeight)/2 - 142) + scrollHeight;
    posiLeft           = (windowWidth - popupWidth)/2;
    var offsetTop = $(window.event.srcElement).offset().top;
	if(offsetTop == 1){
		if(parent.document.getElementById("left_nav")){
			tempitem.css({"top":posiTop + "px","left":posiLeft + "px","display":"block"});//设置position
		}else{
			tempitem.css({"top":(windowHeight - popupHeight)/2 + "px","left":posiLeft + "px","display":"block"});//设置position
		}
	}else{
		tempitem.css({"top":posiTop + "px","left":posiLeft + "px","display":"block"});//设置position
	}
	 
    var menuHtml = "<a href=\"javascript:copyDiamDesc('"+menuId+"')\" class=\"on\">快速复制口径说明</a>"
    	+"<a href=\"javascript:setDiamDesc('"+menuId+"')\">配置新口径</a>"
    	+"<a href=\"javascript:AddCalibe()\">配置新指标</a>";
    $("#kjsm_menu").html("");
    $("#kjsm_menu").html(menuHtml);
    $('#mask').show();
    var urls = base+"/gommgr/warning/prodwarning/getMenuRemark.action";
    $.ajax({
		  type:"POST",
		  url: urls,
		  data:'hashMap.MENU_ID='+menuId,
		  dataType: 'json', 
		  success:function(data){
    		document.all.MENU_REMARK.innerHTML="";
		  	if(data != ""){
		  		 $.each(data, function(i,val){
				      document.all.MENU_REMARK.innerHTML =  document.all.MENU_REMARK.innerHTML+"<br/>"+val.MENU_REMARK;
				  });   
		  	}else{
		  		document.all.MENU_REMARK.innerHTML = "暂无口径说明";
		  	}     
		  }
		  
		}
	);
}

//关闭口径说明方法
function shut()
{
    $('.tcs').hide();
    $('#mask').hide();
}


ln_a.click(function(){
    ln_a.removeClass('on');
    $(this).addClass('on');
});


var t_prev      = $('#t_prev'),
    t_next      = $('#t_next'),
    tabs        = $('#tabs'),
    tab_wrap    = $('#tab_wrap'),
    tab_e       = 0,
    tab_num     = 1,
    tab_click   = false,
    tabs_t      = 0,
    tabs_allow  = 0,
    tabs_w      = parseInt(tabs.width()/117);

function SetIframeHeight(mainFrame){
  var iframeid=document.getElementById(mainFrame); //iframe id
  if (document.getElementById){
  	if (iframeid && !window.opera){
    	if (iframeid.contentDocument && iframeid.contentDocument.body.offsetHeight){
    		if(iframeid.contentDocument.body.offsetHeight < (window.screen.availHeight - 152)){
    			iframeid.height = window.screen.availHeight - 152;
    			if(iframeid.contentDocument.getElementById("right")){
    				iframeid.contentDocument.getElementById("right").style.height = (window.screen.availHeight - 152) + "px";
    			}
    		}else{
	    		iframeid.height = iframeid.contentDocument.body.offsetHeight;
	    	}
    	}else if(iframeid.Document && iframeid.Document.body.scrollHeight){
    		if(iframeid.Document.body.scrollHeight < (window.screen.availHeight - 152)){
    			iframeid.height = window.screen.availHeight - 152;
	    		if(iframeid.contentDocument.getElementById("right")){
	    			iframeid.contentDocument.getElementById("right").style.height = (window.screen.availHeight - 152) + "px";
	    		}
		    }else{
	    		iframeid.height = iframeid.Document.body.scrollHeight;
	    	}
    	}
   	}
  }
}

//打开标签
function newtab(menu_id,tit, url)
{
	$('#top_menu').find('.a1').removeClass('on');
    $('#menu'+menu_id).addClass('on');
	if($('#bt_cont'+menu_id).length>0){
		tab_wrap.find('.tab').removeClass('on');
	    $('#bt_tit'+menu_id).addClass('on');
	    $('.bt').hide();
	    $('#bt_cont'+menu_id).show();
		if (menu_id==-1)
		document.frames('bt_cont'+menu_id).location.href=url;
		return;
	}

	if(menu_id>0){
		url=base+"/main/showMenuPage.action?hashMap.MENU_ID="+menu_id;
	}
    tab_wrap.find('.tab').removeClass('on');
    tab_wrap.append('<div id="bt_tit'+menu_id+'" onclick="changetab('+menu_id+')" class="tab on">'+tit+'<b onclick="shuttab('+menu_id+')">x</b></div>');
    $('.bt').hide();
    $('body').append('<iframe src="'+url+'" id="bt_cont'+menu_id+'" class="bt" onload="Javascript:SetIframeHeight(\'bt_cont'+menu_id+'\')" scrolling="no" frameborder="no"></iframe> ');
    //tab_num++;
}

//打开标签2
function newtab2(menu_id,tit, url)
{  
	$(window.parent.document).find('#top_menu').find('.a1').removeClass('on');
    $(window.parent.document).find('#menu'+menu_id).addClass('on');

	if($(window.parent.document).find('#bt_cont'+menu_id).length>0){
		$(window.parent.document).find('#tab_wrap').find('.tab').removeClass('on');
	    $(window.parent.document).find('#bt_tit'+menu_id).addClass('on');
	    $(window.parent.document).find('.bt').hide();
	    $(window.parent.document).find('#bt_cont'+menu_id).show();
		if (menu_id==-1)
		window.parent.document.frames('bt_cont'+menu_id).location.href=url;
		return;
	}

	if(menu_id>0){
		url=base+"/main/showMenuPage.action?hashMap.MENU_ID="+menu_id;
	}
    $(window.parent.document).find('#tab_wrap').find('.tab').removeClass('on');
    $(window.parent.document).find('#tab_wrap').append('<div id="bt_tit'+menu_id+'" onclick="changetab('+menu_id+')" class="tab on">'+tit+'<b onclick="shuttab('+menu_id+')">x</b></div>');
    $(window.parent.document).find('.bt').hide();
    $(window.parent.document).find('body').append('<iframe src="'+url+'" id="bt_cont'+menu_id+'" class="bt" onload="Javascript:SetIframeHeight(\'bt_cont'+menu_id+'\')" scrolling="no" frameborder="no"></iframe> ');
}

//关闭标签
function shuttab(e)
{
    $('#bt_tit'+e).removeAttr('onclick');
    var eq =  $('.tab').index($('#bt_tit'+e));
    if(eq == 0)
    {
        $('#bt_tit'+e).next().addClass('on');
        $('#bt_tit'+e).remove();
        $('#bt_cont'+e).nextAll('.bt').first().show();
        $('#bt_cont'+e).remove();
        return;
    }
    $('#bt_tit'+e).prev().addClass('on');
    $('#bt_tit'+e).remove();
    $('#bt_cont'+e).prevAll('.bt').first().show();
    $('#bt_cont'+e).remove();
}

//切换标签
function changetab(e)
{
    tab_wrap.find('.tab').removeClass('on');
    $('#bt_tit'+e).addClass('on');
    $('.bt').hide();
    $('#bt_cont'+e).show();
}

function Is_Tab_Click()
{
    tabs_t  = tabs.find('.tab').length;
    if(tabs_t <= tabs_w)
    {
        tab_click = false;
        return;
    }
    tab_click   = true;
    tabs_allow  = tabs_t - tabs_w;
}

t_prev.click(function(){
    if( tab_e >= 0 )
    {
        return;
    }

    Is_Tab_Click();

    if(!tab_click)
    {
        return;
    }

    tab_e++;
    tab_wrap.animate({left:'+=117px'});
})

t_next.click(function(){
    Is_Tab_Click();
    if(!tab_click)
    {
        return;
    }
    if( tab_e < -tabs_allow )
    {
        return;
    }
    tab_e--;
    tab_wrap.animate({left:'-=117px'});
})

//浮动层
$('#deploy').click(function(){
    $('#foot').toggleClass('on');
})