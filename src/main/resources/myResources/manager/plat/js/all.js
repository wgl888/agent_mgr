/**
 * Created by Sony on 2016/1/29.
 */
var oWidth=$(window).width();
var oHeight=$(window).height();
/*选项卡*/
function oSelect(selectArray,choice,value,Reverse){
    for(var i=0;i<$(selectArray[0]).length;i++){
        var Arr=$(selectArray[0]).length*10;
        $(selectArray[0]).eq(i).css("z-index", Arr-i);
        $(selectArray[0]).eq(i).find(choice).css("z-index", Arr-i);
    };
    if(Reverse){
    	if(Reverse[1]==true){  		
    		var oTopHeight=$(Reverse[0]).find(choice).height();
    		var l=$(Reverse[0]).find(choice).length+1;
    		for(var z=0;z<l;z++){
    			$(Reverse[0]).find(choice).eq(z).css("top",-(2+2*z)*oTopHeight+"px")
    		}		
    	}
    	if(Reverse[2]){
    		Reverse[2]();
    	}
    }
    $(selectArray[0]).on("click",function(){
        if($(this).hasClass(selectArray[1])){
            $(this).removeClass(selectArray[1]);
        }else{
        	$(this).addClass(selectArray[1]);
        }
    })
    $(selectArray[0]).find(choice).on("click",function(){
        $(this).parent(selectArray[0]).removeClass(selectArray[1]);
        $(this).siblings(value).text($(this).text());
        $(this).siblings(value).attr("value",$(this).attr("value"));
        return false;
    })
}
/*iframe高度*/
function iFrameHeight(iframeId, minHeight) {
    var browserVersion = window.navigator.userAgent.toUpperCase();
    var isOpera = false;
    var isFireFox = false;
    var isChrome = false;
    var isSafari = false;
    var isIE = false;
    var iframeTime;

    function reinitIframe(iframeId, minHeight) {
        try {
            var iframe = document.getElementById(iframeId);
            var bHeight = 0;
            if (isChrome == false && isSafari == false)
                bHeight = iframe.contentWindow.document.body.scrollHeight;

            var dHeight = 0;
            if (isFireFox == true)
                dHeight = iframe.contentWindow.document.documentElement.offsetHeight + 2;
            else if (isIE == false && isOpera == false)
                dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
            else
                bHeight += 3;
            var height = Math.max(bHeight, dHeight);
            if (height < minHeight) height = minHeight;
            iframe.style.height = height + "px";
        } catch (ex) {
        }
    }

    function startInit(iframeId, minHeight) {
        isOpera = browserVersion.indexOf("OPERA") > -1 ? true : false;
        isFireFox = browserVersion.indexOf("FIREFOX") > -1 ? true : false;
        isChrome = browserVersion.indexOf("CHROME") > -1 ? true : false;
        isSafari = browserVersion.indexOf("SAFARI") > -1 ? true : false;
        if (!!window.ActiveXObject || "ActiveXObject" in window)
            isIE = true;
        reinitIframe(iframeId, minHeight);
        if (iframeTime != null)
            clearInterval(iframeTime)
        iframeTime = window.setInterval(function(){
            reinitIframe(iframeId,minHeight)
        }, 100);
    }
         return startInit(iframeId, minHeight);
}
/*图片选择*/
function oImgScrollChoice(ImgScoll,ImgBox,leftBtn,rightBtn){
    var Num=ImgScoll.find(ImgBox).length;
    var ImgWidth=ImgScoll.find(ImgBox).width()+parseInt(ImgScoll.find(ImgBox).css("margin-right"));
    var moveLeft=null;
    ImgScoll.css("width",Num*ImgWidth+"px");
    leftBtn.on("click",function(){
        moveLeft=parseInt(ImgScoll.css("left"));
        moveLeftLimit=ImgScoll.parent().width()-ImgScoll.width();
        ImgScoll.stop();
        if(moveLeft<=moveLeftLimit){
            ImgScoll.css("left",moveLeftLimit+"px")
        }else{
            ImgScoll.animate({left:moveLeft-ImgWidth+"px"},300)
        }
    })
    rightBtn.on("click",function(){
        moveLeft=parseInt(ImgScoll.css("left"));
        ImgScoll.stop();
        if(moveLeft>=0){
            ImgScoll.css("left",0)
        }else{
            ImgScoll.animate({left:moveLeft+ImgWidth+"px"},300);
        }

    })

}
/*采购*/
function orderDate(){
    $(".PurchaseChoie dl dd").on("click",function(){
        $(this).hasClass("active")? $(this).removeClass("active"):$(this).addClass("active")
    })
    function ChoieOnePlus(objectOne,objectTwo){
        for(var i=0;i<$(objectOne).length;i++){
            if($(objectOne+":eq("+i+")").hasClass("active")){
                $(objectOne+":eq("+i+")").removeClass("active");
                $(objectTwo).append($(objectOne+":eq("+i+")"));
                ChoieOnePlus(objectOne,objectTwo);
            }
        }
    }
    $(".ChoieBtn .plus").on("click",function(){
        ChoieOnePlus(".ChoieOne dd",".ChoieTwo");
    })
    $(".ChoieBtn .delete").on("click",function(){
        ChoieOnePlus(".ChoieTwo dd",".ChoieOne");
    })
}
//数字键盘
function keypad(oInput,NumPlus,NumMinus){
    var	oNum = null;
    $(NumMinus).click(function(){
        oNum =$(oInput).val();
        if(oNum<=1){
            oNum==1
        }else{
            $(oInput).val(parseInt(oNum)-1);
        }
    })
    $(NumPlus).click(function(){
        oNum =$(oInput).val();
        $(oInput).val(parseInt(oNum)+1);
    });
    $(oInput).keyup(function(){
        if (!/^\d+$/.test($(this).val())){
            this.value = /^\d+/.exec(this.value);}
        return false;
    })
}
//输入框提示
function oClearInput(obj,color1,color2){
    var str={};
    for(var i=0;i<obj.length;i++){
        str[obj.eq(i).attr("name")]=obj.eq(i).val()
    }
    var oInputId=null;
    obj.focus(function(){
        $(this).css("color",color1);
        $(this).val("");
        $(this).blur(function(){
            if($(this).val()==""){
                oInputId=$(this).attr("name");
                $(this).val(str[oInputId]);
                $(this).css("color",color2);
            }
        })
    })
}
//当前样式判定及显示隐藏 /[对象 类名 显示隐藏元素],函数1，函数2
function oActive(arr,fn1,fn2){         
            $(arr[0]).click(function(){
                if($(this).hasClass(arr[1])){
                    $(this).removeClass(arr[1]);
                    if(arr[2]){
                        $(arr[2]).slideToggle();
                    }
                    if(fn1){
                        fn1($(this));
                    }
                }else{
                    $(this).addClass(arr[1]);
                    if(arr[2]){
                        $(arr[2]).slideToggle();
                    }
                    if(fn2){
                        fn2($(this));
                    }
                }
            })
        }
//切换
function oTab(arr,fun){
	$(arr[0]).click(function(){
		var i=$(arr[0]).index(this);
		$(arr[0]).removeClass(arr[1]);
		$(this).addClass(arr[1]);
		$(arr[2]).hide();
		$(arr[2]).eq(i).show();	
		if(fun){
			fun();
		}
	})
}
//右侧内容区宽度
function mainContentWidth(){
    		var mainContentWidth=setInterval(function(){
    			 $("#mainContent").css("width",parseInt($(document.body)[0].scrollWidth)-100+"px");
    		},30)
    	}