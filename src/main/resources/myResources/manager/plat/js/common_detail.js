// 专门用来跳转详情页

// 商户详细信息
companyDetail = function(id){
	var url = getRootPath() + "/company/detail?id="+ id;
	layer_default(url,"商户详情",800,600,this);
};
//分润明细3
orderSettlementList = function(time,objectId,objectName){
    changeTd(objectId);
    var url =getRootPath()+"/tasksettlement/orderSettlementList?settlementDTime="+time+"&objectId="+objectId+"&objectName="+objectName;
    $("#orderSettlementList",parent.document.body).attr("src",url);
};

//查看商户组织结构图
showTree = function(id){
	var url = getRootPath() + '/company/tree?id='+ id;
	layer_default(url,"详情",800,500,this);
};
showUpOrderList=function(aid,appID,payDTime,bankChannelID){
	changeTd(aid);
	var url=getRootPath()+'/uporder/list?payStatusCode=2&appId='+appID+'&payDTime='+payDTime+'&bankChannelID='+bankChannelID;
	$("#uporderList",parent.document.body).attr("src",url);
};
showUpOrderListByAppIdAndStatus=function(aid,appId,submitDTime,status){
	changeTd(aid+status);
	var url=getRootPath()+'/uporder/list?appId='+appId+'&submitDTime='+submitDTime+"&payStatusCode="+status;
	$("#uporderList",parent.document.body).attr("src",url);
};
//改变td样式
changeTd= function (id) {
	$("td[id]").removeClass("toggle");
	var v=document.getElementById(id);
	var td=$(v);
	td.addClass("toggle");
};


// 查看订单详细信息
showOrderDetail = function(orderNumber){
	var url = getRootPath() +  '/uporder/todetail?orderNumber='+ orderNumber;
	layer_default(url,"支付订单明细",800,600,this);
};

//支付通道明细
upBankDetail = function(id){
	var url = getRootPath()+'/upbankchannelreg/todetail?id='+ id;
	layer_default(url,"支付通道明细",800,600,this);
};

//代付通道明细
upBankTranDetail = function(id){
	var url = getRootPath()+'/upbanktransferchannelreg/todetail?id='+ id;
	layer_default(url,"代付通道明细",800,600,this);
};

//订单清算列表相应日期和支付状态的订单列表
showTotalOrder = function(appId,Time,payStatusCode){
    var url = getRootPath() +  '/uporder/list?payDTime='+ Time+'&payStatusCode='+payStatusCode+'&appId='+appId;
    layer_default(url,"支付订单",700,500,this);
};

//支付订单关于通道的支付情况查询
upOrderByBankChannel = function(bankChannelID,payStatusCode){
    var url = getRootPath() +  '/uporder/list?payStatusCode='+payStatusCode+'&bankChannelId='+bankChannelID;
    layer_default(url,"支付订单",1000,600,this);
};

//日支付统计
showPayOrder = function(time,bankChannelID){
	var url = getRootPath() +  '/paystatistics/payAmountList?payDTime='+ time+'&bankChannelID='+bankChannelID;
	layer_default(url,"日支付金额",1000,700,this);
};

//查看代付订单详情
toUpTransferDetail = function(orderNumber){
	var url = getRootPath() + '/uptransfer/todetail?orderNumber='+ orderNumber;
	layer_default(url,"详情",800,500,this);
};


// 清算日志详细信息
showSettlementLog = function (taskSettlementCode) {
	layer_default(getRootPath() + "/tasksettlement/log?taskSettlementCode=" + taskSettlementCode,
		"分润过程", 900, 380, this);
};

//代付金额明细
showUpTransferSplit = function (dealDTime) {
    var url = getRootPath()+'/uptransfer/upTransferSplitDetail?dealDTime='+dealDTime;
    layer_title_full(url,"当日代付金额明细" + "[" + dealDTime + "]"  + "");
};

//代付金额明细查询
showUpTransfer = function(dealDTime,appID,bankChannelId){
    changeTd(appID);
    var url =getRootPath()+"/uptransfer/list?dealDTime="+dealDTime+"&appID="+appID+"&bankChannelId="+bankChannelId;
    $("#list",parent.document.body).attr("src",url);
};

$(document).ready(function(){
// 帮助详细信息
	$(".help-button").click(function(){
		var tip = $(this).attr('data');
		parent.layer.open({
			title: '帮助'
			,content: tip
		});
	});
});

showCompanyBalance=function(id){
	var url =getRootPath()+"/company/taskBalance/list?companyId="+id;
	layer_title_full(url,"商户余额详情");
};

showAudit=function(id,companyName){
	var url =getRootPath()+"/company/companyAuditDetail?companyId="+id;
	layer_default(url,"[<span style='color: orange'>"+companyName+"</span>"+"] &nbsp;商户进件审核记录",800,600,this);
};

//通知实例跳转到处理接口
toDealPage = function(noticeExampleCode){
    var url = getRootPath() + '/companyAudit/check?noticeExampleCode='+ noticeExampleCode;
	popup_button_btn('处理页面',url,1000,600,this,"确认提交","返回");
};

noticeContent = function (noticeExampleCode) {
    parent.layer.open({
        type: 2,
        btn: ["我知道了"],
        area: ["800px", "500px"],
        shade: 0.1,
        title: "通知内容",
        content: getRootPath()+"/noticeExample/noticeContent?noticeExampleCode=" + noticeExampleCode,
        yes: function (index, layero) {
            $.ajax({
                type: "POST",
                url: getRootPath()+ "/noticeExample/doEdit",
                dataType: "json",
                cache: false,
                data: {"noticeExampleCode": noticeExampleCode, "isRead": "Y"},
                timeout: 40000,
                success: function (value) {
                    if (value.head.respCode == 0) {
                        parent.layer.close(index);
                        loadPage();
                    } else {
                        layer.msg(value.head.respMsg, {icon: 2, time: 2000});
                    }
                },
                complete: function (XMLHttpRequest, status) {
                    if (status == 'timeout') {
                        xhr.abort();    // 超时后中断请求
                    }
                }
            });
        }
    });
}