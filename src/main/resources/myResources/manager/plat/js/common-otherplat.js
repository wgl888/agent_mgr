	/**弹框1  url 默认提交和取消两个按钮*/
	   /*
	  	参数解释：
	  	title	标题
	  	url		请求的url
	  	id		需要操作的数据id
	  	w		弹出层宽度（缺省调默认值）
	  	h		弹出层高度（缺省调默认值）
	  */
	  function layer_show(title,url,w,h,dom){
	  	if (title == null || title == '') {
	  		title=false;
	  	};
	  	if (url == null || url == '') {
	  		url="404.html";
	  	};
	  	if (w == null || w == '') {
	  		w=800;
	  	};
	  	if (h == null || h == '') {
	  		h=($(window).height() - 50);
	  	};
	  	if(w.indexOf("%")>-1){//宽高使用%做单位
	  		console.log(w);
	  		var index = layer.open({
		  		type: 2,
				btn:["提交","取消"],
		  		area: [w,h],
		  		shade:0,
				offset:'t',
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});
	  	}else{ //宽高使用px做单位
		  	var index = layer.open({
		  		type: 2,
				btn:["提交","取消"],
		  		area: [w+'px',h+'px'],
		  		shade:0.1,
				offset:'5px',
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});
	  	}

	  }
    /**弹框2 - 自定义按钮*/
	  /*
	  	参数解释：
	  	title	标题
	  	url		请求的url
	  	id		需要操作的数据id
	  	w		弹出层宽度（缺省调默认值）
	  	h		弹出层高度（缺省调默认值）
	  */
	  function layer_show_btns(title,url,w,h,dom,btns){
	  	if (title == null || title == '') {
	  		title=false;
	  	};
	  	if (url == null || url == '') {
	  		url="404.html";
	  	};
	  	if (w == null || w == '') {
	  		w=800;
	  	};
	  	if (h == null || h == '') {
	  		h=($(window).height() - 50);
	  	};
	  	if(w.indexOf("%")>-1){//宽高使用%做单位
		  	var index = layer.open({
		  		type: 2,
				btn: btns,
		  		area: [w,h],
		  		shade:0.4,
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});	
	  	}else{
		  	var index =layer.open({
		  		type: 2,
				btn: btns,
		  		area: [w+'px',h+'px'],
		  		shade:0.4,
		  		title: title,
		  		content: url,
				yes : function(index,layero){
					var iframeWin = layero.find('iframe');
					$(iframeWin)[0].contentWindow.layer_submit(dom);
				}
		  	});	
	  	}
  
	  }
    /**弹窗3，内容为html元素*/
	function layer_show_cushtml(title,w,h,htmlContent){
		if (title == null || title == '') {
			title=false;
		};
		if (htmlContent == null || htmlContent == '') {
			htmlContent="";
		};
		if (w == null || w == '') {
			w=800;
		};
		if (h == null || h == '') {
			h=($(window).height() - 50);
		};
		var index = layer.open({
			type: 1,
			area: [w+'px',h+'px'],
			shade:0.4,
			title: title,
			content: '<div class="setlayer" style="margin:10px">'+htmlContent+'</div>'
		});
	}
    /**弹窗4，内容为html，自定义按钮*/
	function layer_show_html(title,htmlContent,w,h,dom,btns){
        if (title == null || title == '') {
            title=false;
        };
        if (htmlContent == null || htmlContent == '') {
            htmlContent="";
        };
        if (w == null || w == '') {
            w=800;
        };
        if (h == null || h == '') {
            h=($(window).height() - 50);
        };
		var index = layer.open({
			type: 1,
			btn: btns,
			area: [w+'px',h+'px'],
			shade:0.4,
			title: title,
			content: '<div class="setlayer" style="margin:10px">'+htmlContent+'</div>'
		});
	}
    /**弹窗5，内容为url，默认关闭按钮*/
    function layer_show_url(title,url,w,h){
        if (title == null || title == '') {
            title=false;
        };
        if (w == null || w == '') {
            w=800;
        };
        if (h == null || h == '') {
            h=($(window).height() - 50);
        };
        var index = layer.open({
            type: 2,
            btn: ["关闭"],
            area: [w+'px',h+'px'],
            shade:0.4,
            title: title,
            content: url
        });
    }
    /**弹窗6，内容为函数，传递按钮信息*/
    function layer_show_func_btns(title,url,w,h,btns,Func){
        if (title == null || title == '') {
            title=false;
        };
        if (url == null || url == '') {
            url="404.html";
        };
        if (w == null || w == '') {
            w=800;
        };
        if (h == null || h == '') {
            h=($(window).height() - 50);
        };
        var index = layer.open({
            type: 2,
            btn: btns,
            area: [w+'px',h+'px'],
            shade:0.4,
            title: title,
            content: url,
            yes : function(index,layero){
                Func(index);
            }
        });
    }
    /**弹窗7，内容为html，传递按钮信息*/
    function layer_show_html_btns(title,htmlContent,w,h,btns,Func){
        if (title == null || title == '') {
            title=false;
        };
        if (w == null || w == '') {
            w=800;
        };
        if (h == null || h == '') {
            h=($(window).height() - 50);
        };
        var index = layer.open({
            type: 1,
            btn: btns,
            area: [w+'px',h+'px'],
            shade:0.4,
            title: title,
            content: '<div class="setlayer" style="margin:10px">'+htmlContent+'</div>',
            yes : function(index,layero){
                Func(index);
            }
        });
    }
	  /**关闭弹出框口*/
	  function layer_close(){
	  	console.log(window.name);
          var index = layer.getFrameIndex(window.name);
          layer.close(index);
	  }

    /**错误信息1*/
    function layer_error_msg(msg){
        layer.msg(msg, {icon: 2,time:2000});
    }
    /**正确信息1*/
    function layer_success_msg(msg){
        layer.msg(msg, {icon: 1,time:2000});
    }
	  /**错误信息2*/
      function layer_error(msg){
          layer.msg(msg, {icon: 2,time:2000,offset:'5px'});
      }
	  /**正确信息2*/
      function layer_success(msg){
          layer.msg(msg, {icon: 1,time:2000,offset:'5px'});
      }
    /**错误信息3*/
    function layer_error_msg_offset(msg,offset){
        layer.msg(msg, {icon: 2,time:2000,offset:offset+'px'});
    }
    /**正确信息3*/
    function layer_success_msg_offset(msg,offset){
        layer.msg(msg, {icon: 1,time:2000,offset:offset+'px'});
    }
      /**提示信息*/
	function show_layermsg(msg,params) {
		layer.msg(msg, params);
	}
	/**关闭弹层*/
	function close_layer(index) {
		layer.close(index);
    }
	/**confirm方法*/
	function layer_confirm(msg,params,Func){
        layer.confirm(msg,params,
			function () {
                Func();
        	}
        );
	};

      /******* **************************************************************************************/
      //从cookie中获取主题样式，更换主题body 20170324
      function changeBodyClass() {
          var className = getCookie('TclassName');
          if(className!=''){
              $("body").removeClass();
              $("body").addClass(className);
		  }
      }

      window.onload=function(){
    	  changeBodyClass();
      }

      //保存cookie
      function setCookie(c_name,value,expiredays)
      {
          var exdate=new Date()
          exdate.setDate(exdate.getDate()+expiredays)
          document.cookie=c_name+ "=" +escape(value)+
              ((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
      }
      //获取cookie
      function getCookie(c_name)
      {
          if (document.cookie.length>0)
          {
              c_start=document.cookie.indexOf(c_name + "=")
              if (c_start!=-1)
              {
                  c_start=c_start + c_name.length+1
                  c_end=document.cookie.indexOf(";",c_start)
                  if (c_end==-1) c_end=document.cookie.length
                  return unescape(document.cookie.substring(c_start,c_end))
              }
          }
          return ""
      }