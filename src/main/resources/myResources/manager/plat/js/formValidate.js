﻿var formValidate ={
     isNull:function(str){
    	 if (str == '' || str.trim() == '') {
			return true;
		 }
    	 return false;
     },
	 lengthValidate:function(obj,length) {
		 var byteLen = 0, wordvalue=obj.val();		
		 len = wordvalue.length;
		 for( var i=0; i<len; i++ ){
			 byteLen += wordvalue.charCodeAt(i) > 255 ? 2 : 1;
			 if(byteLen>length)
				 return false;
		 }
		 return true;
	 },
	 // 判断是否为数字
	 // 是:返回true
	 // 否:返回false
	 isNumber:function(s) {
		var regu = "^[0-9]+$";
		var re = new RegExp(regu);
		if (s.search(re) == -1) {
			return false;
		} else {
			return true;
		}
	},
	patternValidate:function(value,pattern) {
		if(!pattern.test(value)) {
			return false;
		}
		return true;
	}
}