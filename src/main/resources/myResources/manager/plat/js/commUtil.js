﻿	function artNewWindow2(title, url, width, height,closeFun) {
    	art.dialog.open(url, {
    		title:title,
    		width:width,
    		height:height,
    		resize:true,
    		fixed: true,
    		opacity: 0.7,
    		lock: true,
    		close : function () {
    			if(art.dialog.data("state")=="submit"){
    				art.dialog.top.art.dialog.tips('执行成功');
        			eval(closeFun);
    			}
    			art.dialog.data("state", "close");
    		}
    	}, false);
    }	
	function artNewWindow3(title, url, width, height,closeFun) {
		    art.dialog.open(url, {
	    		title:title,
	    		width:width,
	    		height:height,
	    		resize:true,
	    		fixed: true,
	    		opacity: 0.7,
	    		lock: true,
	    		button: [ {
	    			name: '确定',
    			focus: true,
    		    callback: function () {
    	        	var iframe = this.iframe.contentWindow;
    	 	    	if (!iframe.document.body) {
    	 	        	alert('iframe还没加载完毕呢，请稍后！ ');
    	 	        	return false;
    	 	        };
    	 	        iframe.submitForm();
    	 			return false;
    		    }
    		}],
    		// ~ 关闭窗口时，刷新页面.
    		cancel: function () {
    	       	return true;
    	    },
    		close : function () {
    			if(art.dialog.data("state")=="submit"){
    				art.dialog.top.art.dialog.tips('执行成功');
        			eval(closeFun);
    			}
    			art.dialog.data("state", "close");
    		}
    	}, false);
    }
	// 全选
	function allSelect() {
		var labIds = document.getElementsByName("J_Item_Id");
		for (var i=0; i<labIds.length; i++) {
		   if(!labIds[i].disabled) {
			labIds[i].checked =  true; 
		   }
		}
	}
	//反选		
	function fSelect() {
		var labIds = document.getElementsByName("J_Item_Id");
		for (var i=0; i<labIds.length; i++) {
		   if(!labIds[i].disabled)
		   {
			if(labIds[i].checked){
			  labIds[i].checked =  false; 
			}else{
			   labIds[i].checked =  true; 
			}
		   } 
		}
	}			
	// 不选
	function noSelect() {
		var labIds = document.getElementsByName("J_Item_Id");
		for (var i=0; i<labIds.length; i++) {
			labIds[i].checked =  false; 
		}
	}
	function chooseCheck(obj){
		 if(obj.checked){
			 allSelect(); 
		 }else{
			 noSelect();
		 }
	}		
	//ajax提交
	function doAjaxText(url,params,callBackFun,datatype){
		if ( datatype == undefined ){
			datatype = 'text';
		}
		$.ajax( {
			type : 'post',
			data : params,
			url : url,
			dataType : datatype,
			timeout : 60000,
			async: false,
			success : function(resultCode) {
				callBackFun(resultCode);
			}
		});
	}	
	//获得选择的value 都好分割
	function getSelectValues(){
		var strs = "";
		var labIds = document.getElementsByName("J_Item_Id");
		for ( var i = 0; i < labIds.length; i++) {
			if (labIds[i].checked) {
				if (strs == '') {
					strs = strs + labIds[i].value;
				} else {
					strs = strs + ',' + labIds[i].value;
				}
			}
		}
		return strs;
	}
	String.prototype.trim = function(){   
	    return this.replace(/(^\s*)|(\s*$)/g, "");   
	};   
	//过滤特殊字符--跨站脚本漏洞(XSS)
	String.prototype.filterXSS = function(){
		return this.replace(/[<>"'%;()&+-\/\\]+/g,"");  
	};
	
	//去除回车空格换行
	function iGetInnerText(testStr) {
        var resultStr = testStr.replace(/[\r\n\t]/g, ""); //去掉回车换行tab
        return resultStr;
    }
	//iframe高度自适应
	function resizeIframe(frameId){
		var ifm= document.getElementById(frameId);   
		var subWeb = document.frames ? document.frames[frameId].document : ifm.contentDocument;   
		if(ifm != null && subWeb != null) {
		   ifm.height = subWeb.body.scrollHeight + 15;
		   ifm.width = subWeb.body.scrollWidth;
		}   
	}
	