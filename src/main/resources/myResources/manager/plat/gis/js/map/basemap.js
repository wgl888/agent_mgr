var map;								//地图对象
var maplayers = new Array();			//图层数组
var drawingManager;						//拖动框选
var boundOld = null;					//显示范围-旧
var zoomSize = new Object();			//比例缩放尺度
var distTool;
var legendCtrl;							//标题控件
var menuMap;							//地图右键菜单
var gridLayer = null;					//网格图层
var locationMarker = null;			    //定位节点
var isMarquee = false;			        //是否有滚动条
var isLoadZoom = false;			        //是否缩放加载数据

//初始化地图
function initmap(mapinfo) {
	var MapOptions = {
		enableMapClick : false,
		minZoom : parseInt(mapinfo.minzoom),
		maxZoom : parseInt(mapinfo.maxzoom)
	};
	map = new BMap.Map("mapdiv", MapOptions);
	map.mapinfo = mapinfo;
	var point = new BMap.Point(parseFloat(mapinfo.initpx), parseFloat(mapinfo.initpy));
	map.centerAndZoom(point, parseInt(mapinfo.initzoom));
	map.addControl(new BMap.NavigationControl({offset : new BMap.Size(5, 20)}));
	map.addControl(new BMap.ScaleControl({anchor:BMAP_ANCHOR_TOP_RIGHT,offset : new BMap.Size(5, 5)}));
	if(mapinfo.overview == 'true'){
		map.addControl(new BMap.OverviewMapControl());
	}
	map.enableScrollWheelZoom(); 
	map.disableDoubleClickZoom();
	map.enableKeyboard(); 
	initLngLatShow();
	initMapMenu();
	initFitlerOverlay();
	iniZoomSize();
	if(mapinfo.tool != null && mapinfo.tool == "true"){
		initTools();
	}
	if(mapinfo.locationMarker != null && mapinfo.locationMarker == "true"){
		map.addEventListener("click", function(e) {
			if(locationMarker){
				map.removeOverlay(locationMarker);
				locationMarker = null;
			}
			locationMarker = new BMap.Marker(new BMap.Point(e.point.lng,e.point.lat));
			map.addOverlay(locationMarker); 
		});
	}
	
//	map.setMapType(BMAP_HYBRID_MAP);
	var mapTypeControl = new BMap.MapTypeControl({mapTypes: [BMAP_NORMAL_MAP,BMAP_HYBRID_MAP]});
	mapTypeControl.setAnchor(BMAP_ANCHOR_TOP_LEFT);
	mapTypeControl.setOffset(new BMap.Size(55, 30));
	map.addControl(mapTypeControl);
}

//初始化标题
function initlegend(legendinfo) {
	function LegendControl(legendinfo){
		this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
		this.defaultOffset = new BMap.Size(0, 0);
		this._legendinfo = legendinfo;
	}
	LegendControl.prototype = new BMap.Control();
	LegendControl.prototype.initialize = function(map){
		var divMain = document.createElement("div");
		for(var i=0;i<this._legendinfo.length;i++){
			var div = document.createElement("div");
			div.style.display = "inline";
			div.style.marginLeft = "3px";
			div.style.paddingLeft = "8px";
			div.style.paddingRight = "8px";
			div.style.backgroundColor = "#" + this._legendinfo[i].bgcolor;
			div.setAttribute("title", this._legendinfo[i].label);
//			div.style.border = "1px solid gray";
	  		var lbl = document.createElement("label");
	  		lbl.style.color = "#" + this._legendinfo[i].color;
	  		lbl.style.fontSize="12px";
	  		lbl.innerText = this._legendinfo[i].label;
	  		div.appendChild(lbl);
	  		divMain.appendChild(div);
		}
		map.getContainer().appendChild(divMain);
	  	return divMain;
	};
	if(legendCtrl != null){
		map.removeControl(legendCtrl);
		legendCtrl = null;
	}
	legendCtrl = new LegendControl(legendinfo);
	map.addControl(legendCtrl);
}

//初始化重置按键
function initReloadBtn() {
	function ReloadControl(){
		this.defaultAnchor = BMAP_ANCHOR_BOTTOM_LEFT;
		this.defaultOffset = new BMap.Size(8, 3);
	}
	ReloadControl.prototype = new BMap.Control();
	ReloadControl.prototype.initialize = function(map){
		var div = document.createElement("div");
		div.style.display="block";
		div.style.cursor="pointer";
		div.setAttribute("title", "重置地图");
	  	var imgFilter = document.createElement("img");
	  	imgFilter.src = getImagePath() + "refresh.png";
	  	imgFilter.style.width = "40px";
	  	div.appendChild(imgFilter);
		map.getContainer().appendChild(div);
		var isShowFilter = true;
		imgFilter.onclick=function(e){
			initPoint();
			doRefreshMap();
		};
	  	return div;
	};
	var reloadCtrl = new ReloadControl();
	map.addControl(reloadCtrl);
}

//初始化过滤
function initfilter(filterInfo) {
	function FilterControl(filterInfo){
		var offsetX = 0;
		if(filterInfo.offsetX != null && isMarquee){
			offsetX = parseInt(filterInfo.offsetX);
		}
		var offsetY = 0;
		if(filterInfo.offsetY != null && isMarquee){
			offsetY = parseInt(filterInfo.offsetY);
		}
		this.defaultAnchor = BMAP_ANCHOR_BOTTOM_LEFT;
		this.defaultOffset = new BMap.Size(offsetX, offsetY);
	}
	FilterControl.prototype = new BMap.Control();
	FilterControl.prototype.initialize = function(map){
		var div = document.createElement("div");
		div.style.display="block";
		div.style.backgroundColor="white";
		div.style.border = "1px solid gray";
		div.style.cursor="pointer";
		div.setAttribute("title", "过滤器");
	  	var imgFilter = document.createElement("img");
	  	imgFilter.src = getImagePath() + "filter.png"
	  	div.appendChild(imgFilter);
		map.getContainer().appendChild(div);
		var isShowFilter = true;
		imgFilter.onclick=function(e){
			var data =  isShowFilter + "," + (e.clientX+5) +"," + (document.body.offsetHeight - e.clientY + 5);
//			doCallZK('$winGIS','onClickFilter',data);
			isShowFilter = !isShowFilter;
			div.style.backgroundColor=(isShowFilter ? "white":"gray");
		};
	  	return div;
	};
	var filterCtrl = new FilterControl(filterInfo);
	if(filterInfo.visible == 'true' ){
		map.addControl(filterCtrl);
	}
}

//初始化统计
function initcalc(calcInfo) {
	function CalcControl(calcInfo){
		var offsetX = 0;
		if(calcInfo.offsetX != null && isMarquee){
			offsetX = parseInt(calcInfo.offsetX);
		}
		var offsetY = 0;
		if(calcInfo.offsetY != null && isMarquee){
			offsetY = parseInt(calcInfo.offsetY);
		}
		this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
		this.defaultOffset = new BMap.Size(offsetX, offsetY);
		this._calcInfo = calcInfo;
	}
	CalcControl.prototype = new BMap.Control();
	CalcControl.prototype.initialize = function(map){
		var div = document.createElement("div");
		div.style.display="block";
		div.style.backgroundColor="white";
		div.style.border = "1px solid gray";
		div.style.cursor="pointer";
		div.setAttribute("title", "统计信息");
	  	var imgFilter = document.createElement("img");
	  	imgFilter.src = getImagePath() + "hide.png";
	  	div.appendChild(imgFilter);
		map.getContainer().appendChild(div);
		var isShowFilter = true;
		imgFilter.onclick=function(e){
			var w = parseInt(calcInfo.width);
			var h = parseInt(calcInfo.height);
			var info = new Object();
			info.calcPX = (e.clientX - w - 5);
			info.calcPY = (e.clientY - h - 5);
			info.isShowFilter = isShowFilter;
			createCalc(info);
			isShowFilter = !isShowFilter;
			div.style.backgroundColor=(isShowFilter ? "white":"gray");
			imgFilter.src=(isShowFilter ? getImagePath() + "hide.png": getImagePath() + "show.png");
		};
	  	return div;
	};
	var calcCtrl = new CalcControl(calcInfo);
	map.addControl(calcCtrl);
	
}

//初始化查询按键
function initSearch(searchInfo) {
	function SearchControl(searchInfo){
		this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
		var main = window.document.getElementById('mapdiv');
		this.defaultOffset = new BMap.Size(0, main.offsetHeight/2 - 13);
		this._searchInfo = searchInfo;
	}
	SearchControl.prototype = new BMap.Control();
	SearchControl.prototype.initialize = function(map){
		var div = document.createElement("div");
		div.style.display="block";
		div.setAttribute("class", "ctrl-btn");
		div.setAttribute("title", "查询界面");
	  	var imgFilter = document.createElement("img");
	  	imgFilter.src = getImagePath() + "expand.png";
	  	div.appendChild(imgFilter);
		map.getContainer().appendChild(div);
		var isShowFilter = true;
		div.onclick=function(e){
			var data = isShowFilter;
//			doCallZK('$winGIS','onClickSearch',data);
			isShowFilter = !isShowFilter;
			imgFilter.src=(isShowFilter ? getImagePath() + "expand.png": getImagePath() + "collapse.png");
		};
	  	return div;
	};
	var searchCtrl = new SearchControl(searchInfo);
	map.addControl(searchCtrl);
	
}


//初始化比例尺和坐标显示
function initLngLatShow(){
	function LngLatShowControl(){
		this.defaultAnchor = BMAP_ANCHOR_TOP_RIGHT;
		this.defaultOffset = new BMap.Size(5, 27);
	}
	LngLatShowControl.prototype = new BMap.Control();
	LngLatShowControl.prototype.initialize = function(map){
	  var label = document.createElement("label");
	  label.id = "lblLngLat";
	  label.style.fontSize="12px";
	  label.style.bottom="0px";
	  label.style.left="5px";
	  map.getContainer().appendChild(label);
	  return label;
	};
	var lngLatShowCtrl = new LngLatShowControl();
	map.addControl(lngLatShowCtrl);
	map.addEventListener("mousemove", function(e) {
		document.getElementById('lblLngLat').innerText=e.point.lng + "," + e.point.lat;
	});	
}
//初始化右键菜单
function initMapMenu(){
	distTool = new BMapLib.DistanceTool(map);
	menuMap = new BMap.ContextMenu();
	menuMap.addItem(new BMap.MenuItem("放大",function(){map.zoomIn();},{width : 80,iconUrl : getImagePath()+"zoom_in.png"}));
	menuMap.addItem(new BMap.MenuItem("缩小",function(){map.zoomOut();},{width : 80,iconUrl : getImagePath()+"zoom_out.png"}));
	menuMap.addItem(new BMap.MenuItem("测距",function(){distTool.open();},{width : 80,iconUrl : getImagePath()+"dist.png"}));
	map.addContextMenu(menuMap);
}

//初始化工具栏
function initTools() {
	var styleOptions = {
		strokeColor : "blue", // 边线颜色。
		fillColor : "blue", // 填充颜色。当参数为空时，圆形将没有填充效果。
		strokeWeight : 2, // 边线的宽度，以像素为单位。
		strokeOpacity : 0.5, // 边线透明度，取值范围0 - 1。
		fillOpacity : 0.3, // 填充的透明度，取值范围0 - 1。
		strokeStyle : 'solid' // 边线的样式，solid或dashed。
	};
	// 实例化鼠标绘制工具
	drawingManager = new BMapLib.DrawingManager(map, {
		isOpen : false, // 是否开启绘制模式
		enableDrawingTool : true, // 是否显示工具栏
		drawingToolOptions : {
			anchor : BMAP_ANCHOR_TOP_LEFT, // 位置
			offset : new BMap.Size(65, 20), // 偏离值
			scale : 0.5,
			drawingModes : [
			  //BMAP_DRAWING_MARKER, 
			    BMAP_DRAWING_CIRCLE,
			    BMAP_DRAWING_POLYLINE, 
			    BMAP_DRAWING_POLYGON,
			    BMAP_DRAWING_RECTANGLE 
			]
		},
		circleOptions : styleOptions, // 圆的样式
		polylineOptions : styleOptions, // 线的样式
		polygonOptions : styleOptions, // 多边形的样式
		rectangleOptions : styleOptions
	// 矩形的样式
	});
	// 添加鼠标绘制工具监听事件，用于获取绘制结果
	drawingManager.addEventListener('overlaycomplete', overlaycomplete);
}

//改变中心点坐标和缩放倍数
function doCenterAndZoom(px, py, zoom) {
	if(zoom != map.getZoom()){
		isLoadZoom = false;
	}else {
		isLoadZoom = true;
	}
	var point = new BMap.Point(px, py);
	map.centerAndZoom(point, zoom);
}

//改变中心点坐标
function doCenterTo(px, py) {
	var point = new BMap.Point(px, py);
	map.centerAndZoom(point, map.getZoom());
}

//清空数据
function doClearOverlays(){
	map.clearOverlays();
}

//清空图层数据
function doClearLayerOverlays(layerid){
	var layer = getLayerById(layerid);
	if(layer != null){
		clearLayerOverlay(layer);
	}
}

//隐藏图层数据
function doHideLayerOverlays(layerid){
	var layer = getLayerById(layerid);
	if(layer != null){
		hideLayerOverlay(layer);
	}
}

//显示图层数据
function doShowLayerOverlays(layerid){
	var layer = getLayerById(layerid);
	if(layer != null){
		showLayerOverlay(layer);
	}
}

//刷新地图
function doRefreshMap(){
	boundOld = null;
	reloadMap();
}

//设置标题
function doSetLegend(legendinfo){
	initlegend(legendinfo);
}

//清除标题
function doClearLegend(){
	if(legendCtrl != null){
		map.removeControl(legendCtrl);
		legendCtrl = null;
	}
}

//设置地图右键菜单
function doSetMapMenu(menuinfo){
	if(menuMap != null){
		menuMap.addSeparator();
		for(var i=0;i<menuinfo.length;i++){
			if(menuinfo[i].type == null){
				continue;
			}
			if(menuinfo[i].type == "separator"){
				menuMap.addSeparator();
				continue;
			}
			var lblName = menuinfo[i].label;
			var menuWidth = parseInt(menuinfo[i].width);
			addMenuItem(menuinfo[i].type,lblName,menuWidth,getImagePath()+menuinfo[i].icon,menuinfo[i].url);
		}
	}
}

//添加菜单
function addMenuItem(type,menuName,menuWidth,icon,url){
	var item = new BMap.MenuItem(
		menuName,
		function(e,ee){
			if(type == "menu"){
				url = url.replace(/#login_name#/g, getCookie("username"));
//				window.open(url);
				showWindow(menuName,url);
			}
			if(type == "event"){
				eval(url);
			}
		},
		{
			width   :  menuWidth,
			iconUrl :  icon
		});
	menuMap.addItem(item);
}

function showWindow(menuName,url){
	var div = document.getElementById("winShow");
	if(!div){
		div = document.createElement("div");
		div.style.width=(document.body.offsetWidth - 100) + "px";
		div.style.height=(document.body.offsetHeight - 100) + "px";
		div.setAttribute("id", "winShow");
		document.body.appendChild(div);
	}
	
	var frame = document.createElement("iframe");
	frame.style.width = div.style.width;
	frame.style.height = div.style.height;
	frame.style.frameborder="no";
	frame.style.border="0";
	frame.style.marginwidth="0";
	frame.style.marginheight="0";
	frame.style.scrolling="no"; 
	frame.src=url;
	div.appendChild(frame);
	
	$("#winShow").layerModel({
		title : menuName,
		locked : true,
		close : function(){
			div.style.display = "none";
			div.removeChild(frame);
		}
	});
}

function initgrid(gridinfo){
	gridLayer = gridinfo;
	gridLayer.isShow = false;
}

function doShowAllSector(){
//	doCallZK('$winGIS','onShowAllSector',null);
}

function doShowGrid(){
	if(gridLayer){
		var zoom = map.getZoom();
		if(zoom >= gridLayer.minzoom && zoom <= gridLayer.maxzoom){
			if(!gridLayer.isShow){
				gridLayer.isShow = true;
				refreshGrid();
			}else {
				gridLayer.isShow = false;
				if(gridLayer.gridOverlay){
					map.removeOverlay(gridLayer.gridOverlay);
					gridLayer.gridOverlay = null;
				}
			}
		}
	}
}

//设置地图滚动文字
function doInitMarquee(marqueeinfo){
	if(marqueeinfo.message && marqueeinfo.message.length > 0){
		isMarquee = true;
	}
	marqueeMessage.innerText = marqueeinfo.message;
	marqueeMain.style.backgroundColor = "#" + marqueeinfo.bgcolor;
	marqueeMain.style.display = "block";
	
	var tab=document.getElementById("marqueeMain");
	var tab1=document.getElementById("marqueeMessage");
	var tab2=document.getElementById("marqueeMessage2");
	tab2.innerHTML=tab1.innerHTML;
	if(marqueeinfo.isInit == "true"){
		var speed=parseInt(marqueeinfo.speed); //数字越大速度越慢
		function Marquee(){
			if(tab2.offsetWidth-tab.scrollLeft<=0)
				tab.scrollLeft-=tab1.offsetWidth
			else{
				tab.scrollLeft++;
			}
		}
		var MyMar=setInterval(Marquee,speed);
		tab.onmouseover=function() {clearInterval(MyMar)};
		tab.onmouseout=function() {MyMar=setInterval(Marquee,speed)};
	}
}

var overlaycomplete = function(e) {
	if (e.drawingMode == "polyline") {
		showSelect(e.overlay.getBounds());
	}
	if (e.drawingMode == "circle") {
		showSelect(e.overlay.getBounds());
	}
	if (e.drawingMode == "polygon") {
		showSelect(e.overlay.getBounds());
	}
	if (e.drawingMode == "rectangle") {
		showSelect(e.overlay.getBounds());
	}
	if (e.drawingMode != "marker") {
		map.removeOverlay(e.overlay);
	}
};

function showSelect(bounds) {
	var objSelect = "";
	if(maplayers.length > 0){
		var zoom = map.getZoom();
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.minzoom <= zoom && zoom <= maplayer.layerinfo.maxzoom){
				for(var j=0;j<maplayer.overlays.length;j++){
					var overlay = maplayer.overlays[j];
					if(maplayer.layerinfo.type == "marker"){
						if (bounds.containsPoint(overlay.getPosition())) {
							if(overlay.info.selectkey != null){
								objSelect += overlay.info[overlay.info.selectkey] + ',';
							}
						}
					}
					if(maplayer.layerinfo.type == "sector"){
						if (bounds.containsPoint(overlay.point)) {
							if(overlay.info.selectkey != null){
								objSelect += overlay.info[overlay.info.selectkey] + ',';
							}
						}
					}
				}
			}
		}
	}
	if(objSelect.length > 0){
		objSelect = objSelect.substring(0, objSelect.length - 1);
//		doCallZK('$winGIS','onSelectOverlay',objSelect);
	}
};
 

// 创建地图图层
function createMapLayer(layerinfo){
	var maplayer = new Object();
	maplayer.layerinfo = layerinfo;
	var arrOverlay = new Array();
	maplayer.overlays = arrOverlay;
	return maplayer;
}

// 批量创建图层
function addLayer(layerinfo) {
	var maplay = createMapLayer(layerinfo);
	maplayers.push(maplay);
}

function getLayer(layerinfo){
	var layer = null;
	if(maplayers.length > 0){
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.layerid == layerinfo.layerid){
				layer = maplayer;
				break;
			}
		}
		if(layer == null){
			layer = createMapLayer(layerinfo);
		}
	}else {
		layer = createMapLayer(layerinfo);
	}
	return layer;
}

//根据ID获取图层
function getLayerById(layerid){
	var layer = null;
	if(maplayers.length > 0){
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.layerid == layerid){
				layer = maplayer;
				break;
			}
		}
	}
	return layer;
}

function isHaveLayer(layerinfo){
	var layer = null;
	if(maplayers.length > 0){
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.layerid == layerinfo.layerid){
				layer = maplayer;
				break;
			}
		}
	}
	return (layer != null);
}

//初始化根据缩放比例过滤覆盖物
function initFitlerOverlay(){
	if(map.mapinfo.zoomfitler){
		map.addEventListener("zoomend", function(e) {
			if(isLoadZoom){
				fitlerOverlayByZoom();
				boundOld = null;
				refreshMap(true);
				refreshGrid();
			}
			isLoadZoom = true;
		});
		map.addEventListener("dragend", function(e) {
			refreshMap(false);
			refreshGrid();
		});
	}
	map.addEventListener("click", function(e) {
		if(popupCtrl){
			popupCtrl.clicknum ++ ;
		}
		clearFloat();
	});
}

//刷新网格
function refreshGrid(){
	if(gridLayer && gridLayer.isShow){
		var zoom = map.getZoom();
		if(gridLayer.gridOverlay){
			map.removeOverlay(gridLayer.gridOverlay);
			gridLayer.gridOverlay = null;
		}
		if(zoom >= gridLayer.minzoom && zoom <= gridLayer.maxzoom){
			if(gridLayer.gridOverlay){
				map.removeOverlay(gridLayer.gridOverlay);
				gridLayer.gridOverlay = null;
			}else {
				var space = parseInt(gridLayer.initspace);
				space = getZoomRadius(space,gridLayer.minzoom);
				gridLayer.gridOverlay = new GridOverlay(space,gridLayer.alpha);
				map.addOverlay(gridLayer.gridOverlay);
				//刷新网格数据
				var objGrid = new Object();
				var bound = map.getBounds();
				objGrid.pointSouthWest = bound.getSouthWest();
				objGrid.pointNorthEast = bound.getNorthEast();
				objGrid.space = space;
				objGrid.hnum = gridLayer.gridOverlay._hnum;
				objGrid.wnum = gridLayer.gridOverlay._wnum;
				
				var wGrid = gridLayer.wgrid;
				var startGridX = parseInt((objGrid.pointSouthWest.lng - gridLayer.startpx)/wGrid);
				objGrid.startGridX = startGridX;
				
				var hGrid = gridLayer.hgrid;
				var startGridY = parseInt((objGrid.pointSouthWest.lat - gridLayer.startpy)/hGrid);
				objGrid.startGridY = startGridY;
				
//				doCallZK('$winGIS','onLoadGridRect',JSON.stringify(objGrid));
			}
		}
	}
}

//刷新地图
function refreshMap(isClear){
	if(maplayers.length > 0){
		var zoom = map.getZoom();
		var boundTemp = null;
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.limitbound == 'true'){
				if(isClear){
					clearLayerOverlay(maplayer);
				}else {
					clearOverlayNotInBound(maplayer);
				}
				if(maplayer.layerinfo.minzoom <= zoom && zoom <= maplayer.layerinfo.maxzoom){
					var bound = map.getBounds();
				    var pointSouthWest = bound.getSouthWest();
					var pointNorthEast = bound.getNorthEast();
					maplayer.layerinfo.pointSouthWest = pointSouthWest;
					maplayer.layerinfo.pointNorthEast = pointNorthEast;
					if(boundOld != null){
						maplayer.layerinfo.pointSouthWestOld = boundOld.getSouthWest();
						maplayer.layerinfo.pointNorthEastOld = boundOld.getNorthEast();
					}else {
						delete maplayer.layerinfo.pointSouthWestOld;
						delete maplayer.layerinfo.pointNorthEastOld;
					}
					boundTemp = bound;
					addLayerOverlay(maplayer.layerinfo);
				}
			}
		}
		boundOld = boundTemp;
	}
}

//重新加载地图
function reloadMap(){
	if(maplayers.length > 0){
		var zoom = map.getZoom();
		var boundTemp = null;
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.reload == "false"){
				continue;
			}
			if(maplayer.layerinfo.minzoom <= zoom && zoom <= maplayer.layerinfo.maxzoom){
				clearLayerOverlay(maplayer);
				var bound = map.getBounds();
				var pointSouthWest = bound.getSouthWest();
				var pointNorthEast = bound.getNorthEast();
				maplayer.layerinfo.pointSouthWest = pointSouthWest;
				maplayer.layerinfo.pointNorthEast = pointNorthEast;
				if(boundOld != null){
					maplayer.layerinfo.pointSouthWestOld = boundOld.getSouthWest();
					maplayer.layerinfo.pointNorthEastOld = boundOld.getNorthEast();
				}else {
					delete maplayer.layerinfo.pointSouthWestOld;
					delete maplayer.layerinfo.pointNorthEastOld;
				}
				boundTemp = bound;
				addLayerOverlay(maplayer.layerinfo);
			}
		}
		boundOld = boundTemp;
	}
}

//清空不在展示范围的数据
function clearOverlayNotInBound(maplayer){
	var bound = map.getBounds();
	var arrOverlay = new Array();
	for(var j=0; j<maplayer.overlays.length; j++){
		var overlay = maplayer.overlays[j];
		if(overlay.info != null && overlay.info.PX != null && overlay.info.PY != null){
			var point = new BMap.Point(overlay.info.PX, overlay.info.PY);
			if(!bound.containsPoint(point)){
				map.removeOverlay(overlay);
			}else{
				arrOverlay.push(overlay);
			}
		}
	}
	maplayer.overlays = arrOverlay;
}

//清空数据
function clearLayerOverlay(maplayer){
	for(var j=0; j<maplayer.overlays.length; j++){
		var overlay = maplayer.overlays[j];
		map.removeOverlay(overlay);
	}
	var arrOverlay = new Array();
	maplayer.overlays = arrOverlay;
}

//隐藏数据
function hideLayerOverlay(maplayer){
	for(var j=0; j<maplayer.overlays.length; j++){
		var overlay = maplayer.overlays[j];
		overlay.hide();
	}
}

//显示数据
function showLayerOverlay(maplayer){
	for(var j=0; j<maplayer.overlays.length; j++){
		var overlay = maplayer.overlays[j];
		overlay.show();
	}
}

function showLayerOverlayNum(){
	var num = map.getOverlays().length;
	alert(num);
}

//根据缩放比例过滤覆盖物
function fitlerOverlayByZoom(){
	if(maplayers.length > 0){
		var zoom = map.getZoom();
		for(var i=0;i<maplayers.length;i++){
			var maplayer = maplayers[i];
			if(maplayer.layerinfo.minzoom <= zoom && zoom <= maplayer.layerinfo.maxzoom){
				showLayerOverlay(maplayer);
			}else {
				hideLayerOverlay(maplayer);
			}
		}
	}
}

//初始化比例缩放尺度
function iniZoomSize(){
	zoomSize.zoom_8 = 50000;
	zoomSize.zoom_9 = 25000;
	zoomSize.zoom_10 = 20000;
	zoomSize.zoom_11 = 10000;
	zoomSize.zoom_12 = 5000;
	zoomSize.zoom_13 = 2000;
	zoomSize.zoom_14 = 1000;
	zoomSize.zoom_15 = 500;
	zoomSize.zoom_16 = 200;
	zoomSize.zoom_17 = 100;
	zoomSize.zoom_18 = 50;
	zoomSize.zoom_19 = 20;
}

function getImagePath(){
	//获得工程名称
	var strFullPath = window.document.location.href;
	var strPath = window.document.location.pathname;
	var pos = strFullPath.indexOf(strPath);
	var prePath = strFullPath.substring(0, pos);
	var postPath = strPath.substring(0, strPath.substr(1).indexOf('/') + 1);
	//拼接URL
	var local = 'http://'+window.location.host+ postPath + '/resources' +'/gis/images/';
	return local;
}
