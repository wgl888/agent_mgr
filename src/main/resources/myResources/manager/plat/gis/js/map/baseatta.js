var popupCtrl;
function createFloat(info){
	function PopupControl(info){
		this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
		this.defaultOffset = new BMap.Size(info.x,info.y - 16);
		this._info = info;
	}
	PopupControl.prototype = new BMap.Control();
	PopupControl.prototype.initialize = function(map){
		var div = document.createElement("div");
		div.style.width="10px";
		div.style.display="block";
		div.setAttribute("class", "tbl");
		var datainfo = this._info.info;
		var obj = new Object();
		obj['xml'] = mapUrlParam['xml'];
		obj['infoJson'] = JSON.stringify(this._info.info);
		doCallPost("doLoadPopupInfo",obj,function(data){
			if(data.width){
				div.style.width=data.width;
			}
			var labelwidth = "50px";
			if(data.labelwidth){
				labelwidth = data.labelwidth;
			}
			doCallPost("doLoadPopupData",obj,function(data){
				createPropTable(div,data,datainfo,labelwidth);
			});
		});
		map.getContainer().appendChild(div);
	  	return div;
	};
	if(popupCtrl){
		popupCtrl.clicknum = 2;
		clearFloat();
	}
	popupCtrl = new PopupControl(info);
	popupCtrl.clicknum = 0;
	map.addControl(popupCtrl);
}
function clearFloat(){
	if(popupCtrl && popupCtrl.clicknum > 1){
		map.removeControl(popupCtrl);
		popupCtrl = null;
	}
}

function createPropTable(rootdiv,jsonheader,jsondata,labelwidth){
	if(jsonheader.length == 0){
		return;
	}
	var ros_num = jsonheader.length;
	rootdiv.style.height  = (parseInt(ros_num*26) + 23) +"px";
	//设置列宽
	var parentWidth = parseInt(rootdiv.style.width)-1;
	var headerWidth = labelwidth;
	var valueWidth = parentWidth - headerWidth;
	var tdClass;
	//创建table
	var comTable = document.createElement("table");
	comTable.style.width = rootdiv.style.width;
	for(var i=0;i<jsonheader.length;i++){
		var thead = document.createElement("tr");
		var headObj = jsonheader[i];
		if(headObj.tdclass){
			tdClass = headObj.tdclass;
		}
		var th = document.createElement("th");
		th.innerHTML = headObj.label;
		th.setAttribute("title", headObj.label);
		th.style.align = "center";
		th.style.valign = "middle";
		th.style.width = headerWidth + "px";
		thead.appendChild(th);
		
		var td = document.createElement("td");
		if(jsondata[headObj.field]){
			td.innerHTML = jsondata[headObj.field];
		}else {
			td.innerHTML = '';
		}
		td.setAttribute("title", jsondata[headObj.field]);
		td.style.align = "center";
		td.style.valign = "middle";
		td.style.width = (valueWidth-22) + "px";
		
		thead.appendChild(td);
		
		comTable.appendChild(thead);
	}
	rootdiv.appendChild(comTable);	
}