var overlay_type_marker  = "marker"; //标记
var overlay_type_large   = "large";  //海量标记
var overlay_type_heat    = "heat";   //热力
var overlay_type_sector  = "sector"; //扇区
var overlay_type_line    = "line";   //连线
var overlay_type_move    = "move";   //移动

//批量创建覆盖物
function createOverlays(overlays,layerinfo) {
	var maplay = getLayer(layerinfo);
	switch(layerinfo.type){
		case overlay_type_marker :{
			for(var i = 0; i < overlays.length; i++){
				var overlay = createMarker(overlays[i]);
				addOverlayListener(overlay);
				maplay.overlays.push(overlay);
			}
			if(layerinfo.clusterer == 'true'){
				var markerClusterer = new BMapLib.MarkerClusterer(map, {markers:maplay.overlays});
			}else {
				if(maplay.overlays.length > 0){
					for(var i = 0; i < maplay.overlays.length; i++){
						var overlay = maplay.overlays[i];
						map.addOverlay(overlay);
					}
				}
			}
			break;
		}
		case overlay_type_large :{
			createLargeMarker(overlays,layerinfo,maplay);
			break;
		}
		case overlay_type_heat :{
			if (!isSupportCanvas()) {
				alert('热力图目前只支持有canvas支持的浏览器(IE9、IE10、IE11、谷歌等),您所使用的浏览器不能使用热力图功能.');
				return;
			}
			clearLayerOverlay(maplay);
			maplay.overlays.push(createHeatMap(overlays,layerinfo));
			break;
		}
		case overlay_type_sector :{
			if (!isSupportCanvas()) {
				alert('扇区图目前只支持有canvas支持的浏览器(IE9、IE10、IE11、谷歌等),您所使用的浏览器不能使用扇区图功能.');
				return;
			}
			for(var i = 0; i < overlays.length; i++){
				var overlay = createSector(overlays[i]);
				maplay.overlays.push(overlay);
				map.addOverlay(overlay);
				
				var numOverlay = createSectorNum(overlays[i]);
				if(numOverlay){
					maplay.overlays.push(numOverlay);
					map.addOverlay(numOverlay);
				}
			}
			break;
		}
		case overlay_type_line :{
			if (!isSupportCanvas()) {
				alert('连线图目前只支持有canvas支持的浏览器(IE9、IE10、IE11、谷歌等),您所使用的浏览器不能使用连线图功能.');
				return;
			}
			for(var i = 0; i < overlays.length; i++){
				var overlay = createLine(overlays[i]);
				maplay.overlays.push(overlay);
				map.addOverlay(overlay);
			}
			break;
		}
		case overlay_type_move :{
			if (!isSupportCanvas()) {
				alert('移动图目前只支持有canvas支持的浏览器(IE9、IE10、IE11、谷歌等),您所使用的浏览器不能使用移动图功能.');
				return;
			}
			createMove(overlays,layerinfo);
			break;
		}
	}
	if(!isHaveLayer(layerinfo)){
		maplayers.push(maplay);
	}
	if(map.mapinfo.zoomfitler){
		fitlerOverlayByZoom();
	}
}

//添加覆盖物事件
function addOverlayListener(overlay){
	//覆盖物单击事件
	overlay.addEventListener("click", function(e) {
		//设置点击缩放
		if(overlay.info.click != null && overlay.info.click.length > 0){
			var strs= new Array();
			strs=overlay.info.click.split("|");
			if(strs.length > 0){
				if(strs[0] == 'zoom' && strs.length == 2){
					isLoadZoom = true;
					var point = new BMap.Point(overlay.info.PX, overlay.info.PY);
					map.centerAndZoom(point, strs[1]);
				}
			}
		}
		markerClick(overlay.info);
	});
	//鼠标移动至覆盖物事件
	overlay.addEventListener("mouseover", function(e) {
	});
	//鼠标移动离开覆盖物事件
	overlay.addEventListener("mouseout", function(e) {
	});
}

//创建标记
function createMarker(markerobj) {
	//图片根据状态值改变
	if(markerobj.imagefield != null && markerobj.imagefield.length > 0){
		markerobj.icon = splitStringGetValue(markerobj,markerobj.imagefield);
	}
	// 图片
	var markerIcon = null;
	if (markerobj.icon != null && markerobj.iconWidth != null && markerobj.iconHeight != null) {
		markerIcon = new BMap.Icon(getImagePath() + markerobj.icon, new BMap.Size(markerobj.iconWidth, markerobj.iconHeight));
	}
	var MarkerOptions = {
		enableDragging : (markerobj.enableDragging == 'true' ? true : false),
		title : markerobj.title,
		icon : markerIcon,
		label : markerLabel
	};
	// 坐标
	var pointMarker = new BMap.Point(markerobj.PX, markerobj.PY);
	var marker = new BMap.Marker(pointMarker, MarkerOptions); // 创建标注
	//文本
	if (markerobj.label != null) {
		var markerLabel = new BMap.Label(markerobj.label);
		
//		var offsetLabel = new BMap.Size(-15, 27);
//		if(markerobj.labelOffsetX != null && markerobj.labelOffsetY != null){
//			offsetLabel = new BMap.Size(markerobj.labelOffsetX, markerobj.labelOffsetY);
//		}
//		markerLabel.setOffset(offsetLabel);
		
		var width = (markerIcon.size.width - (getStrLength(markerobj.label) * 5))/2;
		var offsetLabel = new BMap.Size(width, markerIcon.size.height);
		markerLabel.setOffset(offsetLabel);
		
		var styleLabel = new Object();
		if(markerobj.labelColor != null){
			styleLabel.color = "#" + markerobj.labelColor;
		}
		if(markerobj.labelBgColor != null){
			styleLabel.backgroundColor = "#" + markerobj.labelBgColor;
		}
		if(markerobj.labelBorder != null){
			styleLabel.border = markerobj.labelBorder;
		}
		if(markerobj.labelFontSize != null){
			styleLabel.fontSize = markerobj.labelFontSize;
		}
		markerLabel.setStyle(styleLabel);
		marker.setLabel(markerLabel);
	}
	marker.info = markerobj;
	//设置覆盖物显示层面
	if(markerobj.zIndex){
		marker.setZIndex(parseInt(markerobj.zIndex));
	}
	return marker;
}

//创建海量节点
function createLargeMarker(makers,layerinfo,maplay) {
	var pointObj = new Object();
	var size = parseInt(layerinfo.size);
	var shape = parseInt(layerinfo.shape);
	//默认集合
	var optionsDefault = {
        size: size,
        shape: shape,
        color: "#" + layerinfo.defaultcolor
    };
    var pointsDefault = [];
	pointObj["optionsDefault"] = optionsDefault;
	pointObj["pointsDefault"] = pointsDefault;
	//区分不同颜色集合
	if(layerinfo.fillcolor != null && layerinfo.fillcolor.length > 0){
		var keyArr = getKeysArray(layerinfo.fillcolor);
		var valueArr = getValuesArray(layerinfo.fillcolor);
		for(var i = 0; i < keyArr.length; i++){
			var options = {
         		size: size,
         		shape: shape,
        		color: "#" + valueArr[i]
    		};
			pointObj[keyArr[i]+"_options"] = options;
			var points = [];
			pointObj[keyArr[i]+"_points"] = points;
		}
	}
	var paramName = getParamName(layerinfo.fillcolor);
	
    for (var i = 0; i < makers.length; i++) {
    	var markerobj = makers[i];
    	var point = new BMap.Point(markerobj.PX, markerobj.PY);
    	point.info = markerobj;
    	if(pointObj[markerobj[paramName]+"_points"] != null){
    		var points = pointObj[markerobj[paramName]+"_points"];
    		points.push(point);
    	}else {
    		pointsDefault.push(point);
    	}
    }
    
    //区分不同颜色集合
	if(layerinfo.fillcolor != null && layerinfo.fillcolor.length > 0){
		var keyArr = getKeysArray(layerinfo.fillcolor);
		var valueArr = getValuesArray(layerinfo.fillcolor);
		for(var i = 0; i < keyArr.length; i++){
			var pointCollection = new BMap.PointCollection(pointObj[keyArr[i]+"_points"], pointObj[keyArr[i]+"_options"]);
			map.addOverlay(pointCollection);
			maplay.overlays.push(pointCollection);
		}
	}
	
	//添加默认
    var pointCollectionDefault = new BMap.PointCollection(pointObj["pointsDefault"], pointObj["optionsDefault"]);
    map.addOverlay(pointCollectionDefault);
    maplay.overlays.push(pointCollectionDefault);
	
    if(layerinfo.click == "true"){
    	for(var i = 0; i < maplay.overlays.length; i++){
    		var pointCollection = maplay.overlays[i];
    			pointCollection.addEventListener('click', function (e) {
    			//设置点击回调ZK
    			var pixel = map.pointToPixel(e.point);
				recallZK('click','click',pixel.x,pixel.y,e.point.info,'onClickOverlay');
      		});
    	}	
    }
}

//添加热力图数据
function createHeatMap(points,layerinfo) {
	var heatmap = new BMapLib.HeatmapOverlay({
		"radius" : 20
	});
	map.addOverlay(heatmap);
	heatmap.setDataSet({
		data : points,
		max : layerinfo.max
	});
	return heatmap;
}
//判断浏览区是否支持canvas
function isSupportCanvas() {
	var elem = document.createElement('canvas');
	return !!(elem.getContext && elem.getContext('2d'));
}


//创建扇区
function createSector(sectorobj) {
	//是否扇形
	var isSector = "false";
	var strs= new Array(); 
	if(sectorobj.isCircle != null){
		isSector = splitStringGetValue(sectorobj,sectorobj.isCircle);
	}
	//偏移角度
	var angle = 0;
	if(isSector == "true" && sectorobj[sectorobj.startAngle] != null){
		angle = parseInt(sectorobj[sectorobj.startAngle]);
	}
	//半径
	var radius = 30;
	if(sectorobj.radius != null){
		var rStr = splitStringGetValue(sectorobj,sectorobj.radius);
		if(rStr != null && rStr.length > 0){
			radius = parseInt(rStr);
		}
	}
	radius = getZoomRadius(radius,sectorobj.initzoom);
	var sectorOverlay = new SectorOverlay(new BMap.Point(sectorobj.PX, sectorobj.PY),radius,angle,isSector,sectorobj);
	sectorOverlay.info = sectorobj;
	sectorOverlay.point = new BMap.Point(sectorobj.PX, sectorobj.PY);
	return sectorOverlay;
}

function createSectorNum(sectorobj){
	if(sectorobj.SECTOR_NUM){
		//是否扇形
		var isSector = "false";
		if(sectorobj.isCircle != null){
			isSector = splitStringGetValue(sectorobj,sectorobj.isCircle);
		}
		//半径
		var radius = 30;
		if(sectorobj.radius != null){
			var rStr = splitStringGetValue(sectorobj,sectorobj.radius);
			if(rStr != null && rStr.length > 0){
				radius = parseInt(rStr);
			}
		}
		radius = getZoomRadius(radius,sectorobj.initzoom);
		radius = radius/2;
		
		//偏移角度
		var angle = 0;
		if(isSector == "true" && sectorobj[sectorobj.startAngle] != null){
			angle = parseInt(sectorobj[sectorobj.startAngle]);
		}
		
		var angleTemp = angle % 90;
		if(angle == 90 || angle == 270){
			angleTemp = 90;
		}
		var x = 0;
		var y = 0;
		if((angle >= 0 && angle <= 90) || (angle >= 180 && angle <= 270) || angle == 360){
			x = Math.sin(angleTemp/180*Math.PI) * radius;
			y = Math.cos(angleTemp/180*Math.PI) * radius;
		}else {
			x = Math.cos(angleTemp/180*Math.PI) * radius;
			y = Math.sin(angleTemp/180*Math.PI) * radius;
		}
		
		if(angle > 180 && angle < 360){
			x = -1 * x;
		}
		
		if(angle > 90 && angle < 270){
			y = 1 * y;
		}else {
			y = -1 * y;
		}
		
		
		var tagIcon = new BMap.Icon(getImagePath() + "tag_bottom.png", new BMap.Size(10,16));
		var marker = new BMap.Marker(new BMap.Point(sectorobj.PX, sectorobj.PY),{icon:tagIcon,offset:new BMap.Size(0 + x,-7 + y)});
		marker.setTop(true);
		marker.info = sectorobj;
		
		var lblX = -2 - (('' + sectorobj.SECTOR_NUM).length - 1) * 4;
		var lblWidth = 12 + (('' + sectorobj.SECTOR_NUM).length - 1) * 7;
		
		var label = new BMap.Label('' + sectorobj.SECTOR_NUM,{offset:new BMap.Size(lblX,-13)});
		label.setStyle({color : "white", 
					  backgroundColor:"#e60611",
					  width : lblWidth + "px",
	                  fontSize : "8px", 
					  height : "12px",
					  lineHeight : "12px",
	                  border : "0px",textAlign:"center"
	                  });
		marker.setLabel(label);
		return marker;
	}
	return null;
}

//获得缩放后的半径
function getZoomRadius(radius,initzoom){
	var result = radius;
	var zoom = map.getZoom();
	if(zoom > initzoom){
		var initSize = zoomSize["zoom_" + initzoom];
		var size = zoomSize["zoom_" + zoom];
		result = radius * (initSize/size);
	}
	return result;
}

//扇区覆盖物
function SectorOverlay(point,radius,angle,isSector,info){
	this._point = point;
	this._radius = radius;
    this._angle = angle;
    this._isSector = isSector;
    this._info = info;
    this._rx = 0;
    this._ry = 0;
}
//扇区属性
SectorOverlay.prototype = new BMap.Marker();
SectorOverlay.prototype.initialize = function(map){
	this._map = map;
	var cans = this._cans = document.createElement("canvas");
	cans.style.position="absolute";
	cans.style.cursor="pointer";
	cans.setAttribute("title", this._info.title);
	var startAngle = 0;
	var endAngle = 0;
	if(this._isSector == "true"){
		cans.width = this._radius;
		cans.height = this._radius;
		startAngle = -120 + this._angle;
		endAngle = -60 + this._angle;
		if(this._angle >= 0  && this._angle < 30){
        	var d = Math.cos((60- this._angle)/180*Math.PI)-Math.cos(60/180*Math.PI);
        	this._rx = this._radius/2 - d * this._radius;
			this._ry = this._radius;
      	}
      	if(this._angle >= 30  && this._angle < 60){
        	this._rx = 0;
			this._ry = this._radius;
      	}
      	if(this._angle >= 60  && this._angle < 120){
        	this._rx = 0;
        	var d = Math.sin((this._angle-60)/180*Math.PI);
			this._ry = this._radius - d * this._radius;
      	}
      	if(this._angle >= 120  && this._angle < 150){
        	this._rx = 0;
			this._ry = 0;
      	}
      	if(this._angle >= 150  && this._angle < 210){
        	var d = Math.sin((this._angle-150)/180*Math.PI);
        	this._rx = 0 + d * this._radius;
			this._ry = 0;
      	}
      	if(this._angle >= 210  && this._angle < 240){
        	this._rx = this._radius;
			this._ry = 0;
      	}
      	if(this._angle >= 240  && this._angle < 300){
        	var d = Math.sin((this._angle-240)/180*Math.PI);
        	this._rx = this._radius;
			this._ry = 0 + d * this._radius;
      	}
      	if(this._angle >= 300  && this._angle <= 330){
        	this._rx = this._radius;
        	this._ry = this._radius;
      	}
      	if(this._angle > 330  && this._angle <= 360){
        	var d = Math.sin((this._angle-330)/180*Math.PI);
        	this._rx = this._radius -  d * this._radius;
        	this._ry = this._radius;
      	}
	}else {
		cans.width = this._radius * 2;
		cans.height = this._radius * 2;
		startAngle = 0;
		endAngle = 360;
		this._rx = this._radius;
        this._ry = this._radius;
	}
	
	var deg = Math.PI/180;
	var ctx = this._cans.getContext('2d');
	var colorStr;
	if(this._info.fillcolor != null && this._info.fillcolor.length > 0){
		colorStr = splitStringGetValue(this._info,this._info.fillcolor);
	}
	if(colorStr == null || colorStr.length == 0){
		if(this._info.defaultcolor != null || this._info.defaultcolor.length > 0){
			var colorStr = this._info.defaultcolor;
		}else {
			colorStr = "31f651";//默认绿色
		}
	}
	ctx.fillStyle = "#" + colorStr;
	if(this._info.alpha == null){
		ctx.globalAlpha = 0.2;
	}else {
		ctx.globalAlpha = parseFloat(this._info.alpha);
	}
	
	// 开始一条新路径
	ctx.beginPath();
	// 位移到圆心，方便绘制
    ctx.translate(this._rx, this._ry);
    // 移动到圆心
    ctx.moveTo(0, 0);
    // 绘制圆弧
    ctx.arc(0, 0, this._radius, startAngle*deg, endAngle*deg);
    // 闭合路径
    ctx.closePath();
    ctx.fill();
    map.getPanes().markerPane.appendChild(cans);
    var point = this._point;
    var info = this._info;
    cans.onclick=function(e){
    	var pixel = map.pointToPixel(point);
    	recallZK('click','click',e.clientX,e.clientY,info,'onClickOverlay');
    };
    return cans;
};
//扇区绘制
SectorOverlay.prototype.draw = function(){
	var map = this._map;
    var pixel = map.pointToOverlayPixel(this._point);
    this._cans.style.marginLeft = pixel.x - this._rx + "px";
    this._cans.style.marginTop  = pixel.y - this._ry + "px";
};


//网格覆盖物
function GridOverlay(space,alpha){
    this._space = space;
	this._alpha = alpha;
}
//网格属性
GridOverlay.prototype = new BMap.Marker();
GridOverlay.prototype.initialize = function(map){
	this._map = map;
	var cans = this._cans = document.createElement("canvas");
	cans.style.position="absolute";
	cans.style.cursor="pointer";
	var c_width = map.getSize().width;
	var v_height = map.getSize().height;
	var c_space = this._space;
	cans.width = c_width;
	cans.height = v_height;
	//获取该canvas的2D绘图环境 
    var context = cans.getContext('2d'); 
	context.globalAlpha = 0.3;
	
	this._hnum = parseInt(v_height/c_space) + 1;
	this._wnum = parseInt(c_width/c_space) + 1;
	
	drawGrid(context,this._hnum,this._wnum,c_space);
	map.getPanes().markerPane.appendChild(cans);
    return cans;
};
//网格绘制
GridOverlay.prototype.draw = function(){
	var map = this._map;
	var bound = map.getBounds();
	var pointSouthWest = bound.getSouthWest();
	var pointNorthEast = bound.getNorthEast();
    var pixelSW = map.pointToOverlayPixel(pointSouthWest);
    var pixelNE = map.pointToOverlayPixel(pointNorthEast);
    this._cans.style.marginLeft = pixelSW.x + "px";
    this._cans.style.marginTop  = pixelNE.y + "px";
};

function drawGrid(context,h_num,w_num,space){
	context.beginPath(); 
	for(var i=0;i<h_num;i++){
		context.moveTo(0,space * i); 
		context.lineTo(space * (w_num - 1),space * i);
	}
	
	for(var i=0;i<w_num;i++){
		context.moveTo(space * i,0); 
		context.lineTo(space * i,space * (h_num - 1));
	}
	context.stroke(); //以空心填充
}

//画网格点
function drawGridRect(rectInfos){
	if(gridLayer && gridLayer.isShow){
		var zoom = map.getZoom();
		if(zoom >= gridLayer.minzoom && zoom <= gridLayer.maxzoom && gridLayer.gridOverlay && gridLayer.gridOverlay._cans){
			var context = gridLayer.gridOverlay._cans.getContext('2d'); 
			context.globalAlpha = gridLayer.gridOverlay._alpha;
			if(rectInfos && rectInfos.length > 0){
				for(var i = 0; i < rectInfos.length; i++){
					var rect = rectInfos[i];
					var colorStr = "31f651";//默认绿色
					if(gridLayer.fillcolor){
						colorStr = splitStringGetValue(rect,gridLayer.fillcolor);
					}
					drawRect(context,rect.x,rect.y,gridLayer.gridOverlay._space, '#' + colorStr);
				}
			}
		}
	}
}

function drawRect(context,x_num,y_num,space,color){
	context.fillStyle = color;
	context.beginPath(); 
	var x = (x_num - 1) * space;
	var y = (y_num - 1) * space;
	context.moveTo(x,y);
	context.lineTo(x,y + space); 
	context.lineTo(x + space,y + space);
	context.lineTo(x + space,y);
	context.fill();//闭合形状并且以填充方式绘制出来 
}

//分割字符串获得值
function splitStringGetValue(object,str){
	var result = "";
	var strs = new Array();
	strs = str.split("|");
	var param = object[strs[0]];
	for (var i = 1; i <strs.length;i=i+2 ) { 
		var key = strs[i];
		var value = strs[i+1];
		if(param == key){
			result = value;
			break;
		}
	}
	return result; 
}

//分割字符串获得值
function getKeysArray(str){
	var result = new Array();
	var strs = new Array();
	strs = str.split("|");
	for (var i = 1; i <strs.length;i=i+2 ) { 
		var key = strs[i];
		result.push(key);
	}
	return result; 
}

//分割字符串获得值
function getValuesArray(str){
	var result = new Array();
	var strs = new Array();
	strs = str.split("|");
	for (var i = 1; i <strs.length;i=i+2 ) { 
		var value = strs[i+1];
		result.push(value);
	}
	return result; 
}

//分割字符串获得值
function getParamName(str){
	var strs = new Array();
	strs = str.split("|");
	var param = strs[0];
	return param;
}

var moveMarker;
var moveWindow;

//创建移动
function createMove(moveObj,layerinfo) {
	var makerObj = moveObj[0];
	
	// 图片
	var markerIcon = null;
	if (makerObj.icon != null && makerObj.iconWidth != null && makerObj.iconHeight != null) {
		markerIcon = new BMap.Icon(getImagePath() + makerObj.icon, new BMap.Size(makerObj.iconWidth, makerObj.iconHeight));
	}	
	var MarkerOptions = {icon : markerIcon};
	var pointMarker = new BMap.Point(makerObj.PX, makerObj.PY);
	moveMarker = new BMap.Marker(pointMarker,MarkerOptions); // 创建标注
	map.addOverlay(moveMarker);
	
	var opts = {
		width : 0,
		height: 0,
		enableCloseOnClick:false,
		offset : new BMap.Size(0, -(makerObj.iconHeight/2))
	};
	moveWindow = new BMap.InfoWindow("", opts);
	moveMarker.openInfoWindow(moveWindow); //开启信息窗口
	moveWindow.setContent(makerObj.tooltip);
	
	var movePoints = new Array();
	for(var i = 0; i < moveObj.length; i++){
		var obj = moveObj[i];
		var point = new BMap.Point(obj.PX,obj.PY);
		movePoints.push(point);
	}
	
	if(layerinfo.showline == 'true'){
		var lineStrokeColor = "blue";
		if(makerObj.linecolor){
			lineStrokeColor = "#" + makerObj.linecolor;
		}
		var lineStrokeWeight = 2;
		if(makerObj.lineweight){
			lineStrokeWeight = makerObj.lineweight;
		}
		var lineStrokeOpacity = 0.5;
		if(makerObj.linealpha){
			lineStrokeOpacity = makerObj.linealpha;
		}
		//增加折线
		var polyline = new BMap.Polyline(movePoints, {strokeColor:lineStrokeColor, strokeWeight:lineStrokeWeight, strokeOpacity:lineStrokeOpacity});  
		map.addOverlay(polyline); 
	}
	
	
	doStartMove(moveObj,layerinfo.delay);
}

function doStartMove(moveObj,delay){
	var i=0;
	function resetMoveMarker(i){
		var obj = moveObj[i];
		var point = new BMap.Point(obj.PX,obj.PY);
		moveMarker.setPosition(point);
		
		var moveContent = moveWindow.getContent();
		var newContent = obj.tooltip;
		if(moveContent != newContent){
			moveWindow.setContent(newContent);
		}
		
		if(i < moveObj.length){
			setTimeout(function(){
				i++;
				resetMoveMarker(i);
			},delay);
		}
	}
	setTimeout(function(){
		resetMoveMarker(1);
	},delay);
}

//创建连线
function createLine(lineobj) {
	var linePoints = new Array();
	alert(lineobj.LINK_PX);
	if(lineobj.LINK_PX && lineobj.LINK_PY){
		var strsPX = new Array();
		strsPX = lineobj.LINK_PX.split("|");
		var strsPY = new Array();
		strsPY = lineobj.LINK_PY.split("|");
		
		var len = strsPX.length;
		if(len > strsPY.length){
			len = strsPY.length;
		}
		
		for (var i = 0; i <len;i++ ) { 
			linePoints.push(new BMap.Point(strsPX[i], strsPY[i]));
		}
	}
	
	var polyline = new BMap.Polyline(linePoints, {
		strokeColor : "blue",
		strokeWeight : 3,
		strokeOpacity : 1
	});
	map.addOverlay(polyline);
	if(lineobj && lineobj.arrow == 'true'){
		addArrow(polyline, 10, Math.PI / 7);
	}
	
	return polyline;
}

function getStrLength(str){
	var l = str.length;
	var blen = 0;
	for(i=0; i<l; i++) {
		if ((str.charCodeAt(i) & 0xff00) != 0) {
			blen ++;
		}
		blen ++;
	}
	return blen;
}

//绘制箭头的函数
function addArrow(polyline, length, angleValue) { 
	var linePoint = polyline.getPath();// 线的坐标串
	var arrowCount = linePoint.length;
	for ( var i = 1; i < arrowCount; i++) { // 在拐点处绘制箭头
		var pixelStart = map.pointToPixel(linePoint[i - 1]);
		var pixelEnd = map.pointToPixel(linePoint[i]);
		var angle = angleValue;// 箭头和主线的夹角
		var r = length; // r/Math.sin(angle)代表箭头长度
		var delta = 0; // 主线斜率，垂直时无斜率
		var param = 0; // 代码简洁考虑
		var pixelTemX, pixelTemY;// 临时点坐标
		var pixelX, pixelY, pixelX1, pixelY1;// 箭头两个点
		if (pixelEnd.x - pixelStart.x == 0) { // 斜率不存在是时
			pixelTemX = pixelEnd.x;
			if (pixelEnd.y > pixelStart.y) {
				pixelTemY = pixelEnd.y - r;
			} else {
				pixelTemY = pixelEnd.y + r;
			}
			// 已知直角三角形两个点坐标及其中一个角，求另外一个点坐标算法
			pixelX = pixelTemX - r * Math.tan(angle);
			pixelX1 = pixelTemX + r * Math.tan(angle);
			pixelY = pixelY1 = pixelTemY;
		} else // 斜率存在时
		{
			delta = (pixelEnd.y - pixelStart.y) / (pixelEnd.x - pixelStart.x);
			param = Math.sqrt(delta * delta + 1);

			if ((pixelEnd.x - pixelStart.x) < 0) // 第二、三象限
			{
				pixelTemX = pixelEnd.x + r / param;
				pixelTemY = pixelEnd.y + delta * r / param;
			} else// 第一、四象限
			{
				pixelTemX = pixelEnd.x - r / param;
				pixelTemY = pixelEnd.y - delta * r / param;
			}
			// 已知直角三角形两个点坐标及其中一个角，求另外一个点坐标算法
			pixelX = pixelTemX + Math.tan(angle) * r * delta / param;
			pixelY = pixelTemY - Math.tan(angle) * r / param;

			pixelX1 = pixelTemX - Math.tan(angle) * r * delta / param;
			pixelY1 = pixelTemY + Math.tan(angle) * r / param;
		}

		var pointArrow = map.pixelToPoint(new BMap.Pixel(pixelX, pixelY));
		var pointArrow1 = map.pixelToPoint(new BMap.Pixel(pixelX1, pixelY1));
		var Arrow = new BMap.Polyline(
				[ pointArrow, linePoint[i], pointArrow1 ], {
					strokeColor : "blue",
					strokeWeight : 3,
					strokeOpacity : 1
				});
		map.addOverlay(Arrow);
	}
}  