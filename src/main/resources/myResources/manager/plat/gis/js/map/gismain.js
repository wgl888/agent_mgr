//参数
var mapUrlParam = new Object();
//地图数据
var mapinfo = new Object();
//地图图层
var mapLayers;
//查询条件
var searchParam = new Object();

var calcPopupInfo = null;

function init(){
	doCallPost("doInitMap",mapUrlParam,function(data){
		mapinfo = data;
		initmap(data);
	});
	initLegendMain();
	initMarqueeMain();
	initReloadBtnMain();
	initMenuMain();
	initOverLaysMain();
}

function initLegendMain(){
	setTimeout(function() {
		doCallPost("doInitLegend",mapUrlParam,initlegend);
	}, 500);
}

function initMenuMain(){
	setTimeout(function() {
		doCallPost("doInitMenu",mapUrlParam,doSetMapMenu);
	}, 500);
}

function initReloadBtnMain(){
	setTimeout(function() {
		initReloadBtn();
	}, 500);
}

function initMarqueeMain(){
	var obj = new Object();
	obj['isInit'] = 'true';
	doCallPost("doInitMarquee",obj,doInitMarquee);
	var interval = 60;
	if(mapinfo.delay){
		interval = mapinfo.delay;
	}
	setTimeout(function() {
		obj['isInit'] = 'false';
		doCallPost("doInitMarquee",obj,doInitMarquee);
	}, interval * 1000);
}

function initOverLaysMain(){
	doCallPost("doInitOverLays",mapUrlParam,function(data){
		mapLayers = data;
		if(mapLayers){
			for(var i = 0; i < mapLayers.length; i++){
				mapLayer = mapLayers[i];
				if(mapLayer['initload'] == 'true'){
					addLayerOverlay(mapLayer);
				}else {
					addLayer(mapLayer);
				}
			}
		}
	});
}

function addLayerOverlay(mapLayer){
	var obj = new Object();
	obj.layerJson = JSON.stringify(mapLayer);
	obj.searchParamJson = JSON.stringify(searchParam);
	doCallPost("doAddLayerOverlay",obj,function(data){
		createOverlays(data,mapLayer);
	});
}

function initPoint(){
	var point = new BMap.Point(parseFloat(map.mapinfo.initpx), parseFloat(map.mapinfo.initpy));
	map.centerAndZoom(point, parseInt(map.mapinfo.initzoom));
}

//加载数据
function doCallPost(event,data,fun){
//	$.post(getLocalUrl() + event + ".htm",data,fun);
	$.post(event + ".htm",data,fun);
}

//获取全路径URL
function getLocalUrl(){
	var strFullPath='',strPath='',pos='',prePath='',postPath='',local='';
	strFullPath = window.document.location.href;
	strPath = window.document.location.pathname;
	pos = strFullPath.indexOf(strPath);
	prePath = strFullPath.substring(0, pos);
	postPath = strPath.substring(0, strPath.substr(1).indexOf('/') + 1);
	local = 'http://'+window.location.host+postPath+'/';
	return local;
};

//获取URL参数
function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
	var r = window.location.search.substr(1).match(reg);  //匹配目标参数
	if (r != null) return unescape(r[2]); return null; //返回参数值
};