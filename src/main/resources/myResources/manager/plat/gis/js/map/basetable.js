function createTabsTable(div,tables){
	var randStr = randomString(10);
	var divRoot = document.createElement("div");
	divRoot.setAttribute("class", "col-sm-12");
	divRoot.style.padding = "0px";
	divRoot.setAttribute("id", "main_" + randStr);
	div.appendChild(divRoot);
	
	var divMain = document.createElement("div");
	divMain.setAttribute("class", "tabs-container");
	divRoot.appendChild(divMain);
	
	var ulTabs = document.createElement("ul");
	ulTabs.setAttribute("class", "nav nav-tabs titleTabs");
	divMain.appendChild(ulTabs);
	
	var divTabs = document.createElement("div");
	divTabs.setAttribute("class", "tab-content");
	divMain.appendChild(divTabs);
	var tabStr = "tab-" + randStr + "-";
	for(var i=0;i<tables.length;i++){
		var liTab = document.createElement("li");
		liTab.innerHTML = "<a data-toggle=\"tab\" href=\"#" + tabStr + i + "\">" + tables[i].title + "</a>";
		ulTabs.appendChild(liTab);
		
		var divTab = document.createElement("div");
		divTab.setAttribute("class", "tab-pane");
		divTab.setAttribute("id", tabStr + i);
		divTab.isLoad = false;
		divTabs.appendChild(divTab);
		
		if(i == 0){
			liTab.setAttribute("class", "active");
			divTab.setAttribute("class", "tab-pane active");
			createTable(tables[i],divTab,tables[i].cNames,tables[i].cModels,tables[i].data,$("#main_" + randStr).width());
			divTab.isLoad = true;
		}
		createTab(liTab,divTab,tables[i],$("#main_" + randStr).width());
	}
}

function createTab(liTab,divTab,tableinfo,widthParent){
	liTab.onclick=function(){
		if(!divTab.isLoad){
			createTable(tableinfo,divTab,tableinfo.cNames,tableinfo.cModels,tableinfo.data,widthParent);
			divTab.isLoad = true;
		}else {
			setTimeout(function() {
				var width = $(divTab.maindivid).width();
		    	$(divTab.tabledivid).setGridWidth(width - 2);
			}, 100);
		}
	};
}

function createTable(tableInfo,rootdiv,cNames,cModels,mydata,widthParent){
	var randStr = randomString(10);
	var divMain = document.createElement("div");
	divMain.setAttribute("class", "jqGrid_wrapper");
	divMain.setAttribute("id", "main_" + randStr);
	rootdiv.appendChild(divMain);
	
	var tableMain = document.createElement("table");
	tableMain.setAttribute("id", "table_" + randStr);
	divMain.appendChild(tableMain);
	
	var divPage = document.createElement("div");
	divPage.setAttribute("id", "page_" + randStr);
	divMain.appendChild(divPage);
	
	$("#table_" + randStr).jqGrid({
        data: mydata,
        datatype: "local",
        styleUI: 'Bootstrap',
        colNames: cNames,
        colModel: cModels,
        viewrecords: true,  
        multiselect: false,  
        autowidth: true,  
        rowNum: 20,
        rowList: [20, 30, 40],
        pagerpos : "left",
        pgtext : "{0}共 {1} 页",
        recordtext  : "{0}-{1}  共{2}条",
        hidegrid: false,
        
        height: (tableInfo.height ? tableInfo.height : "auto"),  
        pager: (tableInfo.isPage && tableInfo.isPage == "true" ? "#page_" + randStr:""),
        forceFit : !(tableInfo.isFlowX && tableInfo.isFlowX == "true")
    });
	if(!(tableInfo.isFlowX && tableInfo.isFlowX == "true")){
		$("#table_" + randStr).closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
	}
	var widthTemp = 2;
	if(!widthParent){
		var width = $("#main_" + randStr).width();
		$("#table_" + randStr).setGridWidth(width - widthTemp);
	}else {
		$("#table_" + randStr).setGridWidth(widthParent - widthTemp);
	}
	rootdiv.maindivid = "#main_" + randStr;
	rootdiv.tabledivid = "#table_" + randStr;
    $(window).bind('resize', function () {
    	if($("#" + rootdiv.id).hasClass('active')){
    		var width = $("#main_" + randStr).width();
        	$("#table_" + randStr).setGridWidth(width - widthTemp);
    	}
    });
}

function randomString(len) {
	len = len || 32;
	var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
	var maxPos = $chars.length;
	var pwd = '';
	for (i = 0; i < len; i++) {
		pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
	}
	return pwd;
}