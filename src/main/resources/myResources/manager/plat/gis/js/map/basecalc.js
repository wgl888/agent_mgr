var calcPopupCtrl;
function createCalc(info){
	function CalcPopupControl(info){
		this.defaultAnchor = BMAP_ANCHOR_BOTTOM_RIGHT;
		this.defaultOffset = new BMap.Size(18,18);
		this._info = info;
	}
	CalcPopupControl.prototype = new BMap.Control();
	CalcPopupControl.prototype.initialize = function(map){
		var div = document.createElement("div");
		div.style.width="100px";
		div.style.height="100px";
		div.style.display="block";
		div.setAttribute("class", "ctrl-btn");
		map.getContainer().appendChild(div);
		
		if(calcPopupInfo.width){
			div.style.width=calcPopupInfo.width + "px";
		}
		if(calcPopupInfo.height){
			div.style.height=calcPopupInfo.height + "px";
		}
		
		doCallPost("doInitCalcInfo",mapUrlParam,function(data){
			createTabsTable(div,data);
//			createTable(data[0],div,data[0].cNames,data[0].cModels,data[0].data);
		});
		
	  	return div;
	};
	
	if(info.isShowFilter){
		calcPopupCtrl = new CalcPopupControl(info);
		map.addControl(calcPopupCtrl);
	}else {
		clearCalc();
	}
	
}
function clearCalc(){
	if(calcPopupCtrl){
		map.removeControl(calcPopupCtrl);
		calcPopupCtrl = null;
	}
}

function createCommonTable(rootdiv,jsonheader,jsondata){
	if(jsonheader.length == 0){
		return;
	}
	var ros_num = jsonheader.length;
	rootdiv.style.height  = (parseInt(ros_num*26) + 23) +"px";
	//设置列宽
	var parentWidth = parseInt(rootdiv.style.width)-1;
	
	var tdClass;
	//创建table
	var comTable = document.createElement("table");
	comTable.style.width = rootdiv.style.width;
	
	for(var i=0;i<jsonheader.length;i++){
		var thead = document.createElement("tr");
		var headObj = jsonheader[i];
		if(headObj.tdclass){
			tdClass = headObj.tdclass;
		}
		var th = document.createElement("th");
		th.innerHTML = headObj.label;
		th.setAttribute("title", headObj.label);
		th.style.align = "center";
		th.style.valign = "middle";
		th.style.width = "20px";
		thead.appendChild(th);
	}
	
	for(var i=0;i<jsonheader.length;i++){
		var trdata = document.createElement("tr");
		var td = document.createElement("td");
		if(jsondata[headObj.field]){
			td.innerHTML = jsondata[headObj.field];
		}else {
			td.innerHTML = '';
		}
		td.setAttribute("title", jsondata[headObj.field]);
		td.style.align = "center";
		td.style.valign = "middle";
		td.style.width = "20px";
		
		trdata.appendChild(td);
		comTable.appendChild(trdata);
	}
	rootdiv.appendChild(comTable);	
}