
popup_none = function (title, url, w, h, dom) {
    if (title == null || title == '') {
        title = false;
    }
    layer.open({
        type: 2,
        area: [w + 'px', h + 'px'],
        shade: 0.1,
        offset: 'lb', //右下角弹出
        title: title,
        content: url
    });
};

//弹出带按钮的弹出框
popup_button = function (title, url, w, h, dom) {
    if (title == null || title == '') {
        title = false;
    }
    layer.open({
        type: 2,
        btn: ["选择", "取消"],
        area: [w + 'px', h + 'px'],
        shade: 0.1,
        title: title,
        content: url,
        yes: function (index, layero) {
            var iframeWin = layero.find('iframe');
            $(iframeWin)[0].contentWindow.doSomething(dom);
        }
    });
};

//弹出带按钮的弹出框
popup_button_btn = function (title, url, w, h, dom,btnYes,btnNo) {
    if (title == null || title == '') {
        title = false;
    }
    parent.layer.open({
        type: 2,
        btn: [btnYes, btnNo],
        area: [w + 'px', h + 'px'],
        shade: 0.1,
        title: title,
        content: url,
        yes: function (index, layero) {
            var iframeWin = layero.find('iframe');
            $(iframeWin)[0].contentWindow.doSomething(dom);
        },
        no:function(){
            history.back();
        }
    });
};

//左下角弹框
popover =function (url,title) {
    top.layer.open({
        type: 2,
        title: title,
        closeBtn: 1,
        shade: 0,
        area: ['500px', '200px'],
        offset: 'rb',
        time: 10000,
        anim: 6,
        content: [url, 'no']
    });

};