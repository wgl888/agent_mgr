//将数字转换成金额显示
to_money = function(num){
    num = num.toFixed(2);
    num = parseFloat(num);
    num = num.toLocaleString();
    return num;//返回的是字符串23,245.12保留2位小数
};
//时间戳转标准时间
stamp_to_time = function(timestamp){
    var newDate = new Date();
    newDate.setTime(timestamp);
    return newDate.toLocaleString();
};
//时间戳转日期
stamp_to_date = function(timestamp){
    var newDate = new Date();
    newDate.setTime(timestamp);
    return newDate.toLocaleDateString()
};
// 时间戳转日期, 格式:yyyy-MM-dd
to_date=function(timestamp){
    var date = new Date(timestamp);
    Y = date.getFullYear() + '-';
    M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
    D = date.getDate() + ' ';
    return (Y+M+D);
};

// 时间戳转换为时间口语,例如:3分钟前
stamp_to_msg=function(dateTimeStamp){
    var minute = 1000 * 60;
    var hour = minute * 60;
    var day = hour * 24;
    var month = day * 30;
    var now = new Date().getTime();
    var diffValue = now - dateTimeStamp;
    if(diffValue < 0){return;}
    var monthC =diffValue/month;
    var weekC =diffValue/(7*day);
    var dayC =diffValue/day;
    var hourC =diffValue/hour;
    var minC =diffValue/minute;
    if(monthC>=1){
        result="" + parseInt(monthC) + "月前";
    }
    else if(weekC>=1){
        result="" + parseInt(weekC) + "周前";
    }
    else if(dayC>=1){
        result=""+ parseInt(dayC) +"天前";
    }
    else if(hourC>=1){
        result=""+ parseInt(hourC) +"小时前";
    }
    else if(minC>=1){
        result=""+ parseInt(minC) +"分钟前";
    }else
        result="刚刚";
    return result;
};

// 取?个小时之前的时间
before_hour_time = function(hour){
    var date = new Date(); //日期对象
    date.setHours (date.getHours () - hour);
    var now;
    now = date.getFullYear()+"-"; //读英文就行了
    now = now + (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';//取月的时候取的是当前月-1如果想取当前月+1就可以了
    now = now + setFormat(date.getDate())+" ";
    now = now + setFormat(date.getHours())+":";
    now = now + setFormat(date.getMinutes())+":";
    now = now + setFormat(date.getSeconds())+"";
    return now;
};

setFormat = function (x) {
    if (x < 10) {x = "0" + x;}
    return x;
};