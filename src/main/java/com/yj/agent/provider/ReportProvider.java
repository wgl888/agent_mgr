package com.yj.agent.provider;

import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.evt.QueryWithDrawEvt;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.jdbc.SQL;

public class ReportProvider {


    public String queryPerson(QueryAgentUserEvt evt) {
        SQL sql = new SQL().SELECT(" count(1),\n" +
                " IFNULL(sum(case when agentLevel = 2 then 1 else 0 end),0) as agentCount,\n" +
                " IFNULL(sum(case when agentLevel = 3 then 1 else 0 end),0) as gamerCount")
                .FROM("t_agent_user")
            .WHERE("parentGameId = '" + evt.getGameId() + "'")
                .WHERE("status = 'E'");
        return sql.toString();
    }

}
