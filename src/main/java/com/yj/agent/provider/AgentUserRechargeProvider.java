package com.yj.agent.provider;

import com.yj.agent.bean.TAgentUserRechargeBean;

import java.util.List;
import java.util.Map;

public class AgentUserRechargeProvider {

    public String insertList(Map map) {
        List<TAgentUserRechargeBean> list = (List<TAgentUserRechargeBean>) map.get("list");
        String sql = "INSERT INTO t_agent_user_recharge (recordId,gameId,amount,rechargeTime,status,createTime,updateTime) VALUES ";
        for (TAgentUserRechargeBean obj : list) {
            sql += "('" + obj.getRecordId() + "','" + obj.getGameId() + "', " + obj.getAmount() + ", '" + obj.getRechargeTime() + "','" + obj.getStatus() + "',now(),now()),";
        }
        sql = sql.substring(0, sql.length() - 1);
        return sql;
    }
}
