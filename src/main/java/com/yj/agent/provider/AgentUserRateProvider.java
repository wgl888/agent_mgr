package com.yj.agent.provider;

import com.yj.agent.bean.TAgentUserRateBean;
import com.yj.agent.bean.TAgentUserRechargeBean;

import java.util.List;
import java.util.Map;

public class AgentUserRateProvider {

    public String insertList(Map map) {
        List<TAgentUserRateBean> list = (List<TAgentUserRateBean>) map.get("list");
        String sql = "INSERT INTO t_agent_user_rate (agentGameId,gameId,amount,status,createTime,updateTime) VALUES ";
        for (TAgentUserRateBean obj : list) {
            sql += "('" + obj.getAgentGameId() + "','" + obj.getGameId() + "', " + obj.getAmount() + ",'" + obj.getStatus() + "',now(),now()),";
        }
        sql = sql.substring(0, sql.length() - 1);
        return sql;
    }
}
