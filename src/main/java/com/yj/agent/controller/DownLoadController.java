package com.yj.agent.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/down")
public class DownLoadController extends BaseController {

    @RequestMapping("/index")
    public String index(HttpServletRequest request, Model model) {
        return "/down/download";
    }

}