package com.yj.agent.controller;

import cn.com.yj.common.enums.YesOrNo;
import cn.com.yj.common.param.out.ServiceResp;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yj.agent.bean.LoginModel;
import com.yj.agent.util.AESUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class BaseController {

    final String key = "izUxyta7f7jHqmS9";

    public ServiceResp ssoLogin(HttpServletRequest request){
        try {
            LoginModel loginModel = (LoginModel) request.getSession().getAttribute("LOGIN_USER");
            String session = request.getParameter("session");
            if(StringUtils.isBlank(session)&&request.getSession().getAttribute("LOGIN_USER")==null){
                return new ServiceResp().error("登录失败:参数缺失");
            }
            if(StringUtils.isNotBlank(session)){
                session = new String(AESUtils.decrypt(session, key, key));
                JSONObject jsonObject = JSON.parseObject(session);
                if(StringUtils.isBlank(jsonObject.getString("gamerId"))
                        ||StringUtils.isBlank(jsonObject.getString("isMgr"))){
                    return new ServiceResp().error("登录失败:参数错误");
                }
                loginModel = new LoginModel();
                loginModel.setGamerId(jsonObject.getString("gamerId"));
                loginModel.setIsMgr(jsonObject.getString("isMgr"));
                request.getSession().setAttribute("LOGIN_USER",loginModel);
            }
            return new ServiceResp().success(loginModel);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统异常");
        }
    }

    public String getLoginGamerId(HttpServletRequest request){
        LoginModel loginModel = (LoginModel) request.getSession().getAttribute("LOGIN_USER");
        return loginModel!=null?loginModel.getGamerId():"";
    }

    public boolean isMgr(HttpServletRequest request){
        LoginModel loginModel = (LoginModel) request.getSession().getAttribute("LOGIN_USER");
        return YesOrNo.Y.name().equals(loginModel.getIsMgr())?true:false;
    }

}