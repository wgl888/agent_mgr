package com.yj.agent.controller;

import cn.com.yj.common.param.out.ServiceResp;
import com.alibaba.fastjson.JSON;
import com.yj.agent.evt.AddAgentUserEvt;
import com.yj.agent.evt.BindUserEvt;
import com.yj.agent.service.SysnService;
import com.yj.agent.service.TAgentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
public class AppController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TAgentUserService tAgentUserService;
    @Autowired
    private SysnService sysnService;

    @RequestMapping("/getUrl")
    public String getUrl(String userId) {
        return "http://xkdwc.net:8001/down/index?userId=" + userId;
    }

    @RequestMapping("/bind")
    public ServiceResp bind(String superUserId, String subUserId, String userName, String account, String registerTime) throws Exception {
        logger.info(" bind method is run .");
        AddAgentUserEvt addAgentUserEvt = new AddAgentUserEvt();
        addAgentUserEvt.setAccount(account);
        addAgentUserEvt.setGameId(subUserId);
        addAgentUserEvt.setParentGameId(superUserId);
        addAgentUserEvt.setAgentLevel(3);
        addAgentUserEvt.setUserName(userName);
        addAgentUserEvt.setRegisterTime(registerTime);
        logger.info(" add evt value is :" + JSON.toJSONString(addAgentUserEvt));
        ServiceResp resp = tAgentUserService.add(addAgentUserEvt);
        logger.info(" add result is " + JSON.toJSONString(resp));
        resp.setBody(null);
        return resp;
    }

    @RequestMapping("/bind1")
    public ServiceResp bind1(@RequestBody BindUserEvt evt) throws Exception {
        AddAgentUserEvt addAgentUserEvt = new AddAgentUserEvt();
        addAgentUserEvt.setAccount(evt.getAccount());
        addAgentUserEvt.setGameId(evt.getSubUserId());
        addAgentUserEvt.setParentGameId(evt.getSuperUserId());
        addAgentUserEvt.setUserName(evt.getUserName());
        addAgentUserEvt.setRegisterTime(evt.getRegisterTime());
        ServiceResp resp = tAgentUserService.add(addAgentUserEvt);
        resp.setBody(null);
        return resp;
    }
}