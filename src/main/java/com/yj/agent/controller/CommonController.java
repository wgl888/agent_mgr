package com.yj.agent.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/common")
public class CommonController extends BaseController {


    @RequestMapping("/error")
    public String error(HttpServletRequest request,Model model) {
        return "/common/error";
    }


}