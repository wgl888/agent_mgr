package com.yj.agent.controller;

import cn.com.yj.common.param.out.ServiceResp;
import com.yj.agent.bean.TAgentUserBean;
import com.yj.agent.bean.TWithDrawBean;
import com.yj.agent.evt.*;
import com.yj.agent.model.ExcelModel;
import com.yj.agent.service.TAgentUserService;
import com.yj.agent.service.TWithDrawService;
import com.yj.agent.util.ExcelUtils;
import com.yj.agent.util.LayTableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/withDraw")
public class WithDrawController {

    @Autowired
    private TAgentUserService tAgentUserService;
    @Autowired
    private TWithDrawService tWithDrawService;

    @ResponseBody
    @RequestMapping("/auditBatch")
    public ServiceResp auditBatch(AuditBatchEvt evt) {
        try {
            return tWithDrawService.auditBatch(evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统异常");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/export",method = RequestMethod.POST)
    public void export(QueryWithDrawEvt evt, HttpServletResponse response){
        try{
            ExcelModel data = new ExcelModel();
            data.setName("任务明细数据");
            List<String> titles = new ArrayList<>();
            titles.add("ID");
            titles.add("游戏ID");
            titles.add("用户账号");
            titles.add("姓名");
            titles.add("代理类型");
            titles.add("代理时间");
            titles.add("代理分成总金币");
            titles.add("已提现金币");
            titles.add("可提现金币");
            titles.add("本次申请金币");
            titles.add("申请时间");
            titles.add("状态");
            data.setTitles(titles);
            List<TWithDrawBean> list = tWithDrawService.export(evt);
            List<List<Object>> rows = new ArrayList();
            NumberFormat nf = new DecimalFormat("##.####");
            if(list!=null&&list.size()>0){
                for(TWithDrawBean item : list){
                    List<Object> row = new ArrayList();
                    row.add(item.getId());
                    row.add(item.getGameId());
                    row.add(item.getAccount());
                    row.add(item.getUserName());
                    row.add(item.getAgentLevel()==1?
                            "一级代理":item.getAgentLevel()==2?"二级代理":item.getAgentLevel()==3?"三级代理":"");
                    row.add(item.getAgentTimeStr());
                    row.add(item.getProfitTotal());
                    row.add(item.getProfitUsedTotal());
                    row.add(item.getCanTotal());
                    row.add(item.getAmount());
                    row.add(item.getCreateTimeStr());
                    row.add("DSH".equals(item.getAuditStatus())?"待审核":
                            "SHTG".equals(item.getAuditStatus())?"审核通过":
                                    "SHBTG".equals(item.getAuditStatus())?"审核不通过":"");
                    rows.add(row);
                }
            }
            data.setRows(rows);
            ExcelUtils.exportExcel(response,"用户提现记录" + new Date().getTime() + ".xlsx",data);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}