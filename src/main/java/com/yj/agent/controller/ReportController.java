package com.yj.agent.controller;

import cn.com.yj.common.param.out.ServiceResp;
import com.yj.agent.bean.LoginModel;
import com.yj.agent.bean.ReportModelBean;
import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


// 报表页面
@Controller
@RequestMapping("/report")
public class ReportController extends BaseController{

    @Autowired
    private ReportService reportService;


    @RequestMapping("/index")
    public String index(HttpServletRequest request, Model model) {
        ServiceResp serviceResp = ssoLogin(request);
        if(!serviceResp.success())return "/common/error";
        // 获取当前登录用户
        String gameId = getLoginGamerId(request);
        // 查询数据
        QueryAgentUserEvt queryAgentUserEvt = new QueryAgentUserEvt();
        queryAgentUserEvt.setGameId(gameId);
        queryAgentUserEvt.setMgr(isMgr(request));
        ReportModelBean bean = reportService.index(queryAgentUserEvt);
        model.addAttribute("item",bean);
        return "/report/index";
    }

}