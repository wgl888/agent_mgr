package com.yj.agent.controller;

import cn.com.yj.common.param.out.ServiceResp;
import com.yj.agent.service.SysnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/sysn")
public class SysnController {

    @Autowired
    private SysnService sysnService;

    @ResponseBody
    @RequestMapping("/sysnUserPayInfo")
    public ServiceResp sysnUserPayInfo(String startTime, String endTime) {
        try {
            sysnService.sysnUserPayInfo(startTime, endTime);
            return new ServiceResp().success("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error(
                    e.getMessage());
        }
    }
}