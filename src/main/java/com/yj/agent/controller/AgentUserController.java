package com.yj.agent.controller;

import cn.com.yj.common.param.out.ServiceResp;
import com.yj.agent.bean.LoginModel;
import com.yj.agent.bean.TAgentUserBean;
import com.yj.agent.bean.TWithDrawBean;
import com.yj.agent.evt.*;
import com.yj.agent.service.TAgentUserService;
import com.yj.agent.service.TWithDrawService;
import com.yj.agent.util.LayTableUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/agent/user")
public class AgentUserController extends BaseController {

    @Autowired
    private TAgentUserService tAgentUserService;
    @Autowired
    private TWithDrawService tWithDrawService;


    // 代理商列表页面只有管理员可以查看
    @RequestMapping("/list")
    public String list(HttpServletRequest request,Model model) {
        ServiceResp serviceResp = ssoLogin(request);
        if(!serviceResp.success())return "/common/error";
        if(!isMgr(request)){
            return "/common/error";
        }
        return "/agentUser/list";
    }


    @ResponseBody
    @RequestMapping("/bindAagentBatch")
    public ServiceResp bindAagentBatch(BindAgentUserBatchEvt evt,HttpServletRequest request) {
        try {
            if(StringUtils.isBlank(evt.getSuperUserId()))
                return new ServiceResp().error("缺数缺数");
            // 会话信息里面获取当前登录的用户
            return tAgentUserService.bindBatch(evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统异常");
        }
    }



    /** 查询2级代理的下级玩家 */
    @RequestMapping("/gamerList")
    public String gamerList(Model model, QueryAgentUserEvt evt,HttpServletRequest request) {
        model.addAttribute("parentUserNo",evt.getGameId());
        return "/agentUser/gamerList";
    }

    @ResponseBody
    @RequestMapping("/queryGamerList")
    public String queryGamerList(QueryAgentUserEvt evt,HttpServletRequest request) {
        try {
            evt.setAgentLevel(3);
            List<TAgentUserBean> list = tAgentUserService.query(evt);
            Integer respCount = tAgentUserService.count(evt);
            return LayTableUtils.render(respCount, list);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
    }


    @ResponseBody
    @RequestMapping("/page")
    public String page(QueryAgentUserEvt evt) {
        try {
            evt.setCreateUser(999L);
            evt.setNotEqAgentLevel(3);
            List<TAgentUserBean> list = tAgentUserService.query(evt);
            Integer respCount = tAgentUserService.count(evt);
            return LayTableUtils.render(respCount, list);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
    }


    @RequestMapping("/toQueryGamer")
    public String toQueryGamer(QueryAgentUserEvt evt,Model model, HttpServletRequest request) {
        model.addAttribute("parentUserNo",evt.getParentUserNo());
        return "/agentUser/chooseGamer";
    }

    @ResponseBody
    @RequestMapping("/queryGamer")
    public String queryGamer(QueryAgentUserEvt evt,HttpServletRequest request) {
        try {
            evt.setAgentLevel(3);
            List<TAgentUserBean> list = tAgentUserService.query(evt);
            Integer respCount = tAgentUserService.count(evt);
            return LayTableUtils.render(respCount, list);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
    }

    @RequestMapping("/subList")
    public String subList(Model model, QueryAgentUserEvt evt,HttpServletRequest request) {
        ServiceResp serviceResp = ssoLogin(request);
        if(!serviceResp.success())return "/common/error";
        if(StringUtils.isBlank(evt.getGameId())){ // 如果没有传取session
            evt.setGameId(getLoginGamerId(request));
            if(!isMgr(request)){
                TAgentUserBean parentUser = tAgentUserService.queryByUserNo(evt);
                if(parentUser==null)return "/common/error";
                model.addAttribute("parentUser", parentUser);
            }
            model.addAttribute("isMgr", isMgr(request));
        }else{
            // 带过来的
            TAgentUserBean parentUser = tAgentUserService.queryByUserNo(evt);
            model.addAttribute("parentUser", parentUser);
            evt.setGameId(getLoginGamerId(request));
            if(!isMgr(request)){
                TAgentUserBean nowUser = tAgentUserService.queryByUserNo(evt);
                if(nowUser==null)return "/common/error";
                model.addAttribute("nowUser", nowUser);
            }
            model.addAttribute("isMgr", isMgr(request));
        }
        return "/agentUser/subList";
    }

    @ResponseBody
    @RequestMapping("/userDetail")
    public TAgentUserBean userDetail(QueryAgentUserEvt evt,HttpServletRequest request) {
        TAgentUserBean item = tAgentUserService.queryDetail(evt);
        return item;
    }

    @ResponseBody
    @RequestMapping("/subPage")
    public String subPage(QueryAgentUserEvt evt,HttpServletRequest request) {
        try {
            List<TAgentUserBean> list = tAgentUserService.subQuery(evt);
            Integer respCount = tAgentUserService.subCount(evt);
            return LayTableUtils.render(respCount, list);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/cancleBind",method = RequestMethod.POST)
    public ServiceResp cancleBind(EditAgentUserEvt evt) {
        try {
            return tAgentUserService.cancleBind(evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统异常");
        }
    }

    @RequestMapping("toAdd")
    public String toAdd(Model model) {
        return "/agentUser/edit";
    }

    @RequestMapping(value = "doAdd", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doAdd(AddAgentUserEvt evt, HttpServletRequest request) {
        try {
            evt.setCreateUser(999L);
            //  如果是1级代理,两个比例都不能为空
            if(evt.getAgentLevel()==1&&(evt.getProfitRate()==null||evt.getSubProfitRate()==null)){
                return new ServiceResp().error("代理分成比例不能为空");
            }
            if(evt.getAgentLevel()==2&&evt.getProfitRate()==null){
                return new ServiceResp().error("代理分成比例不能为空");
            }
           return tAgentUserService.add(evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toEdit")
    public String toEdit(QueryAgentUserEvt evt,
                         Model model) {
        try {
            if (evt.getId() == null) {
                return "/common/error";
            }
            List<TAgentUserBean> list = tAgentUserService.query(evt);
            model.addAttribute("item", list != null ? list.get(0) : null);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
        return "/agentUser/edit";
    }

    @RequestMapping(value = "doEdit", method = RequestMethod.POST)
    @ResponseBody
    public ServiceResp doEdit(EditAgentUserEvt evt, HttpServletRequest request) {
        try {
            return new ServiceResp().success(tAgentUserService.edit(evt));
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统错误");
        }
    }

    @RequestMapping("/toApplyWithDraw")
    public String toApplyWithDraw(QueryAgentUserEvt evt,Model model,HttpServletRequest request) {
        ServiceResp serviceResp = ssoLogin(request);
        if(!serviceResp.success())return "/common/error";
        if(StringUtils.isBlank(evt.getGameId())){
            evt.setGameId(getLoginGamerId(request));
        }
        TAgentUserBean user = tAgentUserService.queryByUserNo(evt);
        model.addAttribute("user", user);
        return "/agentUser/applyWithDraw";
    }

    @ResponseBody
    @RequestMapping("/doApplyWithDraw")
    public ServiceResp doApplyWithDraw(DoApplyWithDrawEvt evt) {
        try {
            return tAgentUserService.doApplyWithDraw(evt);
        } catch (Exception e) {
            e.printStackTrace();
            return new ServiceResp().error("系统异常");
        }
    }

    @RequestMapping("/withDrawList")
    public String withDrawList(HttpServletRequest request) {
        ServiceResp serviceResp = ssoLogin(request);
        if(!serviceResp.success())return "/common/error";
        return "/withdraw/list";
    }

    @ResponseBody
    @RequestMapping("/withDrawPage")
    public String withDrawPage(QueryWithDrawEvt evt) {
        try {
            List<TWithDrawBean> list = tWithDrawService.query(evt);
            Integer respCount = tWithDrawService.count(evt);
            return LayTableUtils.render(respCount, list);
        } catch (Exception e) {
            e.printStackTrace();
            return "/common/error";
        }
    }


}