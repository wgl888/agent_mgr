package com.yj.agent.mapper;

import com.yj.agent.provider.AgentUserRateProvider;
import com.yj.agent.provider.AgentUserRechargeProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface AgentUserRateMapper {

	@InsertProvider(type = AgentUserRateProvider.class , method = "insertList")
	int insertList(Map map);
}
