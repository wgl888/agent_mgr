package com.yj.agent.mapper;

import com.yj.agent.bean.ReportModelBean;
import com.yj.agent.bean.TAgentUserBean;
import com.yj.agent.bean.TWithDrawBean;
import com.yj.agent.bean.UserRateModel;
import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.evt.QueryWithDrawEvt;
import com.yj.agent.provider.AgentUserProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@Mapper
public interface AgentUserMapper {

    @SelectProvider(type = AgentUserProvider.class, method = "sumProfit")
    Long sumProfit(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "rechargeTop")
    List<ReportModelBean> rechargeTop(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "rechargeCount")
    int rechargeCount(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "teamCount")
    int teamCount(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "queryCount")
    int queryCount(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "queryByParam")
    List<TAgentUserBean> queryByParam(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "querySubCount")
    int querySubCount(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "queryByGameId")
    TAgentUserBean queryByGameId(String gameId);

    @SelectProvider(type = AgentUserProvider.class, method = "queryDetail")
    TAgentUserBean queryDetail(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "querySubByParam")
    List<TAgentUserBean> querySubByParam(QueryAgentUserEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "queryByParam")
    TAgentUserBean queryByUserNo(QueryAgentUserEvt evt);

	/* 代理商 */

    @SelectProvider(type = AgentUserProvider.class, method = "queryWithDrawCount")
    int queryWithDrawCount(QueryWithDrawEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "queryWithDrawByParam")
    List<TWithDrawBean> queryWithDrawByParam(QueryWithDrawEvt evt);

    @SelectProvider(type = AgentUserProvider.class, method = "queryWithDrawByParam")
    TWithDrawBean queryWithDrawById(QueryWithDrawEvt evt);

    @Select("SELECT a2.gameId AS gameId2, a2.profitRate AS profitRate2, a1.gameId AS gameId1, a1.profitRate AS profitRate1," +
            " a1.subProfitRate AS subProfitRate1 FROM t_agent_user au" +
            " LEFT JOIN t_agent_user a2 ON au.parentGameId = a2.gameId " +
            " LEFT JOIN t_agent_user a1 ON a2.parentGameId = a1.gameId " +
            " WHERE au.gameId = #{gameId}")
    UserRateModel getRate(@Param("gameId") String gameId);


    @Select("select getChildLst(#{param}) from dual")
    String ids(@Param("param") String param);
}
