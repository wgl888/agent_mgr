package com.yj.agent.mapper;

import com.yj.agent.bean.TAgentUserBean;
import com.yj.agent.bean.TAgentUserRechargeBean;
import com.yj.agent.bean.TWithDrawBean;
import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.evt.QueryWithDrawEvt;
import com.yj.agent.provider.AgentUserProvider;
import com.yj.agent.provider.AgentUserRechargeProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

@Mapper
public interface AgentUserRechargeMapper {

	@InsertProvider(type = AgentUserRechargeProvider.class , method = "insertList")
	int insertList(Map map);


	@Select("SELECT * FROM t_agent_user_recharge")
	List<TAgentUserRechargeBean> queryBeans();

}
