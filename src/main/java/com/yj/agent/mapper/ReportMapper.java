package com.yj.agent.mapper;

import com.yj.agent.bean.ReportModelBean;
import com.yj.agent.bean.TAgentUserBean;
import com.yj.agent.bean.TWithDrawBean;
import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.evt.QueryWithDrawEvt;
import com.yj.agent.provider.AgentUserProvider;
import com.yj.agent.provider.ReportProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@Mapper
public interface ReportMapper {


	@SelectProvider(type = ReportProvider.class , method = "queryPerson")
	ReportModelBean queryPerson(QueryAgentUserEvt evt);

}
