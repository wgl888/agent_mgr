package com.yj.agent;

import com.yj.agent.util.SpringContextUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author wanggl
 * @version V1.0
 * @Title: App
 * @Package com.wgl.springboot.test
 * @Description: (入口类)
 * @date 2016/11/30 10:07
 */
// SpringBoot项目的Bean装配默认规则是根据Application类所在的包位置从上往下扫描！
// “Application类”是指SpringBoot项目入口类。
// 如果Application类所在的包为：com.doone.jx.app，则只会扫描com.doone.jx.app包及其所有子包，
// 如果service或dao所在包不在 com.doone.jx.app 及其子包下，则不会被扫描！
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan
@ServletComponentScan
@SpringBootApplication  // 代替 @Configuration,@EnableAutoConfiguration,@ComponentScan
@MapperScan(basePackages = {"cn.com.yj","com.yj.agent"})
@ComponentScan(basePackages={"cn.com.yj","com.yj.agent"})
@EnableJpaRepositories(basePackages = {"cn.com.yj.common.repository"})
@EntityScan(basePackages = {"cn.com.yj","com.yj.agent"})
public class Application extends SpringBootServletInitializer { // 继承WebMvcConfigurationSupport 定制url匹配规则

    @Override
    protected SpringApplicationBuilder configure(
            SpringApplicationBuilder application) {
        SpringApplicationBuilder springApplicationBuilder =
                application.sources(Application.class);
        return springApplicationBuilder;
    }

    public static void main(String[] args) {
        ApplicationContext app = SpringApplication.run(Application.class, args);
        SpringContextUtil.setApplicationContext(app);
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                container.setSessionTimeout(86400);//单位为S
            }
        };
    }

}
