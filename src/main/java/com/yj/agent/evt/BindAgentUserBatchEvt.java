package com.yj.agent.evt;

import cn.com.yj.common.param.in.EditEvt;
import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class BindAgentUserBatchEvt extends EditEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    /** 上级用户标识 */
    @ValidField(value = "superUserId",length = 128,nullable = false)
    private String superUserId;
    /** 下级用户标识 */
    @ValidField(value = "subUserIds",length = 18,nullable = false)
    private String subUserIds;

    public String getSuperUserId() {
        return superUserId;
    }

    public void setSuperUserId(String superUserId) {
        this.superUserId = superUserId;
    }

    public String getSubUserIds() {
        return subUserIds;
    }

    public void setSubUserIds(String subUserIds) {
        this.subUserIds = subUserIds;
    }
}
