package com.yj.agent.evt;

import cn.com.yj.common.param.in.AddEvt;
import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class BaseEvt extends AddEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    private String session;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}
