package com.yj.agent.evt;

import cn.com.yj.common.param.in.EditEvt;
import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class DoApplyWithDrawEvt extends EditEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    /** 编码 */
    @ValidField(value = "userNo",length = 32,nullable = false)
    private String userNo;
    /** 本次提现金额 */
    @ValidField(value = "amount",length = 18,nullable = false)
    private Long amount;

    private String gameId;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
