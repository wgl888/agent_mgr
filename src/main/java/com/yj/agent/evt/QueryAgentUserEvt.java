package com.yj.agent.evt;

import cn.com.yj.common.param.in.QueryEvt;
import cn.com.yj.common.param.valid.ValidField;

import javax.persistence.Column;
import java.io.Serializable;

public class QueryAgentUserEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;
    private Long id;
    private String userNo;
    /** 账号 */
    private String account;
    private String accountEq;
    /** 姓名 */
    private String userName;
    /** 代理时间开始 */
    private String startTime;
    /** 代理时间结束 */
    private String endTime;
    /** 父级ID */
    private String parentUserNo;
    /** 类型 */
    private Integer agentLevel;
    private Integer notEqAgentLevel;
    private String gameId;
    private String agentId;
    private Long createUser;

    /** 上级账号 */
    private String parentAccount;
    /** 上级姓名 */
    private String parentUserName;

    private String userNameEq;

    private String inConditon;

    public String getInConditon() {
        return inConditon;
    }

    public void setInConditon(String inConditon) {
        this.inConditon = inConditon;
    }

    public String getUserNameEq() {
        return userNameEq;
    }

    public void setUserNameEq(String userNameEq) {
        this.userNameEq = userNameEq;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    private boolean now;

    public boolean isNow() {
        return now;
    }

    public void setNow(boolean now) {
        this.now = now;
    }

    public Integer getNotEqAgentLevel() {
        return notEqAgentLevel;
    }

    public void setNotEqAgentLevel(Integer notEqAgentLevel) {
        this.notEqAgentLevel = notEqAgentLevel;
    }

    public String getAccountEq() {
        return accountEq;
    }

    public void setAccountEq(String accountEq) {
        this.accountEq = accountEq;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Integer getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(Integer agentLevel) {
        this.agentLevel = agentLevel;
    }

    private boolean isMgr = false;

    public boolean isMgr() {
        return isMgr;
    }

    public void setMgr(boolean mgr) {
        isMgr = mgr;
    }

    public String getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(String parentAccount) {
        this.parentAccount = parentAccount;
    }

    public String getParentUserName() {
        return parentUserName;
    }

    public void setParentUserName(String parentUserName) {
        this.parentUserName = parentUserName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getParentUserNo() {
        return parentUserNo;
    }

    public void setParentUserNo(String parentUserNo) {
        this.parentUserNo = parentUserNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}

