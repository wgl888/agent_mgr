package com.yj.agent.evt;

import cn.com.yj.common.param.in.EditEvt;
import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class EditAgentUserEvt extends EditEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    @ValidField(length = 11,nullable = false)
    private Long id;
    /** 分成比例 */
    @ValidField(value = "profitRate",length = 18,nullable = true)
    private Float profitRate;
    /** 二级分成比例 */
    @ValidField(value = "subProfitRate",length = 18,nullable = true)
    private Float subProfitRate;
    /** 姓名 */
    @ValidField(value = "userName",length = 128,nullable = true)
    private String userName;
    /** 代理级别 */
    @ValidField(value = "agentLevel",length = 18,nullable = true)
    private Integer agentLevel;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(Integer agentLevel) {
        this.agentLevel = agentLevel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getProfitRate() {
        return profitRate;
    }

    public void setProfitRate(Float profitRate) {
        this.profitRate = profitRate;
    }

    public Float getSubProfitRate() {
        return subProfitRate;
    }

    public void setSubProfitRate(Float subProfitRate) {
        this.subProfitRate = subProfitRate;
    }
}
