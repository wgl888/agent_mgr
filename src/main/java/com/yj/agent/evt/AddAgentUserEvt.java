package com.yj.agent.evt;

import cn.com.yj.common.param.in.AddEvt;
import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

@ApiModel
public class AddAgentUserEvt extends AddEvt implements Serializable {

    private static final long serialVersionUID = -7779844561001652173L;
    /**
     * 其他系统的标识
     */
    @ValidField(value = "gameId", length = 128, nullable = false)
    private String gameId;
    /**
     * 账号
     */
    @ValidField(value = "account", length = 128, nullable = false)
    private String account;
    /**
     * 姓名
     */
    @ValidField(value = "userName", length = 128, nullable = true)
    private String userName;
    /**
     * 代理级别
     */
    @ValidField(value = "agentLevel", length = 18, nullable = true)
    private Integer agentLevel;
    /**
     * 分成比例
     */
    @ValidField(value = "profitRate", length = 18, nullable = true)
    private Float profitRate;
    /**
     * 二级分成比例
     */
    @ValidField(value = "subProfitRate", length = 18, nullable = true)
    private Float subProfitRate;
    /**
     * 父用户编码
     */
    @ValidField(value = "parentGameId", length = 128, nullable = true)
    private String parentGameId;

    /** 注册时间 */
    @ValidField(value = "registerTime",nullable = true)
    private String registerTime;

    private Long createUser;

    @Override
    public Long getCreateUser() {
        return createUser;
    }

    @Override
    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }
    public String getParentGameId() {
        return parentGameId;
    }

    public void setParentGameId(String parentGameId) {
        this.parentGameId = parentGameId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(Integer agentLevel) {
        this.agentLevel = agentLevel;
    }

    public Float getProfitRate() {
        return profitRate;
    }

    public void setProfitRate(Float profitRate) {
        this.profitRate = profitRate;
    }

    public Float getSubProfitRate() {
        return subProfitRate;
    }

    public void setSubProfitRate(Float subProfitRate) {
        this.subProfitRate = subProfitRate;
    }

    @Override
    public String toString() {
        return "AddAgentUserEvt{" +
                "gameId='" + gameId + '\'' +
                ", account='" + account + '\'' +
                ", userName='" + userName + '\'' +
                ", agentLevel=" + agentLevel +
                ", profitRate=" + profitRate +
                ", subProfitRate=" + subProfitRate +
                ", parentGameId='" + parentGameId + '\'' +
                ", registerTime='" + registerTime + '\'' +
                '}';
    }
}
