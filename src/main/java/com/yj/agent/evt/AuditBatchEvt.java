package com.yj.agent.evt;

import cn.com.yj.common.param.in.AddEvt;
import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class AuditBatchEvt extends AddEvt implements Serializable{

    private static final long serialVersionUID = -4525609018167831289L;

    private String jsonValue;
    private String auditStatus;

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getJsonValue() {
        return jsonValue;
    }

    public void setJsonValue(String jsonValue) {
        this.jsonValue = jsonValue;
    }
}
