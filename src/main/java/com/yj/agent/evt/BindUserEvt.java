package com.yj.agent.evt;

import cn.com.yj.common.param.valid.ValidField;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel
public class BindUserEvt implements Serializable {

    private static final long serialVersionUID = 5263206970900016137L;
    /**
     * 上级用户标识
     */
    @ValidField(value = "superUserId", length = 32, nullable = false)
    private String superUserId;
    /**
     * 下级用户标识
     */
    @ValidField(value = "subUserId", length = 32, nullable = false)
    private String subUserId;
    /**
     * 用户姓名
     */
    @ValidField(value = "userName", length = 32, nullable = false)
    private String userName;
    /**
     * 用户账号
     */
    @ValidField(value = "account", length = 32, nullable = false)
    private String account;
    /**
     * 注册时间
     */
    @ValidField(value = "registerTime", length = 32, nullable = false)
    private String registerTime;

    public String getSuperUserId() {
        return superUserId;
    }

    public void setSuperUserId(String superUserId) {
        this.superUserId = superUserId;
    }

    public String getSubUserId() {
        return subUserId;
    }

    public void setSubUserId(String subUserId) {
        this.subUserId = subUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }
}
