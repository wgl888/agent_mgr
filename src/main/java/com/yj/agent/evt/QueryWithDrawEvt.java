package com.yj.agent.evt;

import cn.com.yj.common.param.in.QueryEvt;

import java.io.Serializable;

public class QueryWithDrawEvt extends QueryEvt implements Serializable {

    private static final long serialVersionUID = -9036264265923229125L;
    private Long id;
    private String userNo;
    /** 账号 */
    private String account;
    /** 姓名 */
    private String userName;
    /** 代理时间开始 */
    private String startTime;
    /** 代理时间结束 */
    private String endTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}

