package com.yj.agent.service;

import com.yj.agent.bean.ReportModelBean;
import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.mapper.AgentUserMapper;
import com.yj.agent.mapper.ReportMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class ReportService {

    @Autowired
    private ReportMapper reportMapper;
    @Autowired
    private AgentUserMapper agentUserMapper;


    public ReportModelBean index(QueryAgentUserEvt evt) {
        ReportModelBean reportModelBean = new ReportModelBean();

        String inCondition = "";
        String in = agentUserMapper.ids((evt.isMgr()?"-1":evt.getGameId()));
        if(StringUtils.isBlank(in)){
            return new ReportModelBean();
        }
        String[] inArray = StringUtils.split(in,",");

        for(String id : inArray){
            inCondition += "'" + id + "',";
        }
        inCondition = StringUtils.substring(inCondition,0,inCondition.length()-1);
        // 团队总人数
        QueryAgentUserEvt queryAgentUserEvt = new QueryAgentUserEvt();
        queryAgentUserEvt.setInConditon(inCondition);
        Integer teamCount = agentUserMapper.teamCount(queryAgentUserEvt);
        reportModelBean.setAllCount(teamCount);
        // 代理人数
        queryAgentUserEvt.setNotEqAgentLevel(3);
        Integer teamAgentCount = agentUserMapper.teamCount(queryAgentUserEvt);
        reportModelBean.setAgentCount(teamAgentCount);
        // 玩家人数
        queryAgentUserEvt.setNotEqAgentLevel(null);
        queryAgentUserEvt.setAgentLevel(3);
        Integer teamGamerCount = agentUserMapper.teamCount(queryAgentUserEvt);
        reportModelBean.setGamerCount(teamGamerCount);
        // 充值总和
        Integer rechargeCount = agentUserMapper.rechargeCount(queryAgentUserEvt);
        reportModelBean.setRechargeTotal(rechargeCount);
        // 今日充值
        queryAgentUserEvt.setNow(true);
        Integer rechargeToday = agentUserMapper.rechargeCount(queryAgentUserEvt);
        reportModelBean.setRechargeToday(rechargeToday);
        // 充值排行榜
        List<ReportModelBean> tops = agentUserMapper.rechargeTop(queryAgentUserEvt);
        reportModelBean.setTops(tops);
        return reportModelBean;
    }


}
