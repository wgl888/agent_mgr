package com.yj.agent.service;

import cn.com.yj.common.param.out.ServiceResp;
import cn.com.yj.common.repository.BaseRepository;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.yj.agent.bean.TAgentUserBean;
import com.yj.agent.bean.TWithDrawBean;
import com.yj.agent.evt.AuditBatchEvt;
import com.yj.agent.evt.QueryAgentUserEvt;
import com.yj.agent.evt.QueryWithDrawEvt;
import com.yj.agent.mapper.AgentUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TWithDrawService {

    @Autowired
    private AgentUserMapper agentUserMapper;
    @Autowired
    private BaseRepository baseRepository;

    public int count(QueryWithDrawEvt evt) {
        return agentUserMapper.queryWithDrawCount(evt);
    }

    public List<TWithDrawBean> query(QueryWithDrawEvt evt) {
        if (evt.getQueryPage() != null && evt.getQuerySize() != null) {
            PageHelper.startPage(evt.getQueryPage(), evt.getQuerySize(), false, false);
        }
        return agentUserMapper.queryWithDrawByParam(evt);
    }

    public List<TWithDrawBean> export(QueryWithDrawEvt evt) {
        return agentUserMapper.queryWithDrawByParam(evt);
    }

    public synchronized ServiceResp auditBatch(AuditBatchEvt evt){
        JSONArray objects = JSONArray.parseArray(evt.getJsonValue());
        if(objects!=null&&objects.size()>0){
            // 修改记录为审核通过   不通过则要把已提现的钱退回去
            // 需要判断当前额度是否足够
            for(Object object : objects){
                JSONObject jsonObject = (JSONObject)object;
                Long id = jsonObject.getLong("id");
                QueryWithDrawEvt queryWithDrawEvt = new QueryWithDrawEvt();
                queryWithDrawEvt.setId(id);
                TWithDrawBean tWithDrawBean =
                        agentUserMapper.queryWithDrawById(queryWithDrawEvt);
                if(tWithDrawBean==null)continue;
                if("SHBTG".equals(evt.getAuditStatus())){
                    // 查询人,将钱还回去
                    QueryAgentUserEvt queryEvt = new QueryAgentUserEvt();
                    queryEvt.setUserNo(tWithDrawBean.getUserNo());
                    List<TAgentUserBean> lst = agentUserMapper.queryByParam(queryEvt);
                    if (lst == null || lst.size() == 0)continue;
                    TAgentUserBean item = lst.get(0);
                    item.setProfitUsedTotal(item.getProfitUsedTotal()-tWithDrawBean.getAmount());
                    tWithDrawBean.setAuditStatus("SHBTG");
                    baseRepository.save(tWithDrawBean);
                    baseRepository.save(item);
                }else if("SHTG".equals(evt.getAuditStatus())){
                    if(tWithDrawBean==null||!"DSH".equals(tWithDrawBean.getAuditStatus()))continue;
                    tWithDrawBean.setAuditStatus("SHTG");
                    baseRepository.save(tWithDrawBean);
                }
            }
        }
        return new ServiceResp().success("");
    }

}
