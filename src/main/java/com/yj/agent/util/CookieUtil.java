package com.yj.agent.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wanggl
 * @version V1.0
 * @Title: CookieUtil
 * @Package com.yj.cashbuzz.util
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/5/25 16:37
 */
public class CookieUtil {


    public static void addCookie(String name, String value, Integer maxAge, HttpServletResponse response) {
        Cookie cookie = new Cookie(name, value);
        if (maxAge != null)
            cookie.setMaxAge(maxAge);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    public static String getCookieValue(String name, HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String value = null;
        if (cookies == null || cookies.length < 1)
            return null;
        for (Cookie c : cookies) {
            if (c.getName().equals(name)) {
                value = c.getValue();
                break;
            }
        }
        return value;
    }

}
