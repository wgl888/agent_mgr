package com.yj.agent.util;

import java.io.Serializable;

public class QueryEvt implements Serializable {

    private Integer init;
    private Integer queryPage;
    private Integer querySize = 10;

    public Integer getInit() {
        return init;
    }

    public void setInit(Integer init) {
        this.init = init;
    }

    public Integer getQueryPage() {
        return queryPage;
    }

    public void setQueryPage(Integer queryPage) {
        this.queryPage = queryPage;
    }

    public Integer getQuerySize() {
        return querySize;
    }

    public void setQuerySize(Integer querySize) {
        this.querySize = querySize;
    }
}
