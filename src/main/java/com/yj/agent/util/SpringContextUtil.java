package com.yj.agent.util;

import org.springframework.context.ApplicationContext;

public class SpringContextUtil {
     
    private static ApplicationContext applicationContext;
 
    //获取上下文
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
 
    //设置上下文
    public static void setApplicationContext(ApplicationContext applicationContext) {
        SpringContextUtil.applicationContext = applicationContext;
    }
 
    //通过名字获取上下文中的bean
    public static Object getBean(String name){
    	if(!applicationContext.containsBean(name)){
    		return null;
    	}
        return applicationContext.getBean(name);
    }
    
    public static Object getBean(Class classType){
    	Object bean = null;
    	try {
    		bean = applicationContext.getBean(classType);
		} catch (Exception e) {}
        return bean;
    }
 
}