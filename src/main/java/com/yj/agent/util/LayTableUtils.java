package com.yj.agent.util;

import cn.com.yj.common.param.out.ServiceResp;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * @author wanggl
 * @version V1.0
 * @Title: LayTableUtils
 * @Package cn.com.yj.plat.web.utils
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/12/9 14:53
 */
public class LayTableUtils {

    public static String renderError(String msg){
        JSONObject object = new JSONObject();
        object.put("code","-1");
        object.put("msg",msg);
        return object.toJSONString();
    }

    public static String render(int count,List list){
        JSONObject object = new JSONObject();
        if(list==null||list.size()==0){
            return renderError("返回对象为空");
        }
        object.put("code","0");
        object.put("count",count);
        object.put("data", JSON.parseArray(JSON.toJSONString(list)));
        return object.toJSONString();
    }
    public static String render(Object count,List list){
        JSONObject object = new JSONObject();
        if(list==null){
            return renderError("返回对象为空");
        }
        object.put("code","0");
        object.put("count",count);
        object.put("data", list);
        return object.toJSONString();
    }
}