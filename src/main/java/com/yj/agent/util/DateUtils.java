package com.yj.agent.util;

import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author wanggl
 * @version V1.0
 * @Title: DateUtils
 * @Package com.yj.cashbuzz.util
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/5/5 10:26
 */
public class DateUtils {

    public static final String START_TIME = " 00:00:00";
    public static final String END_TIME = " 23:59:59";


    public static String timeStamp2Date(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty()) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds + "000")));
    }

    public static Date stringToDate(String date, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String dateToString(Date date, String format) {
        if (date == null) return null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    /*
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    public static Long stringTimeToLong(String date, String time) {
        try {
            if (StringUtils.isEmpty(date)) {
                return null;
            }
            String dateTime = date + time;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateVal = simpleDateFormat.parse(dateTime);
            return dateVal.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
