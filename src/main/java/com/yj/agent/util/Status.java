package com.yj.agent.util;

/**
 * @author wanggl
 * @version V1.0
 * @Title: Status
 * @Package com.wgl.springboot.util
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/4/27 20:07
 */
public enum  Status {

    E("可用"),
    D("删除"),
    T("禁用");

    private String remark;

    private Status(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
