package com.yj.agent.bean;


import cn.com.yj.common.repository.BaseEntity;
import com.yj.agent.util.DateUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

// 用户表
@Entity
@Table(name = "t_agent_user_recharge")
@SequenceGenerator(name = "common_generator", sequenceName = "s_t_agent_user_recharge", // oracle 用到的序列名 s_ts_menu_info
        allocationSize = 1, initialValue = 1) // 主键生成策略,sequenceName为s_表名,其他不用动
public class TAgentUserRechargeBean extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6889352408271995942L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "common_generator")
    @Column(name = "id", length = 11, nullable = false, unique = true)
    private Long id;
    /**
     * 充值记录标识
     */
    @Column(name = "recordId", length = 128, nullable = false)
    private String recordId;
    /**
     * 用户编码
     */
    @Column(name = "gameId", length = 128, nullable = false)
    private String gameId;
    /**
     * 充值金额
     */
    @Column(name = "amount", length = 18, nullable = false)
    private Long amount;
    /**
     * 充值时间
     */
    @Column(name = "rechargeTime", length = 128, nullable = true)
    private String rechargeTime;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getRechargeTime() {
        return rechargeTime;
    }

    public void setRechargeTime(String rechargeTime) {
        this.rechargeTime = rechargeTime;
    }

    @Override
    public String toString() {
        return "TAgentUserRechargeBean{" +
                "id=" + id +
                ", recordId='" + recordId + '\'' +
                ", gameId='" + gameId + '\'' +
                ", amount=" + amount +
                ", rechargeTime='" + rechargeTime + '\'' +
                '}';
    }
}
