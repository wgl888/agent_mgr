package com.yj.agent.bean;


import cn.com.yj.common.repository.BaseEntity;
import com.yj.agent.util.DateUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

// 用户表
@Entity
@Table(name = "t_with_draw")
@SequenceGenerator(name="common_generator",sequenceName="s_t_with_draw", // oracle 用到的序列名 s_ts_menu_info
        allocationSize=1,initialValue=1) // 主键生成策略,sequenceName为s_表名,其他不用动
public class TWithDrawBean extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 6889352408271995942L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO,generator="common_generator")
    @Column(name="id",length = 11,nullable = false,unique = true)
    private Long id;
    /** 编码 */
    @Column(name = "userNo",length = 32,nullable = false)
    private String userNo;
    /** 本次提现金额 */
    @Column(name = "amount",length = 18,nullable = true)
    private Long amount;
    /** 审核状态 */
    @Column(name = "auditStatus",length = 18,nullable = false)
    private String auditStatus;
    /** 审核通过时间 */
    @Column(name = "auditTime",nullable = true)
    private Date auditTime;

    @Transient
    private String gameId;
    @Transient
    private String account;
    @Transient
    private String userName;
    @Transient
    private Date agentTime;
    @Transient
    private Long profitTotal;
    @Transient
    private Long profitUsedTotal;
    @Transient
    private Integer agentLevel;
    @Transient
    private Long totalRate;
    @Transient
    private Long totalOffer;

    public Long getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(Long totalRate) {
        this.totalRate = totalRate;
    }

    public Long getTotalOffer() {
        return totalOffer;
    }

    public void setTotalOffer(Long totalOffer) {
        this.totalOffer = totalOffer;
    }

    @Transient
    public Long getCanTotal(){
        if(this.totalRate==null||this.profitUsedTotal==null)return 0L;
        return this.totalRate - this.profitUsedTotal;
    }

    public String getAgentTimeStr(){
        return DateUtils.dateToString(this.agentTime,"yyyy-MM-dd HH:mm:ss");
    }


    public String getCreateTimeStr(){
        return DateFormatUtils.format(this.getCreateTime(),"yyyy-MM-dd HH:mm:ss");
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getAgentTime() {
        return agentTime;
    }

    public void setAgentTime(Date agentTime) {
        this.agentTime = agentTime;
    }

    public Long getProfitTotal() {
        return totalRate!=null?totalRate:0;
    }

    public void setProfitTotal(Long profitTotal) {
        this.profitTotal = profitTotal;
    }

    public Long getProfitUsedTotal() {
        return profitUsedTotal;
    }

    public void setProfitUsedTotal(Long profitUsedTotal) {
        this.profitUsedTotal = profitUsedTotal;
    }

    public Integer getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(Integer agentLevel) {
        this.agentLevel = agentLevel;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }
}
