package com.yj.agent.bean;


import cn.com.yj.common.repository.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

// 用户表
@Entity
@Table(name = "t_agent_user_rate")
@SequenceGenerator(name = "common_generator", sequenceName = "s_t_agent_user_recharge", // oracle 用到的序列名 s_ts_menu_info
        allocationSize = 1, initialValue = 1) // 主键生成策略,sequenceName为s_表名,其他不用动
public class TAgentUserRateBean extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6889352408271995942L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "common_generator")
    @Column(name = "id", length = 11, nullable = false, unique = true)
    private Long id;
    /**
     * 玩家ID
     */
    @Column(name = "gameId", length = 128, nullable = false)
    private String gameId;
    /**
     * 代理玩家ID
     */
    @Column(name = "agentGameId", length = 128, nullable = false)
    private String agentGameId;
    /**
     * 贡献金币
     */
    @Column(name = "amount", length = 18, nullable = false)
    private Long amount;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAgentGameId() {
        return agentGameId;
    }

    public void setAgentGameId(String agentGameId) {
        this.agentGameId = agentGameId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TAgentUserRateBean{" +
                "id=" + id +
                ", gameId='" + gameId + '\'' +
                ", agentGameId='" + agentGameId + '\'' +
                ", amount=" + amount +
                '}';
    }
}
