package com.yj.agent.bean;

import cn.com.yj.common.repository.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

public class ReportModelBean extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6329709914932448114L;

    private Integer allCount;
    private Integer agentCount;
    private Integer gamerCount;
    private Integer rechargeTotal;
    private Integer rechargeToday;

    private String gameId;
    private String account;
    private String userName;
    private String amount;

    private List<ReportModelBean> tops;

    public List<ReportModelBean> getTops() {
        return tops;
    }

    public void setTops(List<ReportModelBean> tops) {
        this.tops = tops;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getAllCount() {
        return allCount!=null?allCount:0;
    }

    public void setAllCount(Integer allCount) {
        this.allCount = allCount;
    }

    public Integer getAgentCount() {
        return agentCount!=null?agentCount:0;
    }

    public void setAgentCount(Integer agentCount) {
        this.agentCount = agentCount;
    }

    public Integer getGamerCount() {
        return gamerCount!=null?gamerCount:0;
    }

    public void setGamerCount(Integer gamerCount) {
        this.gamerCount = gamerCount;
    }

    public Integer getRechargeTotal() {
        return rechargeTotal!=null?rechargeTotal:0;
    }

    public void setRechargeTotal(Integer rechargeTotal) {
        this.rechargeTotal = rechargeTotal;
    }

    public Integer getRechargeToday() {
        return rechargeToday!=null?rechargeToday:0;
    }

    public void setRechargeToday(Integer rechargeToday) {
        this.rechargeToday = rechargeToday;
    }
}
