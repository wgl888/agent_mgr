package com.yj.agent.bean;


import cn.com.yj.common.repository.BaseEntity;
import com.yj.agent.util.DateUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

// 原始用户表
public class TOrgUserModel implements Serializable {

    private static final long serialVersionUID = -2877672167157753777L;
    /**
     * 玩家ID
     */
    private String userID;
    /**
     * 账号
     */
    private String accounts;
    /**
     * 昵称姓名
     */
    private String nickName;
    /**
     * 注册时间
     */
    private Date registerDate;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
}
