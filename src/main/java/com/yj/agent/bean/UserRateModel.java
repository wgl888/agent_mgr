package com.yj.agent.bean;


import java.io.Serializable;

/**
 * 分成
 *
 * @author lenovo
 */
public class UserRateModel implements Serializable {

    /**
     * 二级ID
     */
    private String gameId2;
    /**
     * 二级分成比例
     */
    private Float profitRate2;
    /**
     * 一级ID
     */
    private String gameId1;
    /**
     * 一级分成
     */
    private Float profitRate1;
    /**
     * 一级所属的二级分成
     */
    private Float subProfitRate1;

    public String getGameId2() {
        return gameId2;
    }

    public void setGameId2(String gameId2) {
        this.gameId2 = gameId2;
    }

    public Float getProfitRate2() {
        return profitRate2;
    }

    public void setProfitRate2(Float profitRate2) {
        this.profitRate2 = profitRate2;
    }

    public String getGameId1() {
        return gameId1;
    }

    public void setGameId1(String gameId1) {
        this.gameId1 = gameId1;
    }

    public Float getProfitRate1() {
        return profitRate1;
    }

    public void setProfitRate1(Float profitRate1) {
        this.profitRate1 = profitRate1;
    }

    public Float getSubProfitRate1() {
        return subProfitRate1;
    }

    public void setSubProfitRate1(Float subProfitRate1) {
        this.subProfitRate1 = subProfitRate1;
    }

    @Override
    public String toString() {
        return "UserRateModel{" +
                "gameId2='" + gameId2 + '\'' +
                ", profitRate2=" + profitRate2 +
                ", gameId1='" + gameId1 + '\'' +
                ", profitRate1=" + profitRate1 +
                ", subProfitRate1=" + subProfitRate1 +
                '}';
    }
}
