package com.yj.agent.bean;


import cn.com.yj.common.repository.BaseEntity;
import com.yj.agent.util.DateUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

// 用户表
@Entity
@Table(name = "t_agent_user")
@SequenceGenerator(name="common_generator",sequenceName="s_t_agent_user", // oracle 用到的序列名 s_ts_menu_info
        allocationSize=1,initialValue=1) // 主键生成策略,sequenceName为s_表名,其他不用动
public class TAgentUserBean extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 6889352408271995942L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO,generator="common_generator")
    @Column(name="id",length = 11,nullable = false,unique = true)
    private Long id;
    /** 编码 */
    @Column(name = "userNo",length = 32,nullable = false)
    private String userNo;
    /** 父用户编码 */
    @Column(name = "parentGameId",length = 128,nullable = true)
    private String parentGameId;
    /** 其他系统的标识 */
    @Column(name = "gameId",length = 128,nullable = true)
    private String gameId;
    /** 账号 */
    @Column(name = "account",length = 128,nullable = false)
    private String account;
    /** 姓名 */
    @Column(name = "userName",length = 128,nullable = true)
    private String userName;
    /** 代理级别 */
    @Column(name = "agentLevel",length = 18,nullable = true)
    private Integer agentLevel;
    /** 分成比例 */
    @Column(name = "profitRate",length = 18,nullable = true)
    private Float profitRate;
    /** 二级分成比例 */
    @Column(name = "subProfitRate",length = 18,nullable = true)
    private Float subProfitRate;
    /** 分成总额 */
    @Column(name = "profitTotal",length = 18,nullable = true)
    private Long profitTotal;
    /** 已提现总额 */
    @Column(name = "profitUsedTotal",length = 18,nullable = true)
    private Long profitUsedTotal;
    /** 贡献总额 */
    @Column(name = "deTotal",length = 18,nullable = true)
    private Long deTotal;
    /** 代理时间 */
    @Column(name = "agentTime",nullable = true)
    private Date agentTime;
    /** 注册时间 */
    @Column(name = "registerTime",nullable = true)
    private Date registerTime;

    @Transient
    private String parentUserName;
    @Transient
    private String registerTimeStr;
    @Transient
    private Long totalRate;
    @Transient
    private Long totalOffer;




    public String getParentUserName() {
        return parentUserName;
    }

    public void setParentUserName(String parentUserName) {
        this.parentUserName = parentUserName;
    }

    public String getRegisterTimeStr() {
        return DateUtils.dateToString(registerTime,"yyyy-MM-dd HH:mm:ss");
    }

    public void setRegisterTimeStr(String registerTimeStr) {
        this.registerTimeStr = registerTimeStr;
    }

    public Long getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(Long totalRate) {
        this.totalRate = totalRate;
    }

    public Long getTotalOffer() {
        return totalOffer!=null?totalOffer:0;
    }

    public void setTotalOffer(Long totalOffer) {
        this.totalOffer = totalOffer;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    @Transient
    public Long getCanTotal(){
        if(this.totalRate==null||this.totalRate==null)return 0L;
        return this.totalRate - this.profitUsedTotal;
    }

    public Long getDeTotal() {
        return deTotal;
    }

    public void setDeTotal(Long deTotal) {
        this.deTotal = deTotal;
    }

    public String getAgentTimeStr(){
        return DateUtils.dateToString(this.agentTime,"yyyy-MM-dd HH:mm:ss");
    }

    public Float getSubProfitRate() {
        return subProfitRate;
    }

    public void setSubProfitRate(Float subProfitRate) {
        this.subProfitRate = subProfitRate;
    }

    public String getParentGameId() {
        return parentGameId;
    }

    public void setParentGameId(String parentGameId) {
        this.parentGameId = parentGameId;
    }

    public Date getAgentTime() {
        return agentTime;
    }

    public void setAgentTime(Date agentTime) {
        this.agentTime = agentTime;
    }

    public Integer getAgentLevel() {
        return agentLevel;
    }

    public void setAgentLevel(Integer agentLevel) {
        this.agentLevel = agentLevel;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }


    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Float getProfitRate() {
        return profitRate;
    }

    public void setProfitRate(Float profitRate) {
        this.profitRate = profitRate;
    }

    public Long getProfitTotal() {
        return totalRate!=null?totalRate:0;
    }

    public void setProfitTotal(Long profitTotal) {
        this.profitTotal = profitTotal;
    }

    public Long getProfitUsedTotal() {
        return profitUsedTotal!=null?profitUsedTotal:0;
    }

    public void setProfitUsedTotal(Long profitUsedTotal) {
        this.profitUsedTotal = profitUsedTotal;
    }
}
