package com.yj.agent.bean;

import cn.com.yj.common.repository.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author wanggl
 * @version V1.0
 * @Title: DataLogBean
 * @Package cn.com.xj.user.service.bean.sys
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/3/27 9:37
 */
public class LoginModel extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6329709914932448114L;

    private String isMgr;
    private String gamerId;

    public String getIsMgr() {
        return isMgr;
    }

    public void setIsMgr(String isMgr) {
        this.isMgr = isMgr;
    }

    public String getGamerId() {
        return gamerId;
    }

    public void setGamerId(String gamerId) {
        this.gamerId = gamerId;
    }
}
