package com.yj.agent.bean;

import cn.com.yj.common.repository.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author wanggl
 * @version V1.0
 * @Title: DataLogBean
 * @Package cn.com.xj.user.service.bean.sys
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/3/27 9:37
 */
@Entity
@Table(name = "d_log")
@SequenceGenerator(name="common_generator",sequenceName="s_d_log", // oracle 用到的序列名 s_tf_custom_group
        allocationSize=1,initialValue=1) // 主键生成策略,sequenceName为s_表名,其他不用动
public class DataLogBean extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6329709914932448114L;

    @Column(name = "operaRemark",length = 512,nullable = true)
    private String operaRemark;
    @Column(name = "className",length = 512,nullable = true)
    private String className;
    @Column(name = "methodName",length = 128,nullable = true)
    private String methodName;
    @Column(name = "inParam",length = 2056,nullable = true)
    private String inParam;
    @Column(name = "outParam",length = 2056,nullable = true)
    private String outParam;
    @Column(name = "isSuccess",length = 12,nullable = false)
    private String isSuccess;
    @Column(name = "timeLong",length = 12,nullable = true)
    private Long timeLong;
    @Column(name = "operaName",length = 128,nullable = true)
    private String operaName;
    @Column(name = "operaId",length = 18,nullable = true)
    private Long operaId;

    public String getOperaName() {
        return operaName;
    }

    public void setOperaName(String operaName) {
        this.operaName = operaName;
    }

    public Long getOperaId() {
        return operaId;
    }

    public void setOperaId(Long operaId) {
        this.operaId = operaId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getInParam() {
        return inParam;
    }

    public void setInParam(String inParam) {
        this.inParam = inParam;
    }

    public String getOutParam() {
        return outParam;
    }

    public void setOutParam(String outParam) {
        this.outParam = outParam;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Long getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(Long timeLong) {
        this.timeLong = timeLong;
    }

    public String getOperaRemark() {
        return operaRemark;
    }

    public void setOperaRemark(String operaRemark) {
        this.operaRemark = operaRemark;
    }

    @Override
    public String toString() {
        return "DataLogBean{" +
                "operaRemark='" + operaRemark + '\'' +
                ", classNmae='" + className + '\'' +
                ", inParam='" + inParam + '\'' +
                ", outParam='" + outParam + '\'' +
                ", isSuccess='" + isSuccess + '\'' +
                ", timeLong=" + timeLong +
                '}';
    }
}
